

@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active"><a href="{{url('home')}}">Dashboad</a> / <a href="{{url('admin/lands')}}">Lands</a> / Add Land</li>
        </ol>
    </div><!--/.row-->

    @if($message = Session::get('message'))
    <br>
    <div class="row btn btn-primary" style="text-align: center; width:100% ">
        <p style="color: #fff">{{ $message }}</p>
    </div>
    <br><br>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Add Land</div>
                <div class="panel-body">
                    <form role="form" action="{{url('admin/add_land')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-3"></div>
                        <div class="col-md-6">


                            <div class="form-group">
                                <label>Select User  </label>
                                <select class="form-control" name="user_id" id="user_id" >
                                    <option value="">-- Select User --</option>
                                    @if(!empty($users))
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name ?? ''}}</option>
                                    @endforeach
                                    @endif
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Land Name </label>
                                <input class="form-control" type="text" value="" name="land_name" id="land_name" placeholder="Land Name Here" />

                            </div>

                            <div class="form-group">
                                <label>Location  </label>
                                <select class="form-control" name="land_location_id" id="land_location_id" >
                                    <option value="">-- Select Location --</option>
                                    @if(!empty($locations))
                                    @foreach($locations as $location)
                                    <option value="{{$location->id}}">{{$location->location_name ?? ''}}</option>
                                    @endforeach
                                    @endif
                                </select>

                            </div>
                            <div class="form-group">
                                <label>Area </label>
                                <input class="form-control" type="text" value="" name="area" id="area" placeholder="Area Here" />

                            </div>
                            <div class="form-group">
                                <label>Address </label>
                                <input class="form-control" type="text" value="" name="address" id="address" placeholder="Address Here" />
                            </div>
                            <div class="form-group">
                                <label>Latitude </label>
                                <input class="form-control" type="text" value="" name="latitude" id="latitude" placeholder="Latitude Here" />
                                
                            </div>
                            <div class="form-group">
                                <label>Longitude </label>
                                <input class="form-control" type="text" value="" name="longitude" id="longitude" placeholder="Longitude Here" />
                            </div>
                           
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Save</button>

                            </div>


                            <div class="col-md-2"></div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->



        </div><!-- /.col-->
       @include('admin.include.footer')
    </div><!-- /.row -->
</div><!--/.main-->
@endsection