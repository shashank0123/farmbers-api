@extends('admin.include.app')

@section('content')
    
        <style type="text/css">
            .table th { font-size: 16px !important }
            .table td { font-size: 14px !important }
        </style>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active"><a href="{{url('home')}}">Dashboad</a>/All Lands</li>
            </ol>
        </div><!--/.row-->
        
        <!-- <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Categories List</h1>
            </div>
        </div> -->
        <!--/.row-->
         @if($message = Session::get('message'))
                <br>
                <div class="row btn btn-primary" style="text-align: center; width:100% ">
                    <p style="color: #fff">{{ $message }}</p>
                </div>
                <br><br>
                @endif
       
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        All Lands

                        <table class="table" style="margin-top: 20px">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Land Name</th>
                                <th>Location</th>
                                <th>Area</th>                              
                                <th>User</th>                              
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($lands))
                            @php $i=1 @endphp
                            @foreach($lands as $land)
                            <tr>
                                <td>{{$i++}}</td>
                                <td class="t-id">{{$land->land_name ?? ''}}</td>
                                <td>{{$land->location_name ?? ""}}</td>
                                
                                <td>{{$land->area ?? ''}}</td>
                                <td>{{$land->name ?? ''}}</td>
                                <td>                                    
                                    <a href="{{url('admin/edit_land/'.$land->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    <a href="{{url('admin/delete_land/'.$land->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                                   </td>
                            </tr>
                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                        <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
                    
                </div>
            </div>
        </div><!--/.row-->
        
      @include('admin.include.footer')
        
    </div>  <!--/.main-->


@endsection