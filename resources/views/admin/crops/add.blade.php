

@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active"><a href="{{url('home')}}">Dashboad</a>/ <a href="{{url('admin/crops')}}">Crops</a> / Add Crop</li>
        </ol>
    </div><!--/.row-->

    @if($message = Session::get('message'))
    <br>
    <div class="row btn btn-primary" style="text-align: center; width:100% ">
        <p style="color: #fff">{{ $message }}</p>
    </div>
    <br><br>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Add Crop</div>
                <div class="panel-body">
                    <form role="form" action="{{url('admin/add_crop')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-3"></div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Crop Name </label>
                                <input class="form-control" type="text" value="" name="crop_name" id="crop_name" placeholder="Name Here" />

                            </div>
                            <div class="form-group">
                                <label>Crop Type </label>
                                <input class="form-control" type="text" value="" name="crop_type" id="crop_type" placeholder="Email Here" />

                            </div>
                            <div class="form-group">
                                <label>Crop Cycle </label>
                                <input class="form-control" type="text" value="" name="crop_cycle" id="crop_cycle" placeholder="Name Here" />
                                
                            </div>

                            <div class="form-group">
                                <label>Crop Image </label>
                                <input class="form-control" type="file" value="" name="crop_image" id="crop_image" placeholder="Name Here" />

                            </div>
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Save</button>

                            </div>


                            <div class="col-md-2"></div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->



        </div><!-- /.col-->
        @include('admin.include.footer')
    </div><!-- /.row -->
</div><!--/.main-->
@endsection