<script type="text/javascript">
	
	function getSteps(){
		var crop_id = $('#crop_id').val();
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		$.ajax({
			url: '/admin/get_setps',
			type: 'POST',
			data: {_token: CSRF_TOKEN, crop_id: crop_id },
			success: function (data) {
				$('#step_id').html(data);
			},
			failure: function (data) {
				alert('Something Went Wrong');
			}
		});
	}


</script>