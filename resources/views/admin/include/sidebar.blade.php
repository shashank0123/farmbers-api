<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>Farm</span>bers</a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
					</a>
					<ul class="dropdown-menu dropdown-messages">
						<li>
							<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
								<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
							</a>
							<div class="message-body"><small class="pull-right">3 mins ago</small>
								<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
								<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
							</div>
						</li>
						<li class="divider"></li>
						<li>
							<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
								<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
							</a>
							<div class="message-body"><small class="pull-right">1 hour ago</small>
								<a href="#">New message from <strong>Jane Doe</strong>.</a>
								<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
							</div>
						</li>
						<li class="divider"></li>
						<li>
							<div class="all-button"><a href="#">
								<em class="fa fa-inbox"></em> <strong>All Messages</strong>
							</a></div>
						</li>
					</ul>
				</li>
				<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
					<em class="fa fa-bell"></em><span class="label label-info">5</span>
				</a>
				<ul class="dropdown-menu dropdown-alerts">
					<li><a href="#">
						<div><em class="fa fa-envelope"></em> 1 New Message
							<span class="pull-right text-muted small">3 mins ago</span></div>
						</a></li>
						<li class="divider"></li>
						<li><a href="#">
							<div><em class="fa fa-heart"></em> 12 New Likes
								<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-user"></em> 5 New Followers
									<span class="pull-right text-muted small">4 mins ago</span></div>
								</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.container-fluid -->
		</nav>

		<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
			<div class="profile-sidebar">
				<div class="profile-userpic">
					<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
				</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">{{ucfirst(Auth::user()->name)}}</div>
					<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="divider"></div>
			
			<ul class="nav menu">
				<li @if($page=='dashboard') {{'class=active'}}@endif><a href="{{url('home')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>

				


				<li class="parent @if($page=='user' || $page=='add_user' || $page=='edit_user') {{'active'}}@endif "><a data-toggle="collapse" href="#user">
					<em class="fa fa-user">&nbsp;</em> User <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="user">
					<li><a class="" href="{{url('admin/users')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Users List
					</a></li>
					<li><a class="" href="{{url('admin/add_user')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Add User
					</a></li>
				</ul>
			</li>


			<li class="parent @if($page=='location' || $page=='add_location' || $page == 'edit_location') {{'active'}}@endif "><a data-toggle="collapse" href="#location">
				<em class="fa fa-arrows">&nbsp;</em> Locations <span data-toggle="collapse" href="#event" class="icon pull-right"><em class="fa fa-plus"></em></span>
			</a>
			<ul class="children collapse" id="location">
				<li><a class="" href="{{url('admin/locations')}}">
					<span class="fa fa-arrow-right">&nbsp;</span> All Locations
				</a></li>
				<li><a class="" href="{{url('admin/add_location')}}">
					<span class="fa fa-arrow-right">&nbsp;</span> Add Location 
				</a></li>
			</ul>
		</li>


		<li class="parent @if($page=='land' || $page=='add_land' || $page == 'edit_land') {{'active'}}@endif "><a data-toggle="collapse" href="#land">
			<em class="fa fa-bar-chart">&nbsp;</em> Land <span data-toggle="collapse" href="#land" class="icon pull-right"><em class="fa fa-plus"></em></span>
		</a>
		<ul class="children collapse" id="land">
			<li><a class="" href="{{url('admin/lands')}}">
				<span class="fa fa-arrow-right">&nbsp;</span> Lands List
			</a></li>
			<li><a class="" href="{{url('admin/add_land')}}">
				<span class="fa fa-arrow-right">&nbsp;</span> Add Land
			</a></li>
		</ul>
	</li>


	<li class="parent @if($page=='crop' || $page=='add_crop' || $page=='edit_crop') {{'active'}}@endif "><a data-toggle="collapse" href="#crop">
		<em class="fa fa-leaf">&nbsp;</em> Crop <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
	</a>
	<ul class="children collapse" id="crop">
		<li><a class="" href="{{url('admin/crops')}}">
			<span class="fa fa-arrow-right">&nbsp;</span> Crops
		</a></li>
		<li><a class="" href="{{url('admin/add_crop')}}">
			<span class="fa fa-arrow-right">&nbsp;</span> Add Crop
		</a></li>
	</ul>
</li>

<li class="parent @if($page=='change' || $page=='add_change' || $page=='edit_change') {{'active'}}@endif "><a data-toggle="collapse" href="#change">
		<em class="fa fa-edit">&nbsp;</em> Change <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
	</a>
	<ul class="children collapse" id="change">
		<li><a class="" href="{{url('admin/changes')}}">
			<span class="fa fa-arrow-right">&nbsp;</span> Changes List
		</a></li>
		<li><a class="" href="{{url('admin/add_change')}}">
			<span class="fa fa-arrow-right">&nbsp;</span> Add Change 
		</a></li>
	</ul>
</li>


	<!-- <li class="parent @if($page=='assign' || $page=='add_assign' || $page == 'edit_assign') {{'active'}}@endif "><a data-toggle="collapse" href="#assign">
		<em class="fa fa-asterisk">&nbsp;</em> Assignments <span data-toggle="collapse" href="#assign" class="icon pull-right"><em class="fa fa-plus"></em></span>
	</a>
	<ul class="children collapse" id="assign">
		<li><a class="" href="{{url('admin/lands')}}">
			<span class="fa fa-arrow-right">&nbsp;</span>Lands-Users List
		</a></li>
		<li><a class="" href="{{url('admin/add_land')}}">
			<span class="fa fa-arrow-right">&nbsp;</span> Assign L-U 
		</a></li>


		<li><a class="" href="{{url('admin/lands')}}">
			<span class="fa fa-arrow-right">&nbsp;</span>Crops-Lands List
		</a></li>
		<li><a class="" href="{{url('admin/add_land')}}">
			<span class="fa fa-arrow-right">&nbsp;</span> Assign C-L
		</a></li>
	</ul>
</li>
 -->


		<!-- <li class="parent @if($page=='step' || $page=='category_detail') {{'active'}}@endif "><a data-toggle="collapse" href="#step">
			<em class="fa fa-navicon">&nbsp;</em> Steps <span data-toggle="collapse" href="#step" class="icon pull-right"><em class="fa fa-plus"></em></span>
		</a>
		<ul class="children collapse" id="step">
			<li><a class="" href="{{url('home')}}">
				<span class="fa fa-arrow-right">&nbsp;</span> Steps List
			</a></li>
			<li><a class="" href="{{url('home')}}">
				<span class="fa fa-arrow-right">&nbsp;</span> Add Step
			</a></li>
		</ul>
	</li> -->



<!-- 
<li @if($page=='offers') {{'class=active'}}@endif><a href="{{url('admin/offers')}}"><em class="fa fa-calendar">&nbsp;</em> Offers</a></li>

<li @if($page=='exclusive') {{'class=active'}}@endif><a href="{{url('admin/exclusive_deals')}}"><em class="fa fa-bar-chart">&nbsp;</em> Exclusive Deals</a></li>

<li @if($page=='premium') {{'class=active'}}@endif><a href="{{url('admin/premium_services')}}"><em class="fa fa-toggle-off">&nbsp;</em> Premium Services</a></li>

<li @if($page=='cms') {{'class=active'}}@endif><a href="{{url('admin/cms')}}"><em class="fa fa-clone">&nbsp;</em> CMS</a></li>

<li class="parent @if($page=='video' || $page=='add_video' || $page=='edit_video') {{'active'}}@endif "><a data-toggle="collapse" href="#video">
	<em class="fa fa-navicon">&nbsp;</em> Videos <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
</a>
<ul class="children collapse" id="video">
	<li><a class="" href="{{url('admin/videos')}}">
		<span class="fa fa-arrow-right">&nbsp;</span> All Videos
	</a></li>
	<li><a class="" href="{{url('admin/add_video_detail')}}">
		<span class="fa fa-arrow-right">&nbsp;</span> Add Video Link
	</a></li>
</ul>
</li> -->

<!-- <li @if($page=='contact') {{'class=active'}}@endif><a href="{{url('admin/contacts')}}"><em class="fa fa-clone">&nbsp;</em> Contact Form</a></li>
<li @if($page=='newsletter') {{'class=active'}}@endif><a href="{{url('admin/newsletters')}}"><em class="fa fa-clone">&nbsp;</em> Newsletters</a></li>

-->
<li><a  onclick="event.preventDefault();
document.getElementById('logout-form').submit();"><em class="fa fa-power-off">&nbsp;</em> Logout</a>



<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	@csrf
</form>

</li>

</ul>
</div><!--/.sidebar-->
