

@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active"><a href="{{url('home')}}">Dashboad</a> / <a href="{{url('admin/crops')}}">Crops</a> / Add Step</li>
        </ol>
    </div><!--/.row-->

    @if($message = Session::get('message'))
    <br>
    <div class="row btn btn-primary" style="text-align: center; width:100% ">
        <p style="color: #fff">{{ $message }}</p>
    </div>
    <br><br>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Add Step for crop - <i><b>{{ucfirst($crop->crop_name ?? '')}}</b></i></div>
                <div class="panel-body">
                    <form role="form" action="{{url('admin/add_step')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <input type="hidden" name="crop_id" id="crop_id" value="{{$crop->id}}">
                            <div class="form-group">
                                <label>Step Number </label>
                                <input class="form-control" type="number" value="" name="step_number" id="step_number" placeholder="Name Here" />

                            </div>
                            <div class="form-group">
                                <label>No of Days </label>
                                <input class="form-control" type="number" value="" name="no_of_days" id="no_of_days" placeholder="Email Here" />

                            </div>
                            <div class="form-group">
                                <label>Description </label>
                                <input class="form-control" type="text" value="" name="description" id="description" placeholder="Name Here" />
                                
                            </div>

                            <div class="form-group">
                                <label>Image </label>
                                <input class="form-control" type="file" value="" name="step_image" id="step_image" placeholder="Name Here" />

                            </div>
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Save</button>

                            </div>


                            <div class="col-md-2"></div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->



        </div><!-- /.col-->
       @include('admin.include.footer')
    </div><!-- /.row -->
</div><!--/.main-->
@endsection