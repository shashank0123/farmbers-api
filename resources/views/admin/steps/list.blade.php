@extends('admin.include.app')

@section('content')
    
        <style type="text/css">
            .table th { font-size: 16px !important }
            .table td { font-size: 14px !important }
        </style>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active"><a href="{{url('home')}}">Dashboad</a>/All Steps</li>
            </ol>
        </div><!--/.row-->
        
        <!-- <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Categories List</h1>
            </div>
        </div> -->
        <!--/.row-->
         @if($message = Session::get('message'))
                <br>
                <div class="row btn btn-primary" style="text-align: center; width:100% ">
                    <p style="color: #fff">{{ $message }}</p>
                </div>
                <br><br>
                @endif
       
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-10">
                        Steps For {{$crop->crop_name ?? ''}}
                                
                            </div>
                            <div class="col-sm-2">
                                <a href="{{url('admin/add_step/'.$crop->id)}}" class="btn btn-primary">Add Step</a>
                            </div>
                        </div>

                        <table class="table" style="margin-top: 20px">
                        <thead>
                            <tr>
                                <th>Step Number</th>
                                <th>Description</th>
                                <th>No of Days</th>
                                <th>Image</th>                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($steps))
                            @php $i=1 @endphp
                            @foreach($steps as $step)
                            <tr>
                                <td>{{$step->step_number ?? ""}}</td>
                                <td>{{$step->description ?? ''}}</td>
                                <td class="t-id">{{$step->no_of_days ?? ''}}</td>
                                <td><img src="{{asset('steps_images/'.$step->step_image)}}" style="height: 80px; width: auto"></td>
                                <td>                                    
                                    <a href="{{url('admin/edit_step/'.$step->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    <a href="{{url('admin/delete_step/'.$step->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                                   </td>
                            </tr>
                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                        <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
                    
                </div>
            </div>
        </div><!--/.row-->
        
      @include('admin.include.footer')
        
    </div>  <!--/.main-->


@endsection