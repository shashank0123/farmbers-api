@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active"> <a href="{{url('home')}}"> Dashboad </a> / <a href="{{url('admin/steps/'.$step->crop_id)}}"> Steps </a> / Edit Step</li>
            </ol>
        </div><!--/.row-->
        
        @if($message = Session::get('message'))
                <br>
                <div class="row btn btn-primary" style="text-align: center; width:100% ">
                    <p style="color: #fff">{{ $message }}</p>
                </div>
                <br><br>
                @endif
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center;">Edit Step</div>
                    <div class="panel-body">
                            <form role="form" action="{{url('admin/edit_step/'.$step->id)}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                        
                        <div class="col-md-6">
                            <input type="hidden" name="step_id" id="step_id" value="{{$step->id}}">
                            <div class="form-group">
                                <label>Step Number </label>
                                <input class="form-control" type="number" value="{{$step->step_number ?? ''}}" name="step_number" id="step_number" placeholder="Step Number Here" />

                            </div>
                            <div class="form-group">
                                <label>No of Days </label>
                                <input class="form-control" type="number" value="{{$step->no_of_days ?? ''}}" name="no_of_days" id="no_of_days" placeholder="Number of days Here" />

                            </div>
                            <div class="form-group">
                                <label>Description </label>
                                <textarea class="form-control"  name="description" id="description" placeholder="Description Here" />{{$step->description ?? ''}}</textarea>
                                
                            </div>

                            <div class="form-group">
                                <label>Image </label>
                                <input class="form-control" type="file" value="" name="step_image" id="step_image" placeholder="Name Here" />

                            </div>
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Update</button>

                            </div>


                                </div>

                               
                        <div class="col-md-6">
                            <img src="{{asset('steps_images/'.$step->step_image)}}" style="width: 100%; height: auto; margin-top: 20%">
                        </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.panel-->
                
                
               
            </div><!-- /.col-->
            @include('admin.include.footer')
        </div><!-- /.row -->
    </div><!--/.main-->
@endsection