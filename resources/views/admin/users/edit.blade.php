

@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active"><a href="{{url('home')}}">Dashboad</a> / <a href="{{url('admin/users')}}">Users</a> / Edit User</li>
        </ol>
    </div><!--/.row-->

    @if($message = Session::get('message'))
    <br>
    <div class="row btn btn-primary" style="text-align: center; width:100% ">
        <p style="color: #fff">{{ $message }}</p>
    </div>
    <br><br>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Edit User</div>
                <div class="panel-body">
                    <form role="form" action="{{url('admin/edit_user/'.$user->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Name </label>
                                <input class="form-control" type="text" value="{{$user->name ?? ''}}" name="name" id="name" placeholder="Name Here" />

                            </div>

                            <div class="form-group">
                                <label>Nick Name </label>
                                <input class="form-control" type="text" value="{{$user->nickname ?? ''}}" name="nickname" id="nickname" placeholder="Name Here" />

                            </div>

                            <div class="form-group">
                                <label>Email </label>
                                <input class="form-control" type="text" value="{{$user->email ?? ''}}" name="email" id="email" placeholder="Email Here" />

                            </div>

                            <div class="form-group">
                                <label>Mobile </label>
                                <input class="form-control" type="text" value="{{$user->mobile ?? ''}}" name="mobile" id="mobile" placeholder="Mobile Here" />

                            </div>

                            <div class="form-group">
                                <label>Profession </label>
                                <input class="form-control" type="text" value="{{$user->profession ?? ''}}" name="profession" id="profession" placeholder="Profession Here" />

                            </div>

                            <div class="form-group">
                                <label>Password </label>
                                <input class="form-control" type="password" value="" name="password" id="password" placeholder="Password Here" />

                            </div>

                            <div class="form-group">
                                <label>Profile Pic </label>
                                <input class="form-control" type="file" value="" name="profile_pic" id="profile_pic" placeholder="Name Here" />

                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" required="required" name="account_status" id="account_status">
                                    <option value="">--Select--</option>
                                    <option value="1" @if($user->account_status =='1'){{'selected'}}@endif>Active </option>
                                    <option value="0" @if($user->account_status =='0'){{'selected'}}@endif>Inactive </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Submit Button</button>

                            </div>
                        </div>

                            <div class="col-md-6" style=" text-align: center;">
                                <img src="{{asset('profiles/'.$user->profile_pic)}}" style="width: 75%; height: auto; margin-top: 20%;">
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->



        </div><!-- /.col-->
       @include('admin.include.footer')
    </div><!-- /.row -->
</div><!--/.main-->
@endsection