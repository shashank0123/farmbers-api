

@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active"><a href="{{url('home')}}">Dashboad</a> / <a href="{{url('admin/locations')}}">Locations</a> / Edit Location</li>
        </ol>
    </div><!--/.row-->

    @if($message = Session::get('message'))
    <br>
    <div class="row btn btn-primary" style="text-align: center; width:100% ">
        <p style="color: #fff">{{ $message }}</p>
    </div>
    <br><br>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Edit Location</div>
                <div class="panel-body">
                    <form role="form" action="{{url('admin/edit_location/'.$location->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-3"></div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Location Name </label>
                                <input class="form-control" type="text" value="{{$location->location_name ?? ''}}" name="location_name" id="location_name" placeholder="Location Name Here" />

                            </div>

                            <div class="form-group">
                                <label>Parent Location  </label>
                                <input class="form-control" type="text" value="{{$location->parent_location ?? ''}}" name="parent_location" id="parent_location" placeholder="Parent Location Name Here" />

                            </div>
                            <div class="form-group">
                                <label>Pin Code </label>
                                <input class="form-control" type="text" value="{{$location->pincode ?? ''}}" name="pincode" id="pincode" placeholder="Pin Code Here" />

                            </div>
                            <div class="form-group">
                                <label>Latitude </label>
                                <input class="form-control" type="text" value="{{$location->latitude ?? ''}}" name="latitude" id="latitude" placeholder="Latitude Here" />
                                
                            </div>
                            <div class="form-group">
                                <label>Longitude </label>
                                <input class="form-control" type="text" value="{{$location->longitude ?? ''}}" name="longitude" id="longitude" placeholder="Longitude Here" />
                                
                            </div>

                           
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Update</button>

                            </div>


                            <div class="col-md-2"></div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->



        </div><!-- /.col-->
        @include('admin.include.footer')
    </div><!-- /.row -->
</div><!--/.main-->
@endsection