

@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active"><a href="{{url('home')}}">Dashboad</a> / <a href="{{url('admin/changes')}}">Changes</a> / Add Change</li>
        </ol>
    </div><!--/.row-->

    @if($message = Session::get('message'))
    <br>
    <div class="row btn btn-primary" style="text-align: center; width:100% ">
        <p style="color: #fff">{{ $message }}</p>
    </div>
    <br><br>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align: center;">Add Change</div>
                <div class="panel-body">
                    <form role="form" action="{{url('admin/add_change')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-3"></div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Crop </label>
                                <select class="form-control"  name="crop_id" id="crop_id" required="required" onchange="getSteps()">
                                    <option> -- Select Crop -- </option>
                                    @if(!empty($crops))
                                    @foreach($crops as $crop)
                                    <option value="{{$crop->id}}">
                                        {{ucfirst($crop->crop_name ?? '')}}
                                    </option>
                                    @endforeach
                                    @endif
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Step  </label>
                                <select class="form-control"  name="step_id" id="step_id" required="required" >
                                    <option> -- Select Step -- </option>
                                    
                                </select>

                            </div>
                            <div class="form-group">
                                <label>No of Days </label>
                                <input class="form-control" type="number" value="" name="no_of_days" id="no_of_days" placeholder="Pin Code Here" />

                            </div>
                            <div class="form-group">
                                <label>Change Type </label>
                                <select class="form-control"  name="change_type" id="change_type" required="required" >
                                    <option> -- Select Type -- </option>
                                    
                                    <option value="Increment">Increment</option>
                                    <option value="Decrement">Decrement</option>
                                   
                                </select>
                                
                            </div>
                            <div class="form-group">
                                <label>Reason Of Change </label>
                                <textarea class="form-control" name="reason_of_change" id="reason_of_change" placeholder="Reason Of Change Here"></textarea>
                                
                            </div>
                            <div class="form-group">
                                <label>Value after Change </label>
                                <textarea class="form-control" name="value_after_change" id="value_after_change" placeholder="Reason Of Change Here"></textarea>
                                
                            </div>

                           
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Save</button>

                            </div>


                            <div class="col-md-2"></div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->



        </div><!-- /.col-->
        @include('admin.include.footer')
    </div><!-- /.row -->
</div><!--/.main-->
@endsection