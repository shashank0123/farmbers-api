@extends('admin.include.app')
  
@section('content')



<div class="container" width="100%">
	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Add Bar Images</div>
				<div class="panel-body">
					<a href="/admin/bar-list"><button type="submit" class="btn btn-success">Back</button> </a><br><br>

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif

					@if($message = Session::get('message'))
					<div class="alert alert-primary">
						<p>{{ $message }}</p>
					</div>
					@endif
					
					
            {!! Form::open([ 'route' => [ 'dropzone.store' ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
            <input type="hidden" name="bar_id" value="{{$barId}}" id="bar_id">
            
            {!! Form::close() !!}
            <br><br>
            <div class="container">
            	{{-- Saved Images --}}
            	<div class="row">
            		@if(!empty($images))
            		@foreach($images as $image)
            		<div class="col-sm-4" class="saved-images" id="bar-image{{$image->id}}">
            			<img src="/daru-assets/images/Admin/Bars/{{$image->image_url}}" id="bars-images">
            			<div class="remove-icon"><i class="fa fa-times" onclick="deleteImage({{$image->id}})"></i></div>
            		</div>
            		@endforeach
            		@endif
            	</div>
            </div>

				</div>
			</div>
		</div>
	</div>
</div>



@endsection

@section('script')

<script type="text/javascript">
        Dropzone.options.imageUpload = {
            maxFilesize         :       1,
            acceptedFiles: ".jpeg,.jpg,.png,.gif"
        };

        function deleteImage(id){

        	 var csrf = $('meta[name="csrf-token"]').attr('content');
        	 alert(csrf);
        	$.ajax({
        type: "POST",
        url: "/admin/delete-bar-image",
        data:{  id: id },
        success:function(data){
         
          if(data.message == 'Deleted Successfully'){
            $('#bar-image'+id).hide();
            
          }
          else{
            alert(data.message);
          }          
        }
      });
        }
</script>

@endsection
