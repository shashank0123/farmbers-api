@extends('admin.include.app')

@section('content')


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Dashboad / Categories / Edit Image Detail</li>
            </ol>
        </div><!--/.row-->
        
        @if($message = Session::get('message'))
                <br>
                <div class="row btn btn-primary" style="text-align: center; width:100% ">
                    <p style="color: #fff">{{ $message }}</p>
                </div>
                <br><br>
                @endif
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center;">Edit Image Detail</div>
                    <div class="panel-body">
                            <form role="form" action="{{url('admin/update_media/'.$media->id)}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                        
                        <div class="col-md-6">

                                <div class="form-group">
                                    <br>
                                    <label>Image Category</label>
                            <select class="form-control" name="category_id" id="category_id" required="required">
                                <option value="">-- Select --</option>  
                                <option value="banner"@if($media->image_category == 'banner'){{'selected'}}@endif>Banner</option>
                                <option value="slider"@if($media->image_category == 'slider'){{'selected'}}@endif>Slider</option>
                                <option value="static"@if($media->image_category == 'static'){{'selected'}}@endif>Static</option>
                            </select>
                                                                            </div>

                                <div class="form-group">
                                    <label>Image Title</label>
                            <input class="form-control" type="text" value="@if($media->image_title){{$media->image_title}}@endif" name="image_title" id="image_title" placeholder="Image Title" />
                                    
                                </div>

                                <div class="form-group">
                                        
                                <label>Image</label>
                                <input type="file" placeholder="" class="form-control" name="image_path" is="image_path"> 
                            </div>
                                             <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="image_description" cols="40" rows="3" id="image_description" spellcheck="true">@if($media->image_description){{$media->image_description}}@endif</textarea>
                    </div>

                   

                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" required="required" name="media_status" id="media_status">
                                            <option value="">--Select--</option>
                                            <option value="Active" @if($media->status == 'Active'){{'selected'}}@endif>Active </option>
                                            <option value="Inactive" @if($media->status == 'Inactive'){{'selected'}}@endif>Inactive </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <br>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                        
                                    </div>
                                </div>

                               
                        <div class="col-md-6">
                            <img src="{{asset('site_media/'.$media->image_path)}}" style="width: 100%; height: auto; margin-top: 20%">
                        </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.panel-->
                
                
               
            </div><!-- /.col-->
            <div class="col-sm-12">
                <p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
            </div>
        </div><!-- /.row -->
    </div><!--/.main-->
@endsection