<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title> <?php echo "Fasttrack";?> Live Tracking</title>

	<link href="/assets/css/bootstrap.css" rel="stylesheet">
	<link href="/assets/css/font-awesome.css" rel="stylesheet">
	<link href="/assets/css/custom-tracking.css" rel="stylesheet">
	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script src="/assets/js/ie-emulation-modes-warning.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

      <!-- Modernizer Script for old Browsers -->
      <script src="/assets/js/modernizr-2.6.2.min.js"></script>
      <style type="text/css">

      	.car-number-plate{
      		background-color: #fff;
      		border:1px solid #000;
      		color: #000;
      		font-size: 22px;
      		line-height: 24px;
      		display: inline-block;
      		padding: 0px 10px;
      	}
      	.tracking-footer{
      		background: linear-gradient(to bottom right, #06a9e0 12%, #fff 180%);            border: 1px solid #fff;
      		color: #fff;
      	}
      	.img-call-center{
      		max-width: 80px;
      		text-align: center;
      		margin: 0px auto 20px auto;
      	}
      	.img-t-user{
      		float: right;
      		margin-left: 20px;
      		height: 100px;
      		width: 100px;
      	}
      	.image-sap{
      		width: 100%;
      		height: 150px;
      		object-fit: fill;
      		object-position: 50% 50%;
      		transition: all 0.4s ease 0s;
      	}
      	.t-driver-info p, .t-trip-info p{
      		margin-bottom: 5px;
      	}
      	.t-driver-info p span{
      		width: 135px;
      		display: inline-block;
      	}
      	.t-trip-info p span{
      		width: 100px;
      		display: inline-block;
      	}
      	.t-driver-info .t-car-plate{
      		margin: 0px 0 0px auto;
      	}

      	@media (min-width: 992px) and (max-width: 1200px){
      		.t-driver-info{
      			/*background-image: url(assets/images/t-sap.jpg);*/
      			background-repeat: repeat-y;
      			background-position: left;
      			min-height: 180px;
      		}

      	}
      	@media (max-width: 992px){
      		.tm-driver-info{
      			display: block;
      			padding: 0px 0px 03px 0px;
      		}
      		.img-t-user-mobile{
      			border:10px solid #048141;
      			margin-top: -50px;
      			float: none;
      			margin-left: auto;
      			margin-right: auto;
      		}
      		.mobile-info span{
      			display:block;
      		}
      		.border-top{
      			border-top: 2px solid rgba(255,255,255,0.2);
      			margin-top: 10px;
      			padding-top: 20px;
      			padding-bottom: 30px;
      		}
      		.ride-info{
      			text-align: left;
      			max-width: 450px;
      			padding-right:50px; 
      			margin: 0px auto;
      		}
      		.mobile-sap{
      			position: absolute;
      			right: 0px;
      			bottom: 0px;
      		}
      		.mobile-care{
      			padding-right: 90px;
      			padding-left: 30px;
      		}
      	}
      	.yoobel-care{
      		font-size: 12px;
      	}
      	@media (min-width: 992px){
          .yo-care{
           width: 18%;
           float: left;
         }
         .yo-sap{
           width: 6%;
           float: left;
         }
         .yo-trip{
           width: 30%;
           float: left;
         }
         .yo-driver{
           width: 40%;
           float: left;
         }
       }

       @media (max-width: 1300px){
          .tracking-footer{
            font-size: 13px;
          }
          .img-t-user{
            float: right;
            margin-top: 13px;
            margin-left: 15px;
            height: 80px;
            width: 80px;
          }
          .img-call-center {
            margin: 0 auto 10px;
            max-width: 70px;
          }
          .yoobel-care{
            font-size: 10px;            
          }
       }
       .vl {
        border-left: 6px solid #fff;
      }
      </style>
  </head>
  <body>
  	<?php $last_location = $data['last_location'];//echo '<pre>'; print_r($last_location); ?>
  	<div class="" style="padding: 0; overflow: hidden;background-color: #06a9e0; ">
  		<header class="text-center">
  			<a href="#"> <img src="/img/fast%20track-final-logo-design.png" class="img-responsive" alt="<?php echo "Fasttrack";?>"> </a>
  		</header>
  		<div class="tracking-map">
  			<div id="map-canvas" style="width:auto;position: inherit;"></div>
  		</div>

  		<div class="tracking-footer visible-md visible-lg">
  			<div class="row" style="padding: 0px 15px;">
  				<div class="yo-care text-center yoobel-care">
  					<div class="p-20">
  						<img src="/assets/images/t-care.png" class="img-responsive img-call-center">
  						<p class="m-b-0"><!--  <?php echo "Fasttrack";?> Call Center No. : -->  1800 1218 026 </p>
  					</div>
  				</div>
  				<div class="yo-sap  hidden-sm hidden-xs hidden-md vl">
  					<!-- <img src="/assets/images/t-footer-sap.jpg" class="image-sap"> -->
  				</div>
  				<div class="yo-trip text-left">
  					<div class="p-20 t-trip-info">
  						<!-- <p> <span>  IMEI : </span> <?php if(!empty($last_location['device_imei'])) echo $last_location['device_imei']; ?>    </p> -->
  						<p> <span> Date/Time : </span>  <?php echo $last_location['trip_datetime']; ?>  </p>
  					</div>
  				</div>
  				<div class="yo-sap  hidden-sm hidden-xs hidden-md vl">
  					<!-- <img src="/assets/images/t-footer-sap.jpg" class="image-sap"> -->
  				</div>
  				<div class=" text-left yo-driver">
  					<div class="p-20 t-driver-info">
  						<div class="row">
  							<div class="col-sm-4 text-center" style="padding: 0px;">
  								<p style="padding-top: 30px;">
  									<?php if(!empty($last_location['vehicle_type'])) echo "Vehicle Type : ".$last_location['vehicle_type']; ?>
  									<span style="width: auto;"></span> </p>
  								<div class="car-number-plate">
  									<?php if(!empty($last_location['registration_number'])) echo $last_location['registration_number']; ?>
  								</div>
  							</div>
                <div class="col-sm-8"  style="padding: 0px;">
                  <img src="<?php //echo $last_location['car_photo'];?>" class="img-responsive img-t-user img-thumbnail img-circle">
                </div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="tracking-footer visible-xs visible-sm">
  			  <div class="yo-trip text-left">
            <div class="p-20 t-trip-info">
              <!-- <p> <span>  IMEI : </span> <?php if(!empty($last_location['device_imei'])) echo $last_location['device_imei']; ?> </p> -->
              <p> <span>  Date/Time : </span>  <?php echo $last_location['trip_datetime']; ?>  </p>
            </div>
          </div>
          <div class="yo-sap  hidden-sm hidden-xs hidden-md">
            <img src="/assets/images/t-footer-sap.jpg" class="image-sap">
          </div>
          <div class=" text-left yo-driver">
            <div class="p-20 t-driver-info">
              <div class="row">
                <div class="col-sm-4 text-center" style="padding: 0px;">
                  <p style="padding-top: 30px;">
                    <?php if(!empty($last_location['vehicle_type'])) echo "Vehicle Type : ".$last_location['vehicle_type']; ?>
                    <span style="width: auto;"></span> </p>
                  <div class="car-number-plate">
                    <?php if(!empty($last_location['registration_number'])) echo $last_location['registration_number']; ?>
                  </div>
                </div>
                <div class="col-sm-8 text-center"  style="padding: 0px;">
                  <img style="float: none !important;margin-left: 0px !important;" src="<?php //echo $last_location['car_photo'];?>" class="img-responsive img-t-user img-thumbnail img-circle">
                </div>
                <div class="col-sm-8 text-center"  style="padding: 0px;margin-top: 1em !important;">
                  <img src="/assets/images/t-care.png" class="img-responsive img-call-center">
                  <p class="m-b-0"> <!-- <?php echo "Fasttrack";?> Call Center Number :  --> 1800 1218 026 </p>
                </div>
              </div>
            </div>
          </div>
  			</dir>
  			<img src="/assets/images/mobile-sap.png" class="mobile-sap">
  		</div>
  	</div>
  	<script src="/assets/js/jquery.min.js"></script> 
  	<script src="/assets/js/bootstrap.js"></script> 
  	<script src="/assets/js/wow.min.js"></script> 
  	<script src="/assets/js/custom.js"></script> 

  	<script type="text/javascript">
  		$(document).ready(function(){
      			var window_height = $(window).height();
      			var header_height = $("header").height();
      			var footer_height = $(".tracking-footer").height();
    	    //alert("window_height:"+window_height+' header_height:'+header_height+' footer_height:'+footer_height);
    	    $("#map-canvas").height(window_height - (header_height+footer_height+35));
    	});
    </script>

<style type="text/css">
	.gm-style-iw1 {
		width: 350px !important;
		top: 15px !important; 
		left: 0 !important;    
		background-color: #fff;
		box-shadow: 0 1px 6px rgb(255, 64, 75);
		border: 1px solid #ef6c0f;
		border-radius: 2px 2px 0 0;
	}
	.iw-title {
		font-family: 'Open Sans Condensed', sans-serif;
		font-size: 22px;
		font-weight: 400;
		padding: 10px;
		background-color: #ef6c0f;
		color: white;
		margin: 0;
		border-radius: 2px 2px 0 0;
	}
  #map-canvas { position: inherit!important; overflow: hidden; }
	#map-canvas img{max-width:none!important;background:none!important}
</style>
<!-- For getting local timezone -->
<script src="/assets/js/jstz.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
      //Get browser timezone & set it
      // var timezone = jstz.determine();
      // var url = '/home/set_timezone';
      // $.ajax({
      //     type: "GET",
      //     url: url,
      //     data: {
      //         "timezone":timezone.name(),
      //     },
      //     success:function(results)
      //     {   
              
      //     }
      // });
  });
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_q4dK7XxTGUh2NziiU749EGP4qUoNKU&sensor=false"></script>
<script type="text/javascript"> 
	$(document).ready(function () {
		var map, location_marker, bounds, infowindow;
		var current_pin = '/assets/images/t-current-pin.png';

		initialize();
		loadMap();
	//Update map every 10 sec
	setInterval(function () {
		initialize();
	}, 10000);

	function loadMap()
	{
		var myOptions = {
			scrollwheel: false,
			navigationControl: false,
			mapTypeControl: false,
			scaleControl: false,
			draggable: true,
			//zoom: 12, 
			mapTypeId: google.maps.MapTypeId.ROAD
		};
		map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
		bounds = new google.maps.LatLngBounds();
		infowindow = new google.maps.InfoWindow();

		var d_latitude = '<?php echo $last_location['latitude'];?>';
		var d_longitude = '<?php echo $last_location['longitude'];?>';
		var username = '<?php echo $last_location['username'];?>';
		var current_latlng = new google.maps.LatLng(d_latitude, d_longitude);
		bounds.extend(current_latlng);
		location_marker = new google.maps.Marker({
			position: current_latlng,
			map: map,
			username: username,
			icon: current_pin
		});

		google.maps.event.addListener(location_marker, 'click', function() {
			var contentString = '<div class="gm-style-iw1">'+
			'<div id="siteNotice">'+
			'</div>'+'<h1 class="iw-title">Driver Name</h1>'+
			'<div id="bodyContent">	'+
			'<p><b>&nbsp;&nbsp;</b>'+username+'</p>'+
			'</div>'+
			'</div>';
			infowindow.setContent(contentString);
			infowindow.open(map, this);
		});
		
		map.fitBounds(bounds);
	}

	function initialize()
	{ 
		var device_imei = '<?php echo $last_location['imei'];?>';
		var url = '/home/livetracking_json/'+device_imei;

		$.ajax({
			type: "GET",
			url: url,
			success:function(result)
			{   
				
        var last_location = result['last_location'];
      	var d_latitude = last_location['latitude'];
      	var d_longitude = last_location['longitude'];
      	var username = last_location['imei'];
        if(last_location != null)
      	{ 
      		var current_latlng = new google.maps.LatLng(d_latitude, d_longitude);
      		bounds.extend(current_latlng);
      		location_marker.setPosition(current_latlng);

      		google.maps.event.addListener(location_marker, 'click', function() {
      			var contentString = '<div class="gm-style-iw1">'+
      			'<div id="siteNotice">'+
      			'</div>'+'<h1 class="iw-title">User Name</h1>'+
      			'<div id="bodyContent">	'+
      			'<p><b>&nbsp;&nbsp;</b>'+username+'</p>'+
      			'</div>'+
      			'</div>';
      			infowindow.setContent(contentString);
      			infowindow.open(map, this);
      		});

      		map.fitBounds(bounds);
      		map.setZoom(17);
      	}
      }
  });
	}
	google.maps.event.addDomListener(window, 'load', initialize);

});
</script>


</body>
</html>