@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
  

<form class="pb-5">
 
<div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputState">Select Vehicle</label>
      <select id="inputState" class="form-control">
        <option value="">Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-6">
     <label for="inputAddress">Select Date </label>
    <input type="date" class="form-control" id="inputAddress" placeholder="1234 Main St" required="required">
    </div>
  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputAddress">Start  Time </label>
    <input type="time" class="form-control" id="inputAddress" placeholder="1234 Main St" required="required">
    </div>
    <div class="form-group col-md-6">
     <label for="inputAddress">End  Time </label>
    <input type="time" class="form-control" id="inputAddress" placeholder="1234 Main St" required="required">
    </div>
  </div>


 


 <div class="form-group">
  <button type="submit" class="btn btn-primary" disabled="">SUBMIT</button>
</div>
</form>

<br>
<h1 style="color: #ff0000; text-align: center;"> Coming Soon... </h1>
</div>
</div>
</div>

@endsection