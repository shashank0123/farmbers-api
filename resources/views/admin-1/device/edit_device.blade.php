@extends('admin.layouts.header')
@section('content')
@if(isset($edit_data))


<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">
   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/show_device')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/update_device/'.$edit_data->device_id) }}" method="POST" role="form">
    {{ csrf_field() }}
    <legend>Edit Device</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif


    <div class="row">
     <div class="col-sm-6">
       <div class="form-group">
         <label for="">Device No.</label>
         <input type="text" class="form-control" value="{{$edit_data->device_no}}" name="device_no" id="" placeholder="Device number" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" pattern="[1-9]{1}[0-9]{14}">
       </div>                
     </div>


     <div class="col-sm-6">
       <div class="form-group">
        <label for="">Sime No.</label>
        <input type="text" class="form-control" value="{{$edit_data->sim_no}}" id="" name="sim_no" placeholder="Sim number" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="">Subscription Date</label>
        <input type="text" class="form-control datepicker" name="subscription_date" required="required" value="{{$edit_data->subscription_date}}">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="">Price</label>
        <input type="text" class="form-control" id="" name="price" placeholder="Price of Device" required="required" value="{{$edit_data->price}}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="">Sim Type</label>
        <select name="sim_type" id="" class="form-control" disabled="">
          <option value="Vodafone" @if($edit_data->sim_type == 'Vodafone'){{'selected'}}@endif>Vodafone</option>
          <option value="Airtel" @if($edit_data->sim_type == 'Airtel'){{'selected'}}@endif>Airtel</option>
        </select>
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="">Device Type</label>
        <select name="device_type" id="" class="form-control" disabled="">
          <option value="ET300"  @if($edit_data->device_type == 'ET300'){{'selected'}}@endif>ET300</option>
          <option value="BW"  @if($edit_data->device_type == 'BW'){{'selected'}}@endif>BW</option>
        </select>
      </div>
    </div>

  </div>







    <div class="row">
     
  
</div>
<button type="submit" class="btn btn-primary">Update</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->
@endif
@endsection()