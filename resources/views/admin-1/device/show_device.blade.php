@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    <div class="row">
      <div class="col-md-6">        
        @if($type=="All")
        <a class="btn btn-warning" href="{{url('admin/add_device')}}">Add device</a>
        @endif
      </div>
      <div class="col-md-6">
        <input type="search" class="form-control" name="device_search" id="device_search" placeholder="Search Device" onkeyup="getDynamicDevice()" style="width: 80%">        
      </div>
    </div>
    <hr> 
    <h4>{{$type}} Devices</h4>
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Device No.</th>
       <th>Sim Number</th>
       <th>Sim Operator</th>
       <th>Device Type</th>
       <th>Subscription Date</th>
       <th>Created At</th>
       <th>Activity</th>
     </tr>
   </thead>
   <thead id="device-section">
    @php $i=1; @endphp
    @foreach($device as $d)
    <tr>
      <?php if (!isset($page)) $page = 0;?>
      <td>{{$i}}</td> 
      <td>{{$d->device_no}}</td>
      <td>{{$d->sim_no}}</td>
      <td>{{$d->sim_type}}</td>
      <td>{{$d->device_type}}</td>
      <td>{{$d->subscription_date}}</td>
      <td>{{explode(' ',$d->created_at)[0]}}</td>

      <td>
        <?php if ($d->device_status == 'active'){?>
          <a href="{{url('admin/bar_device/'.$d->device_id)}}" class="btn btn-success  mb-1 ml-1">Bar</a> 
        <?php }?>

        <?php if ($d->device_status == 'barred'){?>
          <a href="{{url('admin/unbar_device/'.$d->device_id)}}" class="btn btn-success  mb-1 ml-1">Unbar</a> 
        <?php }?>
        <a href="{{url('admin/edit_device/'.$d->device_id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 

        <form action="{{url('admin/delete_device/'.$d->device_id)}}" method="post">
          {{csrf_field()}}
          <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$d->device_id}})"><i class="fa fa-remove"></i></button>
          <input type="submit" hidden="hidden" id="form{{$d->device_id}}" class="btn btn-danger btn-xs">
        </form>
      </td>
    </tr>
    @php $i++ @endphp
    @endforeach
  </thead>
</table>	

</div>

</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">
  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }

  function getDynamicDevice(){
    var search = $('#device_search').val();
  // alert(search);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-device',
   type: 'POST',
   data: {_token: CSRF_TOKEN, search: search },
   success: function (data) {
     $('#device-section').empty();
     $('#device-section').html(data);
   },
   failure: function (data) {
     alert('Something Went Wrong');
   }
 });
  }
</script>
@endsection