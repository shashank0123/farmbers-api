@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/show_device')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/store_device') }}" method="POST" >
    {{ csrf_field() }}
    <legend>Add Device</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">
     <div class="col-sm-6">
       <div class="form-group">
         <label for="device_no">Device No.</label>
         <input type="tel" class="form-control" name="device_no" id="device_no" placeholder="Device number (15 digits)" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required="required" pattern="[1-9]{1}[0-9]{14}" onchange="checkDevice()">
                         <p style="color: red" id="device-error"> </p>
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
        <label for="sim_no">Sim No.</label>
        <input type="tel" class="form-control" id="sim_no" name="sim_no" placeholder="Sim number" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onchange="checkSim()">
                         <p style="color: red" id="sim-error"> </p>
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="subscription_date">Subscription Date</label>
        <input type="text" class="form-control datepicker" name="subscription_date" id="subscription_date" required="required" value="<?php echo date('d/m/Y')?>">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="price">Price</label>
        <input type="number" class="form-control" id="price" name="price" placeholder="Price of Device" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="">Sim Type</label>
        <select name="sim_type" id="" class="form-control">
          <option value="Vodafone">Vodafone</option>
          <option value="Airtel">Airtel</option>
        </select>
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="">Device Type</label>
        <select name="device_type" id="" class="form-control">
          <option value="ET300">ET300</option>
          <option value="BW">BW</option>
        </select>
      </div>
    </div>

  </div>
  <button type="submit" id="submit" class="btn btn-primary">Add</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">
  
 function checkDevice(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var device = $('#device_no').val();
   
    $.ajax({
    type: "POST",
    url: "/admin/checkDevice",
    data:{ _token: CSRF_Token, device: device},
    success:function(msg){
     if(msg.message == 'Device Already Exist'){
        $('#device-error').text(msg.message);
        $('#submit').attr('disabled',true);
     }     
     else{
        $('#device-error').empty();      
        $('#submit').prop('disabled',false);
     }
    }
  });
  }

  function checkSim(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var sim = $('#sim_no').val();
   
    $.ajax({
    type: "POST",
    url: "/admin/checkSim",
    data:{ _token: CSRF_Token, sim: sim},
    success:function(msg){
     if(msg.message == 'Sim Number Already Exist'){
        $('#sim-error').text(msg.message);
        $('#submit').prop('disabled',true);
     }     
     else{
        $('#sim-error').empty();      
        $('#submit').attr('disabled',false);
     }
    }
  });
  }

</script>

@endsection