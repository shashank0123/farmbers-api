@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">
   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/all-complaints')}}">Back</a> <br><br>
    <legend>Reply To Complaint</legend>
  <hr>
  <form action="{{ url('admin/reply/'.$complaint->id) }}" method="POST" role="form">
    {{ csrf_field() }}
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">
     <div class="col-sm-12">
       <div class="form-group">
         <label for="name">User</label>
         <input type="text" class="form-control" value="{{$complaint->name}}" name="name" id="name" placeholder="" required="required" readonly="readonly">
       </div>                
     </div>

     <div class="col-sm-12">
       <div class="form-group">
         <label for="vehicle_no">Vehicle No</label>
         <input type="text" class="form-control" value="{{$complaint->vehicle_no}}" name="vehicle_no" id="vehicle_no" placeholder="" required="required" readonly="readonly">
       </div>                
     </div>

     <div class="col-sm-12">
       <div class="form-group">
         <label for="complaint">Complaint</label>
         <input type="text" class="form-control" value="{{$complaint->complaint}}" name="complaint" id="complaint" placeholder="" required="required" readonly="readonly">
       </div>                
     </div>


     <div class="col-sm-12">
       <div class="form-group">
        <label for="reply">Reply</label>
        <textarea class="form-control" name="reply" id="reply" required="required" rows='5'>@if(!empty($complaint->reply)){{$complaint->reply}}@endif</textarea>
      </div>
    </div>
  
</div>
<button type="submit" class="btn btn-primary">Update</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->

@endsection()