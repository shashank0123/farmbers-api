@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
  <style type="text/css">
    .Profile-sec h4{ color: blue; font-size: 18px; margin: 20px 0; }
  </style>

<form class=" Profile-sec pb-5">
 
  <div class="form-row">
    <div class="form-group mx-auto my-3">
   <img class="img-profile rounded-circle text-center" src="img/nk.jpg" width="60px" height="60px">
  </div>
</div>

 <h4>Edit Profile</h4>
<div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Name</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Name">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Mobile No</label>
      <input type="text" class="form-control" id="inputPassword4" placeholder="Mobile No">
    </div>
  </div>

<div class="form-row">
    <div class="form-group col-md-6">
     <label for="inputAddress">Date of Birth </label>
    <input type="date" class="form-control" id="inputAddress" placeholder="1234 Main St">
    </div>
    <div class="form-group col-md-6">
      <div class="form-group">
    <label for="inputState">Gender</label>
      <select id="inputState" class="form-control">
        <option selected>Choose Gender</option>
        <option>Male</option>
        <option>Female</option>
      </select>
  </div>

  </div>

</div>

  <div class="form-group">
   
        <button type="submit" class="btn btn-primary">Update Profile</button>
  </div>



<h4 >Change Password</h4>

<div class="form-group">
    <label for="inputAddress2"> Old Password </label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Old Password">
  </div>

 <div class="form-group">
    <label for="inputAddress2"> New Password </label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="New Password">
  </div>

<div class="form-group">
    <label for="inputAddress2"> Confirm Password </label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Confirm Password">
  </div>
  

  <!-- <div class="form-group">
    <label for="inputState">Choose Language</label>
      <select id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
  </div> -->


  <div class="form-group">
   
        <button type="submit" class="btn btn-primary">Update Password</button>
  </div>
  



  

</form>

</div>
</div>
</div>

@endsection