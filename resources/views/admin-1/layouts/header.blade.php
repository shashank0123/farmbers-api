<?php
use App\Profile;
$profile = Profile::where('user_id',Auth()->user()->id)->first();
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <script src="https://js.pusher.com/3.0/pusher.min.js"></script>
  

  <meta name="author" content="">

  <title>FAST TRACK</title>

  <!-- Custom fonts for this template-->
  <link href="{{url('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{url('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i')}}" rel="stylesheet">

  <meta name="csrf-token" content="{{csrf_token()}}">  

  <!-- Custom styles for this template-->
  <link href="{{url('/css/sb-admin-2.min.css')}}" rel="stylesheet"> 
  <link href="{{url('/css/chosen.css')}}" rel="stylesheet"> 
  <link href="{{url('/css/sb-admin-2.css')}}" rel="stylesheet"> 
  <link href="/css/select2.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
  <style>
  .table tr td {
    vertical-align: middle;
  }
</style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('admin/admin_dashboard')}}">
        
        <div class="sidebar-brand-text mx-3"> FAST TRACK </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      




      <li class="nav-item active">
        <a class="nav-link" href="{{url('home')}}">
          &nbsp;<i class="fa fa-list-alt" aria-hidden="true"></i>
          
          <span>Dashboard</span></a>

        </li>

        <li class="nav-item">
          <a class="nav-link" href="/admin/registration">
            <img src="/side-icon/complaint.png" style="width: 20px; height: 20px">
            <span>Registration</span></a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{url('admin/bulletins-list')}}">
              <img src="/side-icon/complaint.png" style="width: 20px; height: 20px">
              
              <span>Bulletin</span></a>

            </li>


            <li class="nav-item ">
              <a class="nav-link" onclick="showDevice()">
                <img src="/side-icon/device.png" style="width: 20px; height: 20px">
                {{-- <i class="fa fa-desktop"></i> --}}
                <span>Device Management</span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                <div class="dropdown-device" style="display: none;" id="dropdown">
                  <ul>
                    <li><a href="{{url('admin/add_device')}}"><i class="fa fa-angle-right"></i> Add Device </a></li>
                    <li><a href="{{url('admin/assign_device')}}"><i class="fa fa-angle-right"></i> Assign Device </a></li>
                    <li><a href="{{url('admin/show_device')}}"><i class="fa fa-angle-right"></i> Devices List</a></li>
                    <li><a href="{{url('admin/show_barred_device')}}"><i class="fa fa-angle-right"></i> Barred Device</a></li>
                    <li><a href="{{url('admin/assigned_device_list')}}"><i class="fa fa-angle-right"></i> Assigned Devices List</a></li>
                  </ul>

                </li>



                <li class="nav-item ">
                  <a class="nav-link" onclick="showUser()">
                    <img src="/side-icon/user-profile.png" style="width: 20px; height: 20px">
                    {{-- <i class="fas fa-fw fa-car"></i> --}}
                    <span>User Management</span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                    <div class="dropdown-user" style="display: none;" id="dropdown">
                      <ul>
                        <li><a href="{{url('admin/user-list')}}/{{'user'}}"><i class="fa fa-angle-right"></i> Main Users List</a></li>
                        <li><a href="{{url('admin/add-user')}}/{{'user'}}"><i class="fa fa-angle-right"></i> Add Main User </a></li>

                        <li><a href="{{url('admin/user-list')}}/{{'sub-user'}}"><i class="fa fa-angle-right"></i> Sub Users List</a></li>
                        <li><a href="{{url('admin/add-user')}}/{{'sub-user'}}"><i class="fa fa-angle-right"></i> Add Sub User </a></li>
                        <li><a href="{{url('admin/barred_user_list')}}"><i class="fa fa-angle-right"></i> Barred Users</a></li>
                        <li><a href="{{url('admin/logged_in_users')}}"><i class="fa fa-angle-right"></i> Logged In Users</a></li>
                        <li><a href="{{url('admin/active_app_users')}}"><i class="fa fa-angle-right"></i> Active App Users</a></li>
                      </ul>
                    </div>
                  </li>
                  


                  <li class="nav-item ">
                    <a class="nav-link" onclick="showVehicle()">
                      <img src="/side-icon/vehicle.png" style="width: 20px; height: 20px">
                      {{-- <i class="fas fa-fw fa-car"></i> --}}
                      <span>Vehicle Management</span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                      <div class="dropdown-vehicle" style="display: none;" id="dropdown">
                        <ul>
                          <li><a href="{{url('admin/add_vehicals')}}"><i class="fa fa-angle-right"></i> Add Vehicle </a></li>
                          <li><a href="{{url('admin/add_driver')}}"><i class="fa fa-angle-right"></i> Add Driver </a></li>
                          <li><a href="{{url('admin/show_vehicals')}}"><i class="fa fa-angle-right"></i> Vehicles List</a></li>
                          <li><a href="{{url('admin/show_driver')}}"><i class="fa fa-angle-right"></i> Drivers List</a></li>
                          <li><a href="{{url('brands/create')}}"><i class="fa fa-angle-right"></i> Add Brand </a></li>
                          <li><a href="{{url('brands')}}"><i class="fa fa-angle-right"></i> Brands List</a></li>
                          <li><a href="{{url('admin/assign_vehicle_to_user')}}"><i class="fa fa-angle-right"></i> Assign Vehicle To User </a></li>
                          <li><a href="{{url('admin/assigned_users_list')}}"><i class="fa fa-angle-right"></i> Assigned Vehicles </a></li>
                          <li><a href="{{url('admin/assign_driver')}}"><i class="fa fa-angle-right"></i> Assign Driver </a></li>
                          <li><a href="{{url('admin/assigned_list')}}"><i class="fa fa-angle-right"></i> Assigned Drivers List</a></li>
                          <li><a href="{{url('admin/not_working_vehicles')}}"><i class="fa fa-angle-right"></i> Not Working Vehicles</a></li>
                          <li><a href="{{url('admin/stopped_vehicles')}}"><i class="fa fa-angle-right"></i> Stopped Vehicles</a></li>
                        </ul>
                      </div>
                    </li>


                    <li class="nav-item ">
                      <a class="nav-link" onclick="showFranch()">
                        <img src="/side-icon/franchise.png" style="width: 20px; height: 20px">
                        {{-- <i class="fa fa-desktop"></i> --}}
                        <span>Franchisee </span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                        <div class="dropdown-franch" style="display: none;" id="dropdown">
                          <ul>
                            <li><a href="{{url('admin/add_franchisee')}}"><i class="fa fa-angle-right"></i> Add Franchisee </a></li>
                            <li><a href="{{url('admin/show_franchisees')}}"><i class="fa fa-angle-right"></i> Franchisees List</a></li>

                          </ul>
                        </div>
                      </li>


                      <li class="nav-item ">
                        <a class="nav-link" onclick="showRsa()">
                          <img src="/side-icon/RSA.png" style="width: 20px; height: 20px">
                          {{-- <i class="fa fa-desktop"></i> --}}
                          <span>RSA </span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                          <div class="dropdown-rsa" style="display: none;" id="dropdown">
                            <ul>
                              <li><a href="{{url('admin/add_rsa')}}"><i class="fa fa-angle-right"></i> Add RSA </a></li>
                              <li><a href="{{url('admin/list')}}"><i class="fa fa-angle-right"></i> RSA List</a></li>

                            </ul>
                          </div>
                        </li>




                        <li class="nav-item ">
                          <a class="nav-link" onclick="showPayment()">
                            <img src="/side-icon/rupee.png" style="width: 20px; height: 20px">
                            {{-- <i class="fas fa-fw fa-car"></i> --}}
                            <span>Payments</span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                            <div class="dropdown-payment" style="display: none;" id="dropdown">
                              <ul>
                                <li><a href="{{url('admin/renewals')}}"><i class="fa fa-angle-right"></i> Renewals </a></li>
                                
                              </ul>
                            </div>
                          </li>


                          <li class="nav-item ">
                            <a class="nav-link" onclick="showReport()">
                              <img src="/side-icon/statistics.png" style="width: 20px; height: 20px">
                              {{-- <i class="fas fa-fw fa-car"></i> --}}
                              <span>Report Management</span><span class="pull-right"><i class="fa fa-angle-right"></i></span></a>
                              <div class="dropdown-report" style="display: none;" id="dropdown">
                                <ul>
                                  <li><a href="/admin/distance"><i class="fa fa-angle-right"></i> Distance Report </a></li>
                                  <li><a href="/admin/trip"><i class="fa fa-angle-right"></i> Trip Report </a></li>

                                </ul>
                              </div>
                            </li>
                            
                            

                            
                            <li class="nav-item">
                              <a class="nav-link" href="/admin/all-activities">
                                <img src="/side-icon/activity-log.png" style="width: 20px; height: 20px">
                                {{-- <i class="fa fa-file" aria-hidden="true"></i> --}}
                                <span>All Activities</span></a>
                              </li>

                              <li class="nav-item">
                                <a class="nav-link" href="/admin/all-complaints">
                                  <img src="/side-icon/complaint.png" style="width: 20px; height: 20px">
                                  {{-- <i class="fa fa-comments" aria-hidden="true"></i> --}}
                                  <span>Complaint</span></a>
                                </li>

                                <li class="nav-item">
                                  <a class="nav-link" href="/admin/history">
                                    <img src="/side-icon/history-tracking.png" style="width: 20px; height: 20px">
                                    {{-- <i class="fa fa-car" aria-hidden="true"></i> --}}
                                    <span>History Tracking</span></a>
                                  </li>

                                  <li class="nav-item">
                                    <a class="nav-link" href="/admin/subadmin">
                                      <img src="/side-icon/user-profile.png" style="width: 20px; height: 20px">
                                      
                                      <span>Sub Admin</span></a>
                                    </li>


     <!--   <li class="nav-item">
        <a class="nav-link" href="/distance">
          <img src="/side-icon/distance-report.png" style="width: 20px; height: 20px">
       <i class="fa fa-road" aria-hidden="true"></i>
          <span>Distance report</span></a>
      </li>

    -->
    


    


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->

    

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle" onclick="hideNavbar()"></button>
    </div>
  </li>
</ul>

<!-- End of Sidebar -->




<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">


  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-gradient-primary topbar static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>

      <!-- Topbar Search -->
      <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" method="GET" action="{{url('admin/search')}}" >
        
        <div class="input-group">
          @csrf
          <select name="search_type" id="search_type" class="form-group" required="required">
            <option value=""> -- Select -- </option>
            <option value="user">User</option>
            <option value="driver">Driver</option>
            <option value="vehicle">Vehicle</option>
            <option value="rsa">RSA</option>
            <option value="complaint">Complaint</option>  
            <option value="all">All</option>  
          </select> &nbsp;&nbsp;

          <input style="background-color: #ffffff; opacity: 1;" type="text" class="form-control border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" name="keyword" id="inputserch" required="required">
          <div class="input-group-append">
            <button  style="background-color: #3889f7; opacity: 0.7;"class="btn" type="submit">
              <i class=" text-white fas fa-search fa-sm"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>

        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <!-- <i class="fas fa-bell fa-fw"></i> -->
            <!-- Counter - Alerts -->
            <!-- <span class="badge badge-success  badge-counter">3+</span> -->
          </a>
          <!-- Dropdown - Alerts -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
              Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-primary">
                  <i class="fas fa-file-alt text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 12, 2019</div>
                <span class="font-weight-bold">A new monthly report is ready to download!</span>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-success">
                  <i class="fas fa-donate text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 7, 2019</div>
                $290.29 has been deposited into your account!
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
              <div class="mr-3">
                <div class="icon-circle bg-warning">
                  <i class="fas fa-exclamation-triangle text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 2, 2019</div>
                Spending Alert: We've noticed unusually high spending for your account.
              </div>
            </a>
            <a href="/all-notifications" class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
          </div>
        </li>

        <!-- Nav Item - Messages -->
           <!--  <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class=" color-light fas fa-envelope fa-fw"></i>
              
                <span class="badge badge-success  badge-counter">7</span>
              </a>
            
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li> -->


            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="/admin/complaint" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-question-circle fa-fw"></i>
                <span class="mr-2 d-none d-lg-inline text-light small">&nbsp;Help</span>
                
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                 
                  Basic 
                </a>
                <a class="dropdown-item" href="#">
                 
                  Advanced
                </a>
                
                <div class="dropdown-divider"></div>
                
              </div>
            </li>
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown arrow">
              <a class="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" onclick="showSideNav()">
                <span class="mr-2 d-none d-lg-inline text-light small">{{Auth::user()->name}}</span>
                <img class="img-profile" id="img-profile" src="@if(!empty($profile)){{url('/images/profile/'.$profile->profile_pic)}}@else{{asset('/side-icon/user-icon.png')}}@endif" >
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdow" id="userDropdow">
                <a class="dropdown-item" href="/admin/profile">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                
                <a class="dropdown-item" href="/admin/reset-password">
                  {{-- <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i> --}}
                  <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                  Change Password
                </a>
                
                <div class="dropdown-divider"></div>
                <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display:;">
                  @csrf
                  <a class="dropdown-item" href="{{url('logout')}}" data-toggle="modal" data-target="#logoutModal">
                  </form>
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
        <div class="showNav" onclick="showNavbar()"><img src="{{asset('side-icon/hamburger.png')}}" class="hamburger" style="height: 30px; width: auto; padding: 5px 25px; display: none;"></div>
        
        @yield('content')



        
        <!-- Footer -->
        <footer  class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; Fasttrack GPS 2019</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:;">
              @csrf
              <input type="submit" class="btn btn-primary" href="{{url('logout')}}" value="Logout" />
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="/vendor/chart.js/Chart.min.js"></script>

    
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="/js/demo/chart-area-demo.js"></script>
  <script src="/js/demo/chart-pie-demo.js"></script>
  <script src="/js/demo/select2.js"></script>
  {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6Q-ffZFitzvFsAgEuqJ7JLmFhCKV6TCQ"></script> --}}
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_q4dK7XxTGUh2NziiU749EGP4qUoNKU"></script>
  <script>

    function showRsa(){
      $('.dropdown-rsa').toggle();
    }

    function showSideNav(){
      $('#userDropdow').toggle();
    }

    function showDevice(){
      $('.dropdown-device').toggle();
    }    
    function showVehicle(){
      $('.dropdown-vehicle').toggle();
    }
    function showUser(){
      $('.dropdown-user').toggle();
    }

    function showPayment(){
      $('.dropdown-payment').toggle();
    }

    function showReport(){
      $('.dropdown-report').toggle();
    }
    function showFranch(){
      $('.dropdown-franch').toggle();
    }

    
    
    $(document).ready(function(){
      var date_input=$('.datepicker'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    });
    


    // var username;

// reference for DOM nodes
// var saveNameButton = document.getElementById('saveNameButton');
// var saveNameBox = document.getElementById('name-box');
// var nameInput = document.getElementById('name');
// var welcomeHeading = document.getElementById('welcome-message');
// var deliveryHeroBox = document.getElementById('delivery-hero-box');

// saveNameButton.addEventListener('click', saveName);

// all functions, event handlers
// function saveName (e) {
//   var input = nameInput.value;
//   if (input && input.trim()) {
//     username = input;

//     // hide the name box
//     saveNameBox.classList.add('hidden');

//     // set the name
//     welcomeHeading.innerHTML = 'Hi! <strong>' + username +
//     (mode === 'user'
//       ? '' 
//       : '');
//     // show the delivery hero's div now
//     deliveryHeroBox.classList.remove('hidden');
//   }
//   return;
// }

function hideNavbar(){
  $('#accordionSidebar').hide();
  $('.hamburger').show();
}

function showNavbar(){
  $('#accordionSidebar').show();
  $('.hamburger').hide();
}

</script>
@yield('script')

</body>

</html>
