<?php use App\User; ?>
@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <hr> 
    <legend>All renewals</legend>
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>User Name</th>
       <th>Mobile</th>
       <th>Created At</th>
       <th colspan="2">Action</th>
     </tr>
   </thead>
   <thead>
    @php $i=1; @endphp
    @if(!empty($devices))
    @foreach($devices as $device)
    <tr>
     <td>{{$i}}</td> 
     <td>{{$device->name}}</td>
     <td>@if(!empty($device->mobile)){{$device->mobile}}@endif</td>    
    <td><?php
            $device->subscription_date = str_replace('/', '-', $device->subscription_date);
            $diff = date_diff(date_create($device->subscription_date),date_create(date("d-m-Y")));

?><?php print_r(($diff->y*365)+($diff->m*30)+$diff->d); ?> Days</td>
<td><a href="{{url('admin/edit_device/'.$device->device_id)}}" class="btn btn-primary">Renew</a></td>
 
      
  </tr>
  @php $i++ @endphp
  @endforeach
  @endif
</thead>
</table>	

</div>

</div>
</div>
<!-- Begin Page Content -->


@endsection()