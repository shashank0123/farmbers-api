@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <div class="row">
      <div class="col-md-6">
        <a class="btn btn-warning" href="{{url('admin/complaint')}}">Add Complaint</a>
      </div>
      <div class="col-md-6">
        <input type="search" class="form-control" name="complaint_search" id="complaint_search" placeholder="Search by Name" onkeyup="getComplaintSearch()" style="width: 80%" />       
      </div>
    </div>
   
    <hr> 
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>User Name</th>
       <th>Mobile</th>
       <th>Vehicle Number</th>
       <th>Complaint</th>
       <th>Status</th>
       <th colspan="2">Action</th>
     </tr>
   </thead>
   <thead id="complaint-section">
    @php $i=1; @endphp
    @foreach($complaints as $complaint)
    <tr>
     <td>{{$i++}}</td> 
     <td>{{$complaint->name}}</td>
     <td>{{$complaint->mobile}}</td>
     <td>{{$complaint->vehicle_no}}</td>
     <td>{{$complaint->complaint}}</td>
    <td>{{$complaint->status}}</td>
    <td>

      @if($complaint->status == 'Open')
          <a href="{{url('admin/complaint-open/'.$complaint->id)}}" class="btn btn-primary">Close</a>
        @endif

        @if($complaint->status == 'Close')
          <a href="{{url('admin/complaint-close/'.$complaint->id)}}" class="btn btn-success">Open</a>
        @endif

       <a href="{{url('/admin/reply/'.$complaint->id)}}" class="btn btn-secondary btn-xs">@if($complaint->reply){{'Replied'}}@else{{'Reply'}}@endif</a>
       
       
       <form action="{{url('/admin/complaint/'.$complaint->id)}}" method="post">
        {{csrf_field()}}
        <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$complaint->id}})"><i class="fa fa-remove"></i></button>
        <input type="submit" hidden="hidden" id="form{{$complaint->id}}" class="btn btn-danger btn-xs">
      </form>
      
    </td>
  </tr>
  @endforeach
</thead>
</table>	

</div>
<div class="row">
 
</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">

function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }

  function getComplaintSearch(){
    var search = $('#complaint_search').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/search-complaint',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search },
     success: function (data) {
       $('#complaint-section').empty();
       $('#complaint-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }

</script>
@endsection