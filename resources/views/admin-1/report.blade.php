@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    @if($side=="distance")
    <h3>{{$type}}</h3>
    <p>Vehicle Number : <b>{{$vehicle->vehicle_no}}</b><br>
      Distance Report from <b>{{$from}}</b> to <b>{{$to}}</b><br>
      Total Distance : {{$total_distance}}
    </p>
    <br>

    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th></th>  
       <th>Date</th>
       <th>Distance</th>   
     </tr>
   </thead>
   <tbody>
    @if(!empty($lines))
    @foreach($lines as $line)

    <tr>
      <td></td>
      <th>{{$line->date ?? ' '}}</th>
      <td>{{$line->distance ?? ' '}}</td>
    </tr>

    @endforeach
    @endif
  </tbody>
</table>

@endif


@if($side=="trip")

  <h3>{{$type}}</h3>
    <p>Vehicle Number : <b>{{$vehicle->vehicle_no}}</b><br>
      Trip Report from <b>{{$from}}</b> to <b>{{$to}}</b><br>
      Total Distance : {{$total_distance}}
    </p>
    <br>

    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th></th>  
       <th>Date</th>
       <th>Location</th>   
       <th>Timing(From)</th>   
       <th>Timing(To)</th>   
       <th>Distance</th>   
     </tr>
   </thead>
   <tbody>
    @if(!empty($lines))
    @foreach($lines as $line)

    <tr>
      <td></td>
      <th>{{explode(' ',$line->date)[0]}}</th>
      <th>Location</th>
      <th>{{explode(' ',$line->start_time)[1]}}</th>
      <th>{{explode(' ',$line->end_time)[1]}}</th>
      <td>{{$line->distance}}</td>
    </tr>

    @endforeach
    @endif
  </tbody>
</table>


@endif
 

</div>
</div>
</div>

@endsection