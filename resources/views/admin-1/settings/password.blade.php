@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  
  <div class="row">
  
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    <legend>Reset Password</legend>
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    <br>

    <form class="pb-5" action="/admin/reset-password/{{Auth()->user()->id}}" method="POST">

      @csrf

     <div class="form-group">
      <label for="password"> New Password </label>
      <input type="password" class="form-control" id="password" name="password" placeholder="New Password">
    </div>

    <div class="form-group">
      <label for="cpass"> Confirm New Password </label>
      <input type="password" class="form-control" id="cpass" name="cpass" placeholder="Confirm New Password">
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary">Reset</button>
    </div>

  </form>

</div>
</div>
</div>

@endsection