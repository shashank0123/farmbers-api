@extends('admin.layouts.header')
@section('content')



<style type="text/css">
  .s-sec-1{ margin-bottom: 85px; }
  .setting-head p { background-color: #00000021; padding: 20px 5px 5px 5px; font-size: 16px ; font-weight: bold; }
  .setting-sec-lr h6{ font-size: 18px; color: blue; font-weight: bold; }
  .setting-sec-lr p{ font-size: 14px;  }
</style>

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
  
<div class="setting-sec pb-5">
 

 <div class="s-sec-1"> 
  <div class="setting-head"> 
    <p class="">VOLUME</p>
  </div>
   <div class="setting-sec-lr">
    <div class="float-left"><h6>Select Volume</h6> <p>Enable/Disable App Sound </p></div>
    <div class="float-right"><label class="switch">
      <input type="checkbox" checked>
      <span class="slider round btn-sm"></span>
    </label>
  </div>
 </div>
</div>

<div class="s-sec-1"> 
  <div class="setting-head"> 
    <p class="">NOTIFICATION SETTING</p>
  </div>
   <div class="setting-sec-lr">
    <div class="float-left"><h6> Notification Sound</h6> <p>Enable/Disable Notification Sound </p></div>
    <div class="float-right"><label class="switch">
      <input type="checkbox" checked>
      <span class="slider round"></span>
    </label>
  </div>
 </div>
</div>


<div class="s-sec-1"> 
  <div class="setting-head"> 
    <p class="">MAP DISPLAY SETTING</p>
  </div>
   <div class="setting-sec-lr">
    <div class="float-left"><h6> Select the map dispaly</h6> <p>Select the map View </p></div>
   
 </div>
</div>

<div class="s-sec-1"> 
  <div class="setting-head"> 
    <p class="">REFERESH TIMING </p>
  </div>
   <div class="setting-sec-lr">
    <div class="float-left"><h6> Refresh Interval </h6> <p>Set the device list refresh interval</p></div>
   
 </div>
</div>





</div>




</div>
</div>
</div>

@endsection