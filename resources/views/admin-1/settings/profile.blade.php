@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    <legend>User Profile</legend>
    @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
  
<br><br>
<form class="pb-5" action="/admin/profile-update/{{Auth()->user()->id}}" method="POST" enctype="multipart/form-data">

  @csrf  

<div class="form-row">
  @if(!empty($profile ))
  @if($profile->profile_pic)
  <div class="form-group col-md-4">
   <img class="img-profile text-center" src="{{url('/images/profile/'.$profile->profile_pic)}}" width="100%" height="auto" style="margin-top: 50px">    
  </div>
  @endif
  @endif

    <div class="form-group col-md-8">
      
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{$user->name}}">
    </div>
    <div class="form-group">
      <label for="mobile">Mobile No</label>
      <input type="tel" class="form-control" name="mobile" id="mobile" placeholder="Mobile No" value="@if(!empty($user->mobile)){{$user->mobile}}@endif" pattern="[6-9]{1}[0-9]{9}">
    </div>
 
    <div class="form-group">
     <label for="dob">Date of Birth </label>
    <input type="date" class="form-control" name="dob" id="dob" placeholder="Date of birth"  value="@if(!empty($profile))@if(!empty($profile->dob)){{$profile->dob}}@endif @endif">
    </div>

      <div class="form-group">
    <label for="gender">Gender</label>
      <select id="gender" name="gender" class="form-control" placeholder="Gender" required="required">
        <option selected>Choose Gender</option>
        <option value="Male" @if(!empty($profile)) @if(!empty($profile->gender && $profile->gender == "Male")) {{'selected'}}@endif @endif>Male</option>
        <option value="Female" @if(!empty($profile)) @if(!empty($profile->gender && $profile->gender == "Female")) {{'selected'}}@endif @endif>Female</option>
      </select>
  </div>

    <div class="form-group">
     <label for="profession">Profession </label>
    <input type="text" class="form-control" name="profession" id="profession" placeholder="Profession" value="@if(!empty($profile)) @if(!empty($profile->profession)){{$profile->profession}}@endif @endif">
    </div>

     <div class="form-group">
     <label for="profile_pic">Profile Picture </label>
    <input type="file" class="form-control" name="profile_pic" id="profile_pic"  placeholder="Profile Picture">
    </div>

    <div class="form-group">   
        <button type="submit" class="btn btn-primary">Save</button>
  </div>
  
  </div>

  <!-- <div class="form-group">
    <label for="inputState">Choose Language</label>
      <select id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
  </div> -->

  
  



  

</form>

</div>
</div>
</div>

@endsection