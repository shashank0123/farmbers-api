<?php
use App\Brand;
$uc=0;
$vc=0;
$dc=0;
$rc=0;
$cc=0;
?>

@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <!-- For Users -->
    <hr> 
    @if(!empty($users))
    Search for <i>'{{$keyword}}'</i> in user
    <br>
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>User Name</th>
       <th>Email</th>
       <th>Mobile</th>
       <th>Created At</th>
       <th></th>
     </tr>
   </thead>
   <thead>
    @php $i=1; @endphp
    @foreach($users as $user)
    @php $uc++; @endphp
    <tr>
      <td>{{$i++}}</td>
      <td>@if(!empty($user->user_name)){{$user->name}}@endif</td>
      <td>@if(!empty($user->email)){{$user->email}}@endif</td>
      <td>@if(!empty($user->mobile)){{$user->mobile}}@endif</td>
      <td>{{$user->created_at}}</td>
      <td><a href="{{url('admin/edit_user/sub-user/'.$user->id)}}" class="btn btn-success  mb-1 ml-1">View</td>
      </tr>
      @endforeach

    </thead>
  </table>  
@if($uc == 0)
<p style="text-align: center;">No result found</p>
@endif
  @endif

  <!-- For Vehicles -->

  @if(!empty($vehicles))

  <br><br>
  Search for <i>'{{$keyword}}'</i> in vehicles
  <br>
  <table class="table table-bordered">
   <thead>
    <tr class="alert-info">
     <th>SN</th>  
     <th>Name</th>
     <th>Brand</th>   
     <th>Type</th>
     <th>Vehicle No</th>                              
     <th>Owner</th>
     <th>Image</th>
     <th></th>
   </tr>
 </thead>
 <thead>
  @php $i=1; @endphp
  @foreach($vehicles as $v)
  @php $vc++; @endphp
  <tr>
    <?php $brand = Brand::where('id',$v->vehicle_brand)->first();  ?>

    <td>{{$i++}}</td> 
    <td>{{$v->vehicle_name }}</td>
    <td>{{$brand->brand_name ?? "" }}</td>
    <td>{{$v->vehicle_type }}</td>
    <td>{{$v->vehicle_no }}</td>
    <td>{{$v->owner_id }}</td>
    <td>
      @if($v->vehicle_image != Null)
      <img src="{{url('/upload/'.$v->vehicle_image)}} " style="width: 50px;height: 50px;">
      @endif
    </td>
    <td><a href="{{url('/admin/edit_vehicals/'.$v->vehicle_id)}}" class="btn btn-success  mb-1 ml-1">View</a></td>
  </tr>
  @endforeach

</thead>
</table>  
@if($vc == 0)
<p style="text-align: center;">No result found</p>
@endif
@endif


<!-- For Drivers -->


@if(!empty($drivers))

<br><br>
Search for <i>'{{$keyword}}'</i> in drivers
<br>
<table class="table table-bordered">
 <thead>
  <tr class="alert-info">
   <th>SNo</th> 
   <th>Name</th>
   <th>Phone</th>
   <th>Email</th>
   <th>Date Of Birth</th>
   <th>Gender</th>
   <th></th>
 </tr>
</thead>
<thead>
  @php $i=1; @endphp
  @foreach($drivers as $dr)
  @php $dc++; @endphp
  <tr>
    <td>{{$i++}}</td> 
    <td>{{$dr->driver_name}}</td>
    <td>{{$dr->driver_phone}}</td>
    <td>{{$dr->driver_email}}</td>
    <td>{{$dr->driver_dob}}</td>
    <td>{{$dr->driver_gander}}</td>
    <td><a href="{{url('/admin/edit_driver/'.$dr->driver_id)}}" class="btn btn-success  mb-1 ml-1">View</a> 
  </td>
</tr>
@endforeach

</thead>
</table>  
@if($dc == 0)
<p style="text-align: center;">No result found</p>
@endif
@endif


<!-- for complaints -->
@if(!empty($complaints))

<br><br>
Search for <i>'{{$keyword}}'</i> in complaints
<br>
<table class="table table-bordered">
 <thead>
  <tr class="alert-info">
   <th>SNo</th> 
   <th>Name</th>
   <th>Mobile</th>
   <th>Vehicle Number</th>
   <th>Complaint</th>
 </tr>
</thead>
<thead>
  @php $i=1; @endphp
  @foreach($complaints as $dr)
  @php $cc++; @endphp
  <tr>
   
    <td>{{$i++}}</td> 
    <td>{{$dr->name}}</td>
    <td>{{$dr->mobile}}</td>
    <td>{{$dr->vehicle_no}}</td>
    <td>{{$dr->complaint}}</td>
   
  </td>
</tr>
@endforeach

</thead>
</table>  
@if($cc == 0)
<p style="text-align: center;">No result found</p>
@endif
@endif


<!-- for RSA -->

@if(!empty($rsas))

<br><br>
Search for <i>'{{$keyword}}'</i> in RSA
<br>
<table class="table table-bordered">
 <thead>
  <tr class="alert-info">
   <th>SNo</th> 
   <th>Company</th>
   <th>Contact</th>   
 </tr>
</thead>
<thead>
  @php $i=1; @endphp
  @foreach($rsas as $dr)
  @php $rc++; @endphp
  <tr>   
    <td>{{$i++}}</td> 
    <td>{{$dr->company}}</td>
    <td>{{$dr->contact}}</td>  
  </td>
</tr>
@endforeach

</thead>
</table>  

@if($rc == 0)
<p style="text-align: center;">No result found</p>
@endif

@endif





</div>
<div class="row">
 <div class="col-sm-12">

 </div>
</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()