@extends('admin.layouts.header')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid ">
   <!-- Page Heading -->
   <!-- Content Row -->
   <div class="row">
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-12 col-md-12 mb-4 pt-4">
        @if($message = Session::get('message'))
        <div class="alert alert-primary">
         <p>{{ $message }}</p>
      </div>
      @endif
      <a class="btn btn-warning" href="{{url('admin/show_driver')}}">Back</a> 
      <hr>
      <form action="{{url('admin/store_driver')}}" method="POST" enctype="multipart/form-data">
         @csrf

         <legend>Add Driver</legend>

         @if ($errors->any())
         @foreach($errors->all() as $error)
         <div class="alert-danger">* {{$error}}</div>
         @endforeach
         @endif
         <div class="row">

          <div class="col-sm-6">       

           <div class="form-group">
             <label for="">Name</label>
             <input type="text" class="form-control" id="" name="driver_name" placeholder="Enter Name" required="required" >
          </div>


          <div class="form-group">
             <label for="">Phone</label>
             <input type="tel" class="form-control" id="" name="driver_phone" placeholder="Enter Phone / Mobile No" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" pattern="[6-9]{1}[0-9]{9}">
          </div>

          <div class="form-group">
             <label for="">Email</label>
             <input type="email" class="form-control" name="driver_email" id="" placeholder="Enter Email-Id">
          </div>


          <div class="form-group">
             <label for="">Pan No.</label>
             <input type="text" class="form-control" name="driver_pan_no" id="" placeholder="Enter Pan Card Number">
          </div>

          <div class="form-group">
             <label for="">Aadhar No.</label>
             <input type="text" class="form-control" name="driver_aadhar_no" id="" placeholder="Enter Aadhar Card Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
          </div>

          <div class="form-group">
            <label for="">Driver Licence No.</label>
            <input type="text" class="form-control" name="driver_drive_licence_no" id="" placeholder="Enter Drive Licence Number" required="required">
         </div>

         <div class="form-group">
            <label for="">Country</label>
            <select name="driver_country" class="form-control" required="required" required="required">
               <option  value="">----SELECT----</option>
               <option value="india">India</option>

            </select>
         </div>

         <div class="form-group">
            <label for="">State</label>
            <select name="driver_state" id="state" class="form-control" onchange="getCity()">
               <option value="">----SELECT----</option>
               @if(!empty($states))
               @foreach($states as $state)
               <option value="{{$state->state}}">{{$state->state}}</option>
               @endforeach
               @endif
            </select>
         </div>
         
      </div>


      <div class="col-sm-6">     

        <div class="form-group">
          <label for="">City</label>
          <select name="driver_city" class="form-control" id="city" >
            <option value="">----SELECT----</option>
         </select>
      </div>

      <div class="form-group">
       <label for="">Pin Code</label>
       <input type="number" class="form-control" name="driver_pincode" id="" placeholder="Pin Code Number" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
    </div>


    <div class="form-group">
       <label for="">Date Of Birth</label>
       <input type="Date" class="form-control" id="" name="driver_dob" placeholder="Date Of Birth" required="required">
    </div>
    
    <div class="form-group">
      <label for="">Gender</label>
      <p><input type="radio" value="Male"  id="" name="driver_gander">Male &nbsp;&nbsp;
         <input type="radio"  value="Female" id="" name="driver_gander">Female &nbsp;&nbsp;
      </div>

      <div class="form-group">
       <label for="">Id Proof Image</label>
       <input type="file" class="form-control" id="driver_idproof_image" name="driver_idproof_image" placeholder="Input field" >
    </div>


    <div class="form-group">
       <label for="">Pan Card Image</label>
       <input type="file" class="form-control" id="driver_pan_image" name="driver_pan_image" placeholder="Input field">
    </div>

    <div class="form-group">
      <label for="">Aadhar Card Image</label>
      <input type="file" class="form-control" id="driver_aadhar_image" name="driver_aadhar_image" placeholder="Input field">
   </div>

   <div class="form-group">
      <label for="">Drive Licence Image</label>
      <input type="file" class="form-control" id="driver_drive_licence_image" name="driver_drive_licence_image" placeholder="Input field" required="required">
   </div>

   <div class="form-group">
      <label for="">Driver Photo</label>
      <input type="file" class="form-control" id="driver_photo" name="driver_photo" placeholder="Input field">
   </div>
   
</div>
</div>
<button type="submit" class="btn btn-primary">Add</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->

<script type="text/javascript">
  function getCity(){
    var state = $('#state').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         url: '/admin/cities',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: state },
         success: function (data) {
           $('#city').empty();
           $('#city').html(data);
         },
         failure: function (data) {
           alert('Something Went Wrong');
         }
      });
  }
</script>

@endsection()