@extends('admin.layouts.header')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid ">
      <!-- Page Heading -->
      <!-- Content Row -->
      <div class="row">
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-12 col-md-12 mb-4 pt-4">
           @if($message = Session::get('message'))
           <div class="alert alert-primary">
            <p>{{ $message }}</p>
      </div>
      @endif

      <a class="btn btn-warning" href="{{url('admin/show_driver')}}">Back</a> 
      <hr>
      <form action="{{url('admin/update_driver/'.$driver->driver_id)}}" method="POST" role="form" enctype="multipart/form-data">
            @csrf

            <legend>Edit Driver's Detail</legend>

            @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert-danger">* {{$error}}</div>
            @endforeach
            @endif
            <div class="row">

             <div class="col-sm-6">       

                 <div class="form-group">
                      <label for="driver_name">Name</label>
                      <input type="text" class="form-control" id="driver_name" name="driver_name" value="@if(!empty($driver->driver_name)){{$driver->driver_name}}@endif" placeholder="Enter Name" required="required">
                </div>


                <div class="form-group">
                      <label for="driver_phone">Phone</label>
                      <input type="tel" class="form-control" id="driver_phone" name="driver_phone" value="@if(!empty($driver->driver_phone)){{$driver->driver_phone}}@endif" placeholder="Enter Phone / Mobile No" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required="required" pattern="[6-9]{1}[0-9]{9}" >
                </div>

                <div class="form-group">
                      <label for="driver_email">Email</label>
                      <input type="text" class="form-control" name="driver_email" id="driver_email" value="@if(!empty($driver->driver_email)){{$driver->driver_email}}@endif" placeholder="Enter Email-Id">
                </div>


                <div class="form-group">
                      <label for="driver_pan_no">Pan No.</label>
                      <input type="text" class="form-control" name="driver_pan_no" id="driver_pan_no" value="@if(!empty($driver->driver_pan_no)){{$driver->driver_pan_no}}@endif" placeholder="Enter Pan Card Number">
                </div>

                <div class="form-group">
                      <label for="driver_aadhar_no">Aadhar No.</label>
                      <input type="text" class="form-control" name="driver_aadhar_no" id="driver_aadhar_no" value="@if(!empty($driver->driver_aadhar_no)){{$driver->driver_aadhar_no}}@endif" placeholder="Enter Aadhar Card Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                </div>

                <div class="form-group">
                  <label for="driver_drive_licence_no">Drive Licence No.</label>
                  <input type="text" class="form-control" name="driver_drive_licence_no" id="driver_drive_licence_no" value="@if(!empty($driver->driver_drive_licence_no)){{$driver->driver_drive_licence_no}}@endif" placeholder="Enter Drive Licence Number" required="required">
            </div>

            <div class="form-group">
                  <label for="driver_country">Country</label>
                  <select name="driver_country" class="form-control" id="driver_country" required="required">
                        <option value="">----SELECT----</option>
                        <option value="india" @if($driver->driver_country=='india'){{'selected'}}@endif>India</option>
                  </select>
            </div>

            <div class="form-group">
                  <label for="driver_state">State</label>
                   <select name="driver_state" id="state" class="form-control" required="required" onchange="getCity()">
               <option value="">----SELECT----</option>
               @if(!empty($states))
               @foreach($states as $state)
               <option value="{{$state->state}}" @if($driver->driver_state==$state->state){{'selected'}}@endif>{{$state->state}}</option>
               @endforeach
               @endif
            </select>
            </div>

      </div>


      <div class="col-sm-6">     

           <div class="form-group">
                <label for="driver_city">City</label>
                <select name="driver_city" class="form-control" id="city" required="required">
                  <option value="">----SELECT----</option>
                  @if(!empty($cities))
               @foreach($cities as $city)
               <option value="{{$city->city}}" @if($driver->driver_city==$city->city){{'selected'}}@endif>{{$city->city}}</option>
               @endforeach
               @endif
            </select>
      </div>

      <div class="form-group">
          <label for="driver_pincode">Pin Code</label>
          <input type="text" class="form-control" name="driver_pincode" id="driver_pincode" placeholder="Pin Code Number" value="@if(!empty($driver->driver_pincode)){{$driver->driver_pincode}}@endif" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
    </div>


    <div class="form-group">
          <label for="driver_dob">Date Of Birth</label>
          <input type="Date" class="form-control" id="driver_dob" name="driver_dob" placeholder="Date Of Birth" value="@if(!empty($driver->driver_dob)){{$driver->driver_dob}}@endif" required="required">
    </div>

    <div class="form-group">
      <label for="driver_gander">Gender</label>
      <p><input type="radio" value="Male"  id="Male" name="driver_gander" @if($driver->driver_gander == 'Male'){{'checked'}}@endif>Male &nbsp;&nbsp;
            <input type="radio" value="Female" id="Female" name="driver_gander" @if($driver->driver_gander == 'Female'){{'checked'}}@endif>Female &nbsp;&nbsp;
      </div>

      <div class="form-group">
          <label for="driver_idproof_image">Id Proof Image</label>
          <input type="file" class="form-control" id="driver_idproof_image" name="driver_idproof_image" placeholder="Input field" >
    </div>


    <div class="form-group">
          <label for="driver_pan_image">Pan Card Image</label>
          <input type="file" class="form-control" id="driver_pan_image" name="driver_pan_image" placeholder="Input field">
    </div>

    <div class="form-group">
      <label for="driver_aadhar_image">Aadhar Card Image</label>
      <input type="file" class="form-control" id="driver_aadhar_image" name="driver_aadhar_image" placeholder="Input field">
</div>

<div class="form-group">
      <label for="driver_drive_licence_image">Drive Licence Image</label>
      <input type="file" class="form-control" id="driver_drive_licence_image" name="driver_drive_licence_image" placeholder="Input field">
</div>

<div class="form-group">
      <label for="driver_photo">Driver Photo</label>
      <input type="file" class="form-control" id="driver_photo" name="driver_photo" placeholder="Input field">
</div>

</div>
</div>
<button type="submit" class="btn btn-primary">Update</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->

<script type="text/javascript">
  function getCity(){
    var state = $('#state').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         url: '/admin/cities',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: state },
         success: function (data) {
           $('#city').empty();
           $('#city').html(data);
         },
         failure: function (data) {
           alert('Something Went Wrong');
         }
      });
  }
</script>

@endsection()