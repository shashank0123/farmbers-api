@extends('admin.layouts.header')
@section('content')

<div class="container-fluid">
 <div class="row">
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   <div class="row">
    <div class="col-md-6">        
     <a class="btn btn-warning" href="{{url('admin/add_driver')}}">Add Driver</a> 
   </div>
   <div class="col-md-6">
    <input type="search" class="form-control" name="driver_search" id="driver_search" placeholder="Search Driver" onkeyup="getDriverSearch()" style="width: 80%" />        
  </div>
</div>

<hr>
<table class="table table-bordered table-responsive">
 <thead>
   <tr class="alert-info">
     <th>SNo</th>	
     <th>Name</th>
     <th>Phone</th>
     <th>Email</th>
     <th>Date Of Birth</th>
     <th>Gander</th>
     <th>Pan Card No</th>		
     <th>Aadhar Card No </th>
     <th>Drive Licence No</th>
     <th>Country</th>
     <th>State</th>
     <th>City</th>
     <th>Pin Code</th>                        			
     <th>Pan Card Image</th>    
     <th>Aadhar Card Image </th>
     <th>Drive Licence Image</th>
     <th>Driver Id Image</th>
     <th>Driver Image</th>
     <th colspan="2">Activity</th>
   </tr>
 </thead>
 <thead id="driver-section">
  @php $i=1; @endphp
  @foreach($driver as $dr)
  <tr>
   <td>{{$i}}</td> 
   <td>{{$dr->driver_name}}</td>
   <td>{{$dr->driver_phone}}</td>
   <td>{{$dr->driver_email}}</td>
   <td>{{$dr->driver_dob}}</td>
   <td>{{$dr->driver_gander}}</td>
   <td>{{$dr->driver_pan_no}}</td>
   <td>{{$dr->driver_aadhar_no}}</td>
   <td>{{$dr->driver_drive_licence_no}}</td>
   <td>{{$dr->driver_city}}</td>
   <td>{{$dr->driver_state}}</td>
   <td>{{$dr->driver_country}}</td>
   <td>{{$dr->driver_pincode}}</td>
   <td>
    @if($dr->driver_pan_image!=null)<a href="{{asset('/upload/'.$dr->driver_pan_image)}}" target="_blank"><img src="upload/{{$dr->driver_pan_image}}" style="width: 50px;height: 50px;"></a>@endif
  </td>
  <td>
    @if($dr->driver_aadhar_image!=null)<a href="{{asset('/upload/'.$dr->driver_aadhar_image)}}" target="_blank"><img src="upload/{{$dr->driver_aadhar_image}}" style="width: 50px;height: 50px;"></a>@endif
  </td>
  <td>
    @if($dr->driver_drive_licence_image!=null)<a href="{{asset('/upload/'.$dr->driver_drive_licence_image)}}" target="_blank"><img src="upload/{{$dr->driver_drive_licence_image}}" style="width: 50px;height: 50px;"></a>@endif
  </td>
  <td>
    @if($dr->driver_idproof_image)<a href="{{asset('/upload/'.$dr->driver_idproof_image)}}" target="_blank"><img src="upload/{{$dr->driver_idproof_image}}" style="width: 50px;height: 50px;"></a>@endif
  </td>
  <td>
    @if($dr->driver_photo!=null)<a href="{{asset('/upload/'.$dr->driver_drive_licence_image)}}" target="_blank"><img src="upload/{{$dr->driver_photo}}" style="width: 50px;height: 50px;"></a>@endif
  </td>


  <td><a href="{{url('admin/edit_driver/'.$dr->driver_id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 

   <form action="{{url('admin/delete_driver/'.$dr->driver_id)}}" method="get">
    {{csrf_field()}}
    {{ method_field("DELETE") }}
    <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$dr->driver_id}})"><i class="fa fa-remove"></i></button>
    <input type="submit" hidden="hidden" id="form{{$dr->driver_id}}" class="btn btn-danger btn-xs">
  </form>
</td>
</tr>
@php $i++ @endphp
@endforeach
</thead>
</table>	

</div>

</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">
  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }

  function getDriverSearch(){
    var search = $('#driver_search').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/dynamic-drivers',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search },
     success: function (data) {
       $('#driver-section').empty();
       $('#driver-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }
</script>
@endsection