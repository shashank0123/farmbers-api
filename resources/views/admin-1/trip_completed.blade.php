<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title> <?php echo "FastTrack";?> Live Order Tracking</title>

	<link href="/assets/css/bootstrap.css" rel="stylesheet">
	<link href="/assets/css/font-awesome.css" rel="stylesheet">
	<link href="/assets/css/custom-tracking.css" rel="stylesheet">
	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script src="/assets/js/ie-emulation-modes-warning.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Modernizer Script for old Browsers -->
      <script src="/assets/js/modernizr-2.6.2.min.js"></script>
      <style type="text/css">

      	.car-number-plate{
      		background-color: #fff;
      		border:1px solid #000;
      		color: #000;
      		font-size: 22px;
      		line-height: 24px;
      		display: inline-block;
      		padding: 0px 10px;
      	}
      	.tracking-footer{
      		background-color: #ff7935;
      		color: #fff;
      	}
      	.img-call-center{
      		max-width: 80px;
      		text-align: center;
      		margin: 0px auto 20px auto;
      	}
      	.img-t-user{
      		float: right;
      		margin-left: 20px;
      		height: 100px;
      		width: 100px;
      	}
      	.image-sap{
      		width: 100%;
      		height: 150px;
      		object-fit: fill;
      		object-position: 50% 50%;
      		transition: all 0.4s ease 0s;
      	}
      	.t-driver-info p, .t-trip-info p{
      		margin-bottom: 5px;
      	}
      	.t-driver-info p span{
      		width: 135px;
      		display: inline-block;
      	}
      	.t-trip-info p span{
      		width: 100px;
      		display: inline-block;
      	}
      	.t-driver-info .t-car-plate{
      		margin: 0px 0 0px auto;
      	}

      	@media (min-width: 992px) and (max-width: 1200px){
      		.t-driver-info{
      			background-image: url(assets/images/t-sap.jpg);
      			background-repeat: repeat-y;
      			background-position: left;
      			min-height: 180px;
      		}

      	}
      	@media (max-width: 992px){
      		.tm-driver-info{
      			display: block;
      			padding: 0px 0px 03px 0px;
      		}
      		.img-t-user-mobile{
      			border:10px solid #ff7935;
      			margin-top: -50px;
      			float: none;
      			margin-left: auto;
      			margin-right: auto;
      		}
      		.mobile-info span{
      			display:block;
      		}
      		.border-top{
      			border-top: 2px solid rgba(255,255,255,0.2);
      			margin-top: 10px;
      			padding-top: 20px;
      			padding-bottom: 30px;
      		}
      		.ride-info{
      			text-align: right;
      			max-width: 450px;
      			padding-right:50px; 
      			margin: 0px auto;
      		}
      		.mobile-sap{
      			position: absolute;
      			right: 0px;
      			bottom: 0px;
            display: none;
      		}
      		.mobile-care{
      			padding-right: 30px;
      			padding-left: 30px;
      		}
      	}
      	.yoobel-care{
      		font-size: 12px;
      	}
      	@media (min-width: 992px){
          .yo-care{
           width: 18%;
           float: left;
         }
         .yo-sap{
           width: 6%;
           float: left;
         }
         .yo-trip{
           width: 30%;
           float: left;
         }
         .yo-driver{
           width: 40%;
           float: left;
         }
       }

       @media (max-width: 1300px){
          .tracking-footer{
            font-size: 13px;
          }
          .img-t-user{
            float: right;
            margin-top: 13px;
            margin-left: 15px;
            height: 80px;
            width: 80px;
          }
          .img-call-center {
            margin: 0 auto 10px;
            max-width: 70px;
          }
          .yoobel-care{
            font-size: 10px;            
          }
       }
       .ride-comp{
          margin: 30px auto;
          display: inline-block;
          padding: 30px;
          font-size: 18px;
          color: #111;
          border: #8dc63f 2px solid;
          border-radius: 6px;
          background-color: #fff;
       }
      </style>
  </head>
  <body>
  	<?php //echo '<pre>'; print_r($trip_data); ?>
  	<div class="" style="padding: 0; overflow: hidden;background-color: #ff7935;">
  		<header class="text-center tracking-footer">
  			<a href="#"> <img src="img/fast%20track-final-logo-design.png" class="img-responsive" alt="<?php echo "Fasttrack";?>"> </a>
  		</header>
      <div class="text-center">
    		<div class="ride-comp">
            <i class="fa fa-info-circle fa-2x"> </i>
            <br/>
          <br/>

    			 Sorry! Order was completed.
    		</div>
      </div>

  		<div class="tracking-footer visible-md visible-lg">
  			<div class="row" style="padding: 0px 15px;">
  				<div class="col-md-12 text-center yoobel-care">
  					<div class="p-20">
  						<img src="img/fast%20track-final-logo-design.png" class="img-responsive img-call-center">
  						<p class="m-b-0"> <?php echo "Fasttrack";?> Call Center No. :  021 222 33333 </p>
  					</div>
  				</div>
				</div>
  		</div>
  		<div class="tracking-footer visible-xs visible-sm" style="position: relative;">
  			<dir class="tm-driver-info p-b-30">
  				<div class="text-center mobile-care border-top">
  					<img src="img/fast%20track-final-logo-design.png" class="img-responsive img-call-center">
  					<p class="m-b-0"> <?php echo "Fasttrack";?> Call Center Number :  021 222 33333 </p>
  				</div>
  			</dir>
  			<img src="/assets/images/mobile-sap.png" class="mobile-sap">
  		</div>
  	</div>
  	<script src="/assets/js/jquery.min.js"></script> 
  	<script src="/assets/js/bootstrap.js"></script> 
  	<script src="/assets/js/wow.min.js"></script> 
  	<script src="/assets/js/custom.js"></script> 

  	<script type="text/javascript">
  		$(document).ready(function(){
  			var window_height = $(window).height();
  			var header_height = $("header").height();
  			var footer_height = $(".tracking-footer").height();
	    //alert("window_height:"+window_height+' header_height:'+header_height+' footer_height:'+footer_height);
	    $("#map-canvas").height(window_height - (header_height+footer_height+35));
	});
</script>
</body>
</html>