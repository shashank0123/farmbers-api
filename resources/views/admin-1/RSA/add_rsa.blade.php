@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/list')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/store_rsa') }}" method="POST" role="form">
    {{ csrf_field() }}
    <legend>Add RSA (Roadside Assistance)</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">
     <div class="col-sm-6">
       <div class="form-group">
         <label for="">Company</label>
         <input type="text" class="form-control" name="company" id="" placeholder="Enter Company Name Here"  required="required">
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
        <label for="">Contact Number</label>
        <input type="tel" class="form-control" id="" name="contact" placeholder="Contact Number" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
      </div>
    </div>
        
  </div>
  <button type="submit" class="btn btn-primary">Add</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()