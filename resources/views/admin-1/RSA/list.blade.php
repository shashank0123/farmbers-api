@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    
    <div class="row">
      <div class="col-md-6">
        <a class="btn btn-warning" href="{{url('admin/add_rsa')}}">Add RSA</a>
      </div>
      <div class="col-md-6">
        <input type="search" class="form-control" name="rsa_search" id="rsa_search" placeholder="Search RSA" onkeyup="getRsaSearch()" style="width: 80%" />        
      </div>
    </div>
    
    <hr> 
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Company</th>       
       <th>Contact</th>
       <th>Created At</th>
       <th>Activity</th>
     </tr>
   </thead>
   <thead id="rsa-section">
    @php $i=1; @endphp
    @foreach($rsas as $d)
    <tr>
     <td>{{$i++}}</td> 
     <td>{{$d->company}}</td>
     <td>{{$d->contact}}</td>     
     <td>{{date('d-m-Y',strtotime(explode(' ',$d->created_at)[0]))}}</td>
     
     <td>
       
      <a href="{{url('admin/edit_rsa/'.$d->id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 
      
      <form action="{{url('admin/delete_rsa/'.$d->id)}}" method="post">
        {{csrf_field()}}
        <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$d->id}})"><i class="fa fa-remove"></i></button>
        <input type="submit" hidden="hidden" id="form{{$d->id}}" class="btn btn-danger btn-xs">
      </form>
    </td>
  </tr>
  @endforeach
</thead>
</table>	

</div>
<div class="row">
 
</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">

  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }

  function getRsaSearch(){
    var search = $('#rsa_search').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/search-rsa',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search },
     success: function (data) {
       $('#rsa-section').empty();
       $('#rsa-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }

</script>
@endsection
