<?php use App\Brand;

use App\Driver;
use App\User;
use App\Vehicle;
use App\DeviceToVehicle;

$total_users = User::count();
$total_drivers = Driver::count();
$total_vehicles = Vehicle::count();
$active_devices = DeviceToVehicle::count();
?>
@extends('admin.layouts.header')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid  bg-gradient-primary">

  <div class="row">
    <div class="col-sm-3 center">
      <a href="/admin/user-list/{{'users'}}">
        <div class="card1">
          <img src="/side-icon/user-icon.png" class="img-responsive">
          <h2>Total Users <br>
          {{ $total_users ?? '' }}</h2>
        </div>
      </a>
    </div>
    <div class="col-sm-3 center">
      <a href="/admin/show_vehicals">
      <div class="card1">
        <img src="/side-icon/car-icon.png" class="img-responsive">
        <h2>Total Vehicles <br>
        {{$total_vehicles ?? '' }}</h2>
      </div>
    </a>
    </div>
    <div class="col-sm-3 center">
      <a href="/admin/show_driver">
      <div class="card1">
        <img src="/side-icon/driver-icon.png" class="img-responsive">
        <h2>Total Drivers<br>
        {{$total_drivers ?? '' }}</h2>
      </div>
    </a>
    </div>
    <div class="col-sm-3 center">
      <a href="/admin/assigned_device_list">
      <div class="card1">
        <img src="/side-icon/device-icon.png" class="img-responsive">
        <h2>Active Devices<br>
        {{$active_devices ?? '' }}</h2>
      </div>
    </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/not_working_vehicles">
      <div class="card1">
        <img src="/side-icon/not-working-vehicles.png" class="img-responsive">
        <h2>Not Working Vehicles<br>
        0</h2>
      </div>
    </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/stopped_vehicles">
      <div class="card1">
        <img src="/side-icon/stopped-vehicles.png" class="img-responsive">
        <h2>Stopped Vehicles<br>
        0</h2>
      </div>
    </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/logged_in_users">
      <div class="card1">
        <img src="/side-icon/logged-in-users.png" class="img-responsive">
        <h2>Logged In Users<br>
        0</h2>
      </div>
    </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/active_app_users">
      <div class="card1">
        <img src="/side-icon/active-users.png" class="img-responsive">
        <h2>Active App Users<br>
        0</h2>
      </div>
    </a>
    </div>
  </div>
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
    {{-- https://goo.gl/maps/EPav7yVStZrLFsgg9 --}}
    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-12 col-md-12 mb-4 pt-4">
      {{-- <div class="map-responsive">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.766788144537!2d77.27259541455975!3d28.636750890641974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfde68e92d50b%3A0xa3079c526ec485c9!2sv3s%20mall!5e0!3m2!1sen!2sin!4v1575362096798!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
       --}}
    </div>

  </div> 
</div>

<!-- Begin Page Content -->
<div class="container-fluid">
       <div style="min-height: 400px;" id="map"></div>
       <div id="name-box" class="name-box">
  <h3>Enter your username</h3>
  <input id="name" type="text" placeholder="e.g. Mike">
  <button id="saveNameButton">Save</button>
</div>

  <!-- Content Row -->

  <div class="row mt-4">

    <!-- Area Chart -->
    <div class="col-xl-5 col-lg-6">
      <div class="card  shadow mb-4 p-4">
        <!-- Card Header - Dropdown -->
        <div>
          <h4><span class="float-left vtitle" ><b>List of All Vehicles</b></span>
            <span class="float-right"><a href="/show_vehicals" class="btn btn-outline-primary allsee">See All</a></span></h4>
          </div>

          <div class="card-sec-cod1 py-3">

            @if(!empty($vehicles))
            @foreach ($vehicles as $vehicle)
            <?php $brand = Brand::where('id',$vehicle->vehicle_brand)->first(); if (!isset($brand->name)) $brandsname = "Unknown"; else $brandsname = $brand->name; ?>
            <div class="row my-4 mx-auto">
              <div class="col-sm-3">
                <div class="carlist-img text-center mb-3">
                  <img src="@if(!empty($vehicle->vehicle_image)){{url('/upload/'.$vehicle->vehicle_image)}}@else{{url('side-icon/vehicle-default.png')}}@endif">
                </div>
              </div>
              <div class="col-sm-9">
               <div class="carlist-text pl-4">
                 <h4 class="" >{{$vehicle->vehicle_name}}</h4>
                 <p class="mb-0">Register No : {{$vehicle->vehicle_no}}</p>
                 <p>Brand : {{ $brandsname }}</p>
                 <button type="button" class="btn btn-success px-4">Call</button>
                 <a href="{{url('/edit_vehicals/'.$vehicle->vehicle_id)}}" class="btn btn-primary px-4">Edit</a>
               </div>
             </div>

           </div>
           <hr>

           @endforeach
           @endif





         </div>

       </div>
     </div>

     <!-- Pie Chart -->
     <div class="col-xl-7  col-lg-6">
       <div class="card  shadow mb-4 p-4">
        <!-- Card Header - Dropdown -->
        <div>
          <h4><span class="float-left vtitle" ><b>List of All Drivers</b></span>
            <span class="float-right"><a href="/show_driver" class="btn btn-outline-primary allsee">View All</a></span></h4>
          </div>

          <div class="card-sec-cod1 py-3">

            @if(!empty($drivers))
            @foreach($drivers as $driver)
            <div class="row my-4 mx-auto">
              <div class="col-sm-3">
                <div class="carlist-img text-center mb-3">
                  <img src="@if(!empty($driver->driver_photo)){{url('/upload/'.$driver->driver_photo)}}@else{{url('side-icon/driver-default.png')}}@endif">
                </div>
              </div>
              <div class="col-sm-9">
               <div class="carlist-text pl-4">
                 <h4 class="" >{{$driver->driver_name}}</h4>
                 <p class="mb-0">Contact No : {{$driver->driver_phone}}</p>
                 <p>State : {{ucfirst($driver->driver_state)}}</p>
                 <a href="" class="btn btn-success px-4">Call</a>
                 <a href="/edit_driver/{{$driver->driver_id}}" class="btn btn-primary px-4">Edit</a>
                 <a href="/delete_driver/{{$driver->driver_id}}" class="btn btn-danger mtopdlt">Delete</a>
               </div>
             </div>

           </div>
           <hr>
           @endforeach
           @endif

         </div>

       </div>
     </div>
   </div>


   <div class="row mt-4 align-items-center">
    <div class="col-xl-12 col-lg-12">
      <div class="card  shadow mb-4 p-4">
       <div>
        <h4><span class="float-left vtitle" ><b>List of All Vehicles Drivers</b></span>
          <span class="float-right"><a href="/assigned_list" class="btn btn-outline-primary allsee">View All</a></span></h4>
          <br>
          {{-- <p>Shutdown the engine, if your vehicle is disturbing or in emergency</p> --}}
        </div>

        <div> 

          @if(!empty($vehicle_drivers))
          @foreach($vehicle_drivers as $drive)
          <div class="row my-4 mx-auto align-items-center bg-onoffsec">
            <div class="md-9 col-sm-9 col-12">
              <div class="row my-4 mx-auto align-items-center">
                <div class="col-md-3 col-sm-12 col-12">
                  <div class="carower-img  mb-3">
                    <img src="@if(!empty($driver->vehicle_image)){{url('/upload/'.$driver->vehicle_image)}}@else{{url('side-icon/vehicle-default.png')}}@endif">
                  </div>
                </div>
                <div class="col-md-9 col-sm-12 col-12">
                  <div class="carlist-text">
                   <h4 class="" >Vehicle - {{strtoupper($drive->vehicle_name)}} / {{$drive->vehicle_brand}}</h4>
                   <h4 class="" >Driver - {{ucfirst($drive->driver_name)}} / {{$drive->vehicle_brand}}</h4>
                   <p class="mb-0">Register No : {{$drive->vehicle_no}}</p>


                 </div>
               </div>
             </div>
           </div>
           <div class="col-md-3 col-sm-3 col-12 align-items-center">

             <div class="carlist-text pl-4">
              <div class="row">
                <div class="col-sm-6">
                 <a href="/edit_assigning/{{$drive->id}}" class="btn btn-primary px-4">Edit</a>                  
               </div>
               <div class="col-sm-6">
                <form action="{{url('/delete_assinging/'.$drive->id)}}" method="POST">
                  {{csrf_field()}}

                  <button type="submit" class="btn btn-danger mtopdlt">Delete</submit>
                  </form>
                </div>
              </div>




            </div>
            {{--  <div class="onoffbtn text-center pl-4">

               <label class="switch">
                <input type="checkbox" checked>
                <span class="slider round"></span>
              </label>
            </div> --}}
          </div>

        </div>

        @endforeach
        @endif

{{-- <div class="row my-4 mx-auto align-items-center bg-onoffsec">
            <div class="md-9 col-sm-9 col-12">
              <div class="row my-4 mx-auto align-items-center">
                <div class="col-md-3 col-sm-12 col-12">
                  <div class="carower-img  mb-3">
                    <img src="img/vector-03.png">
                  </div>
                </div>
                <div class="col-md-9 col-sm-12 col-12">
                  <div class="carlist-text">
                   <h4 class="" >Aulto 6789</h4>
                   <p class="mb-0">Register No : 98767856C</p>


                 </div>
               </div>
             </div>
           </div>
           <div class="col-md-3 col-sm-3 col-12 align-items-center">
             <div class="onoffbtn text-center pl-4">

               <label class="switch">
                <input type="checkbox" checked>
                <span class="slider round"></span>
              </label>
            </div>
          </div>

        </div> --}}
      </div>

    </div>
  </div>
</div>









</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->





@endsection