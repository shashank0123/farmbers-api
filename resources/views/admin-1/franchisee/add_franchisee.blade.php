@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/show_franchisees')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/store_franchisee') }}" method="POST" role="form">
    {{ csrf_field() }}
    <legend>Add Franchisee</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">
     <div class="col-sm-6">
       <div class="form-group">
         <label for="franchise_name">Name</label>
         <input type="text" class="form-control" name="franchise_name" id="franchise_name" placeholder="Franchisee Name"  required="required">
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
         <label for="email">Email</label>
         <input type="email" class="form-control" name="email" id="email" placeholder="Franchisee Email"  >
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
         <label for="mobile">Phone</label>
         <input type="tel" class="form-control" name="mobile" id="mobile" placeholder="Franchisee Phone"  required="required" pattern="[6-9]{1}[0-9]{9}"  oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
        <label for="franchise_address">Address</label>
        <input type="text" class="form-control" id="franchise_address" name="franchise_address" placeholder="Franchisee Address" required="required">
      </div>
    </div>

    <div class="col-sm-6">
     <div class="form-group">
      <label for="gst_no">GST No </label>
      <input type="text" class="form-control" name="gst_no" placeholder="GST No" id="gst_no" required="required">
    </div>
  </div>


  <div class="col-sm-6">
   <div class="form-group">
    <label for="yearly_rate">Yearly Rate</label>
    <input type="text" class="form-control" name="yearly_rate" placeholder="Yearly Rate" id="yearly_rate" required="required">
  </div>
</div>

<div class="col-sm-6">
 <div class="form-group">
  <label for="whitelabel">White Label</label>
  <input type="text" class="form-control" name="whitelabel" id="whitelabel" required="required" >
</div>
</div>

<div class="col-sm-6">
 <div class="form-group">
  <label for="extra">Extra</label>
  <input type="text" class="form-control" name="extra" id="extra" required="required">
</div>
</div>

<div class="col-sm-6">
 <div class="form-group">
  <label for="password">Password</label>
  <input type="password" class="form-control" name="password" placeholder="Password" id="password" required="required">
</div>
</div>

<div class="col-sm-6">
 <div class="form-group">
  <label for="confirm_password">Confirm Password</label>
  <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" id="confirm_password" required="required">
</div>
</div>

</div>
<button type="submit" class="btn btn-primary">Add</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()