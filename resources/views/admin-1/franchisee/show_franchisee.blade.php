@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <div class="row">
    <div class="col-md-6">
      <a class="btn btn-warning" href="{{url('admin/add_franchisee')}}">Add Franchisee</a>
   </div>
   <div class="col-md-6">
    <input type="search" class="form-control" name="franchisee_search" id="franchisee_search" placeholder="Search Franchisee" onkeyup="getFranchiseeSearch()" style="width: 80%" />        
  </div>
</div>

    
    <hr> 
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Name</th>
       <th>GST Number</th>
       <th>Yearly Rate</th>
       <th>Address</th>
       <th>Created At</th>
       <th>Status</th>
       <th>Activity</th>
     </tr>
   </thead>
   <thead id="franchisee-section">
    @php $i=1; @endphp
    @foreach($franchisees as $franch)
    <tr>
     <td>{{$i}}</td> 
     <td>{{$franch->franchise_name}}</td>
     <td>{{$franch->gst_no}}</td>
     <td>{{$franch->yearly_rate}}</td>
     <td>{{$franch->franchise_address}}</td>
     <td>{{explode(' ',$franch->created_at)[0]}}</td>
     <td>@if($franch->account_status == 'barred'){{'Inactive'}}@else{{ucfirst($franch->account_status)}}@endif</td>
     
     <td>
      <a href="{{url('admin/edit_franchisee/'.$franch->franchise_id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 
      @if($franch->account_status == 'active')
      <a onclick="getActive('{{$franch->id}}','{{\Auth::user()->email}}')" class="btn btn-danger  mb-1 ml-1" style="color: #fff">Inactive</a>
      @else
      <a onclick="getInactive('{{$franch->id}}','{{\Auth::user()->email}}')" class="btn btn-primary  mb-1 ml-1" style="color: #fff">Active</a>
      @endif 
      
      <!--  <form action="{{url('admin/delete_franchisee/'.$franch->id)}}" method="post">
        {{csrf_field()}}
        <button type="submit" class="btn btn-danger btn-xs">Delete</button>
      </form> -->
    </td>
  </tr>
  @php $i++ @endphp
  @endforeach
</thead>
</table>	

</div>

</div>
</div>
<!-- Begin Page Content -->

<script type="text/javascript">

  function getActive(id,email){
    var password = prompt('First Enter Your Password.');
    if(password){
    var status = 'barred';
    
     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
 
      $.ajax({
         url: '/admin/checkAdmin',
         type: 'POST',
         data: {_token: CSRF_TOKEN, password: password, email: email, franch: id, status: status },
         success: function (data) {
          alert(data);
          setTimeout(function(){location.reload();},1000);
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
    }
  }

  function getInactive(id,email){
    var password = prompt('First Enter Your Password.');
    if(password){
    var status = 'active';
    
     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
 
      $.ajax({
         url: '/admin/checkAdmin',
         type: 'POST',
         data: {_token: CSRF_TOKEN, password: password, email: email, franch: id, status: status },
         success: function (data) {
          alert(data);
          setTimeout(function(){location.reload();},1000);
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
    }
  }

  function getFranchiseeSearch(){
    var search = $('#franchisee_search').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/search-franchisee',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search },
     success: function (data) {
       $('#franchisee-section').empty();
       $('#franchisee-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }

</script>

@endsection()
