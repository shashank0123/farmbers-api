@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/show_franchisees')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/update_franchisee/'.$franchisee->franchise_id) }}" method="POST" role="form">
    {{ csrf_field() }}
    <legend>Edit Franchisee Detail</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">

     <div class="col-sm-6">
       <div class="form-group">
         <label for="franchise_name">Name</label>
         <input type="text" class="form-control" name="franchise_name" id="" placeholder="Franchisee Name"  required="required" value="{{$franchisee->franchise_name}}">
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
         <label for="email">Email</label>
         <input type="email" class="form-control" name="email" id="email" placeholder="Franchisee Email" value="{{$franchisee->email}}">
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
         <label for="mobile">Phone</label>
         <input type="tel" class="form-control" name="mobile" id="mobile" placeholder="Franchisee Phone"  required="required" value="{{$franchisee->mobile}}" pattern="[6-9]{1}[0-9]{9}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
        <label for="franchise_address">Address</label>
        <input type="text" class="form-control" id="franchise_address" name="franchise_address" placeholder="Franchisee Address" required="required" value="@if(!empty($franchisee->franchise_address)){{$franchisee->franchise_address}}@endif">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="gst_no">GST No </label>
        <input type="text" class="form-control" name="gst_no" id="gst_no" required="required" value="@if(!empty($franchisee->gst_no)){{$franchisee->gst_no}}@endif">
      </div>
    </div>


<div class="col-sm-6">
       <div class="form-group">
        <label for="yearly_rate">Yearly Rate</label>
        <input type="text" class="form-control" name="yearly_rate" id="yearly_rate" required="required" value="@if(!empty($franchisee->yearly_rate)){{$franchisee->yearly_rate}}@endif">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="whitelabel">White Label</label>
        <input type="text" class="form-control" name="whitelabel" id="whitelabel" required="required" value="@if(!empty($franchisee->whitelabel)){{$franchisee->whitelabel}}@endif">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="extra">Extra</label>
        <input type="text" class="form-control" name="extra" id="extra" required="required"  value="@if(!empty($franchisee->extra)){{$franchisee->extra}}@endif">
      </div>
    </div>

    <div class="col-sm-6">
 <div class="form-group">
  <label for="password">Password</label>
  <input type="password" class="form-control" name="password" placeholder="Password" id="password">
</div>
</div>

<div class="col-sm-6">
 <div class="form-group">
  <label for="confirm_password">Confirm Password</label>
  <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" id="confirm_password">
</div>
</div>

  </div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()