<?php use App\User; ?>
@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <div class="row">
      <div class="col-md-6">        
       <h4 style="margin-top: 15px">Barred Users</h4>
     </div>
     <div class="col-md-6">
      <input type="search" class="form-control" name="user_search" id="user_search" placeholder="Search User" onkeyup="getUserSearch()" style="width: 80%">        
    </div>
  </div>
    <hr> 
    
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>User Name</th>
       <th>Email</th>
       <th>Mobile</th>
        <th>Status</th>
        <th>Change Status</th>
       <th>Created At</th>
       <th colspan="2">Action</th>
     </tr>
   </thead>
   <thead id="user-section">
    @php $i=1; @endphp
    @foreach($users as $user)
    <tr>
     <td>{{$i++}}</td> 
     <td>{{$user->name}}</td>
     <td>{{$user->email}}</td>
     <td>@if(!empty($user->mobile)){{$user->mobile}}@endif</td>
     <td>Barred</td>
     <td><a href="{{url('admin/unbar_user/'.$user->id)}}" class="btn btn-success  mb-1 ml-1">Unbar</a></td>
    
     <td>{{explode(' ',$user->created_at)[0]}}</td>
     <td colspan="2"><a href="{{url('admin/edit_user/'.$type.'/'.$user->id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 
      
       <form action="{{url('admin/delete_user/'.$type.'/'.$user->id)}}" method="post">
        {{csrf_field()}}
        <button type="submit" class="btn btn-danger btn-xs">Delete</button>
      </form>
    </td>
  </tr>
  @endforeach
</thead>
</table>	

</div>

</div>
</div>
<!-- Begin Page Content -->


@endsection()


@section('script')
<script type="text/javascript">
  
function getUserSearch(){
    var search = $('#user_search').val();
    var type = @php echo "'".$type."'" @endphp;
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/dynamic-barred-users',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search,type: type },
     success: function (data) {
       $('#user-section').empty();
       $('#user-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }

</script>
@endsection