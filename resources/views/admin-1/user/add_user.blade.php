@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/user-list/'.$type)}}">Back</a> 
  <hr>
  <form action="{{ url('admin/store_user/'.$type) }}" method="POST" role="form">
    {{ csrf_field() }}
    <div class="row">
      <legend>Add @if($type=='user'){{'Main User'}} @else {{'Sub User'}} @endif</legend>
      @if ($errors->any())
      @foreach($errors->all() as $error)
      <div class="alert-danger">* {{$error}}</div>
      @endforeach
      @endif
      <div class="row">
       <div class="col-sm-12">
         <div class="form-group">
           <label for="name">User Name<sup> *</sup></label>
           <input type="text" class="form-control" name="name" id="name" placeholder="Name Here" required="required">
         </div>               
       </div>

       <div class="col-sm-12">
         <div class="form-group">
           <label for="user_name">User ID<sup> *</sup></label>
           <input type="text" class="form-control" name="user_name" id="user_name" placeholder="User Id" required="required" onchange="checkUser()">
         </div>               
         <p style="color: red" id="user-error"> </p>
       </div>

       @if($type == 'sub-user')
       <div class="col-sm-12">
         <div class="form-group">
           <label for="parent_user">Parent<sup> *</sup></label>
           <select class="form-control" name="parent_user" id="parent_user" placeholder="Name Here" required="required">
             <option> -- Select Parent User -- </option>
             @if(!empty($roles))
             @foreach($roles as $use)
             <option value="{{$use->id}}">{{ucfirst($use->name)}}</option>
             @endforeach
             @endif
           </select>
         </div>               
       </div>

       <div class="col-sm-12" id="vehicle" style="display: none;">
         <div class="form-group">
           <label for="vehicle_id">Vehicle<sup> *</sup></label>
           <div id="output"></div>
           <select data-placeholder="Select Vehicle ..." name="vehicle_id[]" id="vehicle_id" multiple class="chosen-select form-control" required="required">            
           </select>
         </div>               
       </div>

       @endif

       <div class="col-sm-12">
         <div class="form-group">
          <label for="mobile">Mobile</label>
          <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" pattern="[6-9]{1}[0-9]{9}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
        </div>
      </div>

      <div class="col-sm-12">
       <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
      </div>
    </div>

    <div class="col-sm-12">
     <div class="form-group">
      <label for="activation_date">Activation Date</label>
      <input type="text" readonly class="form-control datepicker" id="activation_date" name="activation_date" placeholder="Eg. <?php echo date('d/m/Y')?>">
    </div>
  </div>

  <div class="col-sm-12">
   <div class="form-group">
    <label for="expiry_date">Expiry Date<sup> *</sup></label>
    <input type="text" class="form-control datepicker" id="expiry_date" name="expiry_date" placeholder="Eg. <?php echo date('d/m/Y')?>">
  </div>
</div>

<div class="col-sm-12">
 <div class="form-group">
  <label for="password">Password <sup>*</sup></label>
  <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
</div>
</div>

</div>
<button type="submit" id="submit" class="btn btn-primary">Add User</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script src="/js/chosen.jquery.js"></script>
<script>

  function checkUser(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var id = $('#user_name').val();

    $.ajax({
      type: "POST",
      url: "/admin/checkUser",
      data:{ _token: CSRF_Token, id: id},
      success:function(msg){
       if(msg.message == 'Already Exist'){
        $('#user-error').text(msg.message);
        $('#submit').attr('disabled',true);
      }     
      else{
        $('#user-error').empty();      
        $('#submit').prop('disabled',false);
      }
    }
  });
  }



  $('#parent_user').on('change',function(){
    $('#vehicle').show();
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var user = $('#parent_user').val();


    $.ajax({
      type: "POST",
      url: "/admin/getVehicles",
      data:{ _token: CSRF_Token, id: user},
      success:function(msg){
        $('#vehicle_id').html(msg.select);
        
        $(".chosen-select").chosen();        
        $('.chosen-results').html(msg.ul);
        $("#form_field").trigger("liszt:updated");

      }
    });
  });


</script>

@endsection