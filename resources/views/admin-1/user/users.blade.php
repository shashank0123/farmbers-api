<?php use App\User; ?>
@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif  

    
    <hr> 
    <legend>All {{$type}}</legend>
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>  
       <th>User Name</th>
       <th>Email</th>
       <th>Mobile</th>
       <th>Mobile Model</th>
       @if($type == 'sub-user')
       <th>Parent</th>
       @endif
       @if($type == 'user')
       <th>View Sub Users</th>
       @endif
       <th>View Assigned Vehicles</th>
       <th>Status</th>
       <th>Created At</th>
       <th colspan="2">Action</th>
     </tr>
   </thead>
   <thead id="user-section">

    @php $i=1; @endphp
    @if(!empty($users))
    @foreach($users as $user)
    <tr>
     <td>{{$i}}</td> 
     <td>{{$user->name}}</td>
     <td>{{$user->email}}</td>
     <td>@if(!empty($user->mobile)){{$user->mobile}}@endif</td>
     <td>@if(!empty($user->mobile_model)){{$user->mobile_model}}@endif</td>
     @if($type == 'sub-user')
     <td>
       <?php $parent = User::where('id',$user->parent_user)->first();
       if(!empty($parent))
       echo ucfirst($parent->name); ?>
     </td>
     @endif

     @if($type == 'user')
       <td><a href="{{url('admin/sub_users_list/'.$user->id)}}">View Sub Users</a></td>
       @endif

    <td>
      <a href="{{url('admin/user_vehicles_list/'.$user->id)}}">View vehicles list</a>
    </td>
     <td>{{$user->account_status}}</td>
     <td>{{explode(' ',$user->created_at)[0]}}</td>
     <td style="width: 110px;">
      <?php if ($user->account_status == 'active'){?>
      <a href="{{url('admin/bar_user/'.$user->id)}}" class="btn btn-success  mb-1 ml-1">Bar</a> 
      <?php }?>

      <?php if ($user->account_status == 'barred'){?>
      <a href="{{url('admin/unbar_user/'.$user->id)}}" class="btn btn-success  mb-1 ml-1">Unbar</a> 
      <?php }?>

      <a href="{{url('admin/edit_user/'.$type.'/'.$user->id)}}" class="btn btn-success  mb-1 ml-1"><i class="fa fa-pencil"></i></a> 
      
       <form style="float: right" action="{{url('admin/delete_user/'.$type.'/'.$user->id)}}" method="post">
        {{csrf_field()}}
        <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$user->id}})"><i class="fa fa-remove"></i></button>
        <input type="submit" hidden="hidden" id="form{{$user->id}}" class="btn btn-danger btn-xs">
      </form>
    </td>
  </tr>
  @php $i++ @endphp
  @endforeach
  @endif


</thead>
</table>  
@if($i==1)
  <div style="text-align: center;"><h4>No result found</h4></div>
  @endif
</div>

</div>
</div>

@endsection

@section('script')
<!-- Begin Page Content -->
<script type="text/javascript">
  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }



  function getUserSearch(){
    var search = $('#user_search').val();
    var type = @php echo "'".$type."'"; @endphp
  // alert(search);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-users',
   type: 'POST',
   data: {_token: CSRF_TOKEN, search: search,type: type },
   success: function (data) {
     $('#user-section').empty();
     $('#user-section').html(data);
   },
   failure: function (data) {
     alert('Something Went Wrong');
   }
 });
  }

</script>

@endsection