<?php use App\User; ?>
@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <a class="btn btn-warning" href="{{url('admin/create-subadmin')}}">Add Sub Admin</a>
    <hr> 
    <legend>All Sub Admins</legend>
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Sub Admin Name</th>
       <th>Email</th>
       <th>Mobile</th>
       <th>Created At</th>
       <th colspan="2">Action</th>
     </tr>
   </thead>
   <thead>

    @php $i=1; @endphp
    @if(!empty($users))
    @foreach($users as $user)
    <tr>
     <td>{{$i}}</td> 
     <td>{{$user->name}}</td>
     <td>{{$user->email}}</td>
     <td>@if(!empty($user->mobile)){{$user->mobile}}@endif</td>    
     <td>{{explode(' ',$user->created_at)[0]}}</td>
     <td style="width: 110px;">
      
      <a href="{{url('admin/edit-subadmin/'.$user->id)}}" class="btn btn-success  mb-1 ml-1"><i class="fa fa-pencil"></i></a> 
      
       <form style="float: right" action="{{url('admin/delete-subadmin/'.$user->id)}}" method="post">
        {{csrf_field()}}
        <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$user->id}})"><i class="fa fa-remove"></i></button>
        <input type="submit" hidden="hidden" id="form{{$user->id}}" class="btn btn-danger btn-xs">
      </form>
    </td>
  </tr>
  @php $i++ @endphp
  @endforeach
  @endif


</thead>
</table>	
@if($i==1)
  <div style="text-align: center;"><h4>No result found</h4></div>
  @endif
</div>

</div>
</div>
<!-- Begin Page Content -->
<script type="text/javascript">
  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }
</script>

@endsection