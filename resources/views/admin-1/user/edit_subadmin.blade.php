@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
   <!-- Page Heading -->
   <!-- Content Row -->
   <div class="row">
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-12 col-md-12 mb-4 pt-4">

         @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
         <a class="btn btn-warning" href="{{url('admin/subadmin')}}">Back</a> 
         <hr>
         <form action="{{ url('admin/update-subadmin/'.$user->id) }}" method="POST" role="form">
            {{ csrf_field() }}
            <legend>Edit Sub Admin</legend>
            @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert-danger">* {{$error}}</div>
            @endforeach
            @endif
            <div class="row">
               <div class="col-sm-12">
                 <div class="form-group">
                   <label for="name">User Name</label>
                   <input type="text" class="form-control" name="name" id="name" placeholder="Name Here" value="{{$user->name}}"  required="required">
                </div>               
             </div>

            

             <div class="col-sm-12">
               <div class="form-group">
                  <label for="mobile">Mobile</label>
                  <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="@if(!empty($user->mobile)){{$user->mobile}}@endif" required="required" pattern="[6-9]{1}[0-9]{9}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
               </div>
            </div>

            <div class="col-sm-12">
               <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email}}">
               </div>
            </div>

            <div class="col-sm-12">
               <div class="form-group">
                  <label for="activation_date">Activation Date</label>
                  <input type="text" class="form-control datepicker" id="activation_date" name="activation_date" placeholder="Activation Date" value="@if(!empty($user->activation_date)){{$user->activation_date}}@endif"> 
               </div>
            </div>

            <div class="col-sm-12">
               <div class="form-group">
                  <label for="expiry_date">Expiry Date</label>
                  <input type="text" class="form-control datepicker" id="expiry_date" name="expiry_date" placeholder="Expiry Date" value="@if(!empty($user->expiry_date)){{$user->expiry_date}}@endif">
               </div>
            </div>

            <div class="col-sm-12">
               <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
               </div>
            </div>

            <div class="col-sm-12">
               <div class="form-group">
                  <label for="cpassword">Confirm Password</label>
                  <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password">
               </div>
            </div>

         </div>
         <button type="submit" class="btn btn-primary">Update</button>
      </form>

   </div>
</div>
</div>
<!-- Begin Page Content -->

@endsection()

@section('script')
<script src="/js/chosen.jquery.js"></script>
<script type="text/javascript">
  
</script>
@endsection