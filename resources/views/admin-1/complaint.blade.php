@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <a class="btn btn-warning" href="{{url('admin/all-complaints')}}">All Complaints</a>
    <hr> 

<form class="pb-5" method="POST" action="/admin/complaint/submit">
  @csrf
  <div class="form-group">
    <label for="name">Full Name</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required="required">
  </div>
   <div class="form-group">
    <label for="mobile">Mobile No:</label>
    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile No:" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" pattern="[6-9]{1}[0-9]{9}" required="required">
  </div>
   <div class="form-group">
    <label for="vehicle_no">Vehicle  No:</label>
    <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="Vehicle  No:" required="required">
  </div>

 
  <div class="form-group">
    <label for="complaint">Complaint </label>
    <textarea class="form-control" id="complaint" name="complaint" rows="3" required="required"></textarea>
  </div>

   <button type="submit" class="btn btn-primary">Send Complaint </button>

</form>

</div>
</div>
</div>

@endsection