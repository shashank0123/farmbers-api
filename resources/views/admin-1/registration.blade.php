@extends('admin.layouts.header')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('home')}}">Back To Dashboard</a> 
  <hr>
  <form action="{{ url('admin/registration') }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <legend>Device</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif

    <!-- For Device -->
    <div class="row">
     <div class="col-sm-6">
       <div class="form-group">
         <label for="device_no">Device No.</label>
         <input type="text" class="form-control" name="device_no" id="device_no" placeholder="Device number (15 digits)"  required="required" pattern= "[1-9]{1}[0-9]{14}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onchange="checkDevice()">
         <p id="device-error" style="color: red"></p>
       </div>                
     </div>

     <div class="col-sm-6">
       <div class="form-group">
        <label for="sim_no">Sim No.</label>
        <input type="tel" class="form-control" id="sim_no" name="sim_no" placeholder="Sim number" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onchange="checkSim()" minlength="6" maxlength="13">
        <p id="sim-error" style="color: red"></p>
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="subscription_date">Activation Date</label>
        <input type="text" class="form-control datepicker" name="subscription_date" id="subscription_date" required="required" value="<?php echo date('d/m/Y')?>">
      </div>
    </div>


    <div class="col-sm-6">
       <div class="form-group">
        <label for="expiry_date">Expiry Date</label>
        <input type="text" class="form-control datepicker" name="expiry_date" id="expiry_date" required="required" value="">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="price">Price</label>
        <input type="number" class="form-control" id="price" name="price" placeholder="Price of Device" required="required" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="sim_type">Sim Type</label>
        <select name="sim_type" id="sim_type" class="form-control" >
          <option value="Vodafone">Vodafone</option>
          <option value="Airtel">Airtel</option>
        </select>
      </div>
    </div>

    <div class="col-sm-6">
       <div class="form-group">
        <label for="device_type">Device Type</label>
        <select name="device_type" id="device_type" class="form-control">
          <option value="ET300">ET300</option>
          <option value="BW">BW</option>
        </select>
      </div>
    </div>

  </div>

  <!-- For vehicle -->
  <hr>
  <legend>Vehicle</legend>

    <div class="row">
      <div class="col-sm-6">

        <div class="form-group">
          <label for="">Vehicle Brand</label>
          {{ Form::select('vehicle_brand', $brands , null, ['class'=>'form-control autoselect', 'id' => 'brand']) }}
        </div>

        <div class="form-group">
          <label for="">Vehicle Name</label>
          <input type="text" class="form-control" id="vehicle_name" name="vehicle_name" placeholder="Vehicle Name">
        </div>

        <div class="form-group">
          <label for="">Vehical Type</label>

          {{ Form::select('vehicle_type', $vehicle_type , null, ['class'=>'form-control autoselect']) }}
        </div>
      </div>
      <div class="col-sm-6">

       <div class="form-group">
        <label for="">Vehicle Number</label>
        <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="Registeration Plate Number" required="required" onchange="checkVehicle()">
        <p id="vehicle-error" style="color: red"></p>
      </div>

      <div class="form-group">
        <label for="">Vehicle Image</label>
        <input type="file" class="form-control" id="vehicle_image" name="vehicle_image" placeholder="Image of car">
      </div>

    </div>
  </div>
  <hr>

  <!-- For User -->
  <legend>User</legend>
  <div class="row">
    <div class="col-sm-12">
     <div class="form-group">
        <label for="">Owner Name/ID</label> &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" name="new_user" id="new_user" value="new" label="New User" onclick="fieldRequired()">&nbsp;New User 
        {{ Form::text('owner_id', null, ['class'=>'form-control autoselect', 'id' => 'ownername', 'value'=>'new' , 'onchange'=>'getRadio()']) }}
        <p id="user-error" style="color: red"></p>
      </div>
    </div>

    <div class="form-group autocomplete col-sm-6">
      <label for="">Password</label> 
      <input type="password" name="password" id="password" class="form-control autoselect">
    </div>

    <div class="form-group autocomplete col-sm-6">
      <label for="">Mobile Number</label> 
      <input type="tel" name="mobile" id="mobile" class="form-control autoselect" pattern="[6-9]{1}[0-9]{9}" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
    </div>
  </div>
  <button type="submit" id="submit" class="btn btn-primary">Add</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->

<script type="text/javascript">
  function getRadio(){
  if(document.getElementById('new_user').checked) {
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
        var owner = $('#ownername').val(); 

        $('#user-error').hide();
    $.ajax({
      type: "POST",
      url: "/admin/checkUser",
      data:{ _token: CSRF_Token, id: owner},
      success:function(msg){
       if(msg.message != 'Invalid User'){
        $('#user-error').show();
        $('#user-error').text('Username Already Exist');
      }  
      else{
        $('#user-error').hide();        
      }   
    }
  });
  }
  else{  
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var owner = $('#ownername').val();   
    $.ajax({
      type: "POST",
      url: "/admin/checkUser",
      data:{ _token: CSRF_Token, id: owner},
      success:function(msg){
       if(msg.message == 'Invalid User'){
        $('#user-error').text(msg.message);
      }     
    }
  });
  }
}

function fieldRequired(){
  if (document.getElementById('new_user').checked){
    $('#password').attr('required',true);
        $('#mobile').attr('required',true);
  }
  else{
    $('#password').prop('required',false);
        $('#mobile').prop('required',false);
      }
}

function checkVehicle(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var vehicle = $('#vehicle_no').val();
   
    $.ajax({
    type: "POST",
    url: "/admin/checkVehicle",
    data:{ _token: CSRF_Token, vehicle: vehicle},
    success:function(msg){
     if(msg.message == 'Vehicle Number Already Exist'){
        $('#vehicle-error').text(msg.message);
        $('#submit').prop('disabled',true);
     }     
     else{
        $('#vehicle-error').empty();      
        $('#submit').attr('disabled',false);
     }
    }
  });
  }

  function checkDevice(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var device = $('#device_no').val();
   
    $.ajax({
    type: "POST",
    url: "/admin/checkDevice",
    data:{ _token: CSRF_Token, device: device},
    success:function(msg){
     if(msg.message == 'Device Already Exist'){
        $('#device-error').text(msg.message);
        $('#submit').attr('disabled',true);
     }     
     else{
        $('#device-error').empty();      
        $('#submit').prop('disabled',false);
     }
    }
  });
  }

  function checkSim(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var sim = $('#sim_no').val();
   
    $.ajax({
    type: "POST",
    url: "/admin/checkSim",
    data:{ _token: CSRF_Token, sim: sim},
    success:function(msg){
     if(msg.message == 'Sim Number Already Exist'){
        $('#sim-error').text(msg.message);
        $('#submit').prop('disabled',true);
     }     
     else{
        $('#sim-error').empty();      
        $('#submit').attr('disabled',false);
     }
    }
  });
  }

</script>

@endsection()