@extends('admin.layouts.header')
@section('content')




               <!-- Begin Page Content -->
               <div class="container-fluid  bg-gradient-primary">
                  <!-- Page Heading -->
                  <!-- Content Row -->
                  <div class="row">
                     <!-- Earnings (Monthly) Card Example -->
                     <div class="col-xl-12 col-md-12 mb-4 pt-4">
                        <div class="map-responsive">
                           <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=Eiffel+Tower+Paris+France" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Begin Page Content -->
               <div class="container-fluid">
                  <div class="row mt-4">
                     <div class="col-xl-5 col-lg-6">
                        <div class="card  shadow mb-4 p-4">
                           <h4><span class="float-left vtitle" ><b>Device Stats</b></span>
                           </h4>
                           <div class="card-sec-cod2120 py-3">
                              <p><b><span class="text-primary"> Vehicle Type:</span> &nbsp; &nbsp;Car  </b></p>
                              <p><b><span class="text-primary">  Model Name :</span> &nbsp; &nbsp; Xput3 </b></p>
                              <p><b><span class="text-primary"> Register Vehicle No : </span> &nbsp; &nbsp;838747837 </b></p>
                              <p><b><span class="text-primary"> Purchase Year : </span> &nbsp; &nbsp;2019 </b></p>
                              <p><b><span class="text-primary"> Register Vehicle Address:  </span> &nbsp; &nbsp; Utaam Nagar Delhi, 1103737 </b></p>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-7 col-lg-6">
                        <div class="card  shadow mb-4 p-4">
                           <h4><span class="float-left vtitle" ><b>Vehicle Real Time Stats</b></span>
                           </h4>
                           <div class="row p-3">
                              <div class="col-4">
                                 <div class="userdash-sec1">
                                    <img src="img/place.png">
                                    <h5>View In Map</h5>
                                    <p>Real Time Tracking</p>
                                 </div>
                              </div>
                              <div class="col-4">
                                 <div class="userdash-sec1">
                                    <img src="img/performance.png">
                                    <h5>150 km/h</h5>
                                    <p>Real Time Speed</p>
                                 </div>
                              </div>
                              <div class="col-4">
                                 <div class="userdash-sec1">
                                    <img src="img/engine.png">
                                    <h5>On</h5>
                                    <p>Engine(On/Off)</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Content Row -->
                  <div class="row mt-4">
                     <div class="col-xl-7 col-lg-6">
                        <div class="card  shadow mb-4 py-2 px-4">
                           <!-- Nav pills -->
                           <ul class="nav nav-pills nav-tabsA1" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" data-toggle="pill" href="#tabA1">Today</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" data-toggle="pill" href="#tabA2">Yesterday</a>
                              </li>
                              <li><span class="float-right position-absolute btn-rgt">
                                 <button type="button" class="btn btn-outline-primary allsee">See All</button>
                                 </span>
                              </li>
                           </ul>
                           <!-- Tab panes -->
                           <div class="tab-content">
                              <div id="tabA1" class=" tab-pane active">
                                 <br>
                                 <div class="">
                                    <div class="row ">
                                       <div class="col-4">
                                          <div class="userdash-sec12">
                                             <img src="img/distance.png">
                                             <h5>250km</h5>
                                             <p>kms Travelled</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec12">
                                             <img src="img/fuel.png">
                                             <h5>15km/h</h5>
                                             <p>Fluel Economy</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec12">
                                             <img src="img/stopwatch.png">
                                             <h5>02h:30m:45s</h5>
                                             <p>Travel Duration</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div id="tabA2" class="container tab-pane fade">
                                 <br>
                                 <div class="">
                                    <div class="row ">
                                       <div class="col-4">
                                          <div class="userdash-sec12">
                                             <img src="img/distance.png">
                                             <h5>250km</h5>
                                             <p>kms Travelled</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec12">
                                             <img src="img/fuel.png">
                                             <h5>15km/h</h5>
                                             <p>Fluel Economy</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec12">
                                             <img src="img/stopwatch.png">
                                             <h5>02h:30m:45s</h5>
                                             <p>Travel Duration</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-5 col-lg-6">
                        <div class="card  shadow mb-4 p-3">
                           <!-- Card Header - Dropdown -->
                           <div>
                              <p><span class="float-left vtitle"><b> Vehicles Last Update</b></span>
                                 <span class="float-right"><button type="button" class="btn btn-outline-primary allsee">Get Summery</button></span>
                              </p>
                           </div>
                           <div class="card-sec-cod1 py-3">
                              <div class="row my-2 mx-auto align-items-center">
                                 <div class="col-sm-3">
                                    <div class="vlastupddate-img text-center mb-3">
                                       <img src="img/map.png">
                                    </div>
                                 </div>
                                 <div class="col-sm-9 pl-0">
                                    <div class="vlastupddate-text  ">
                                       <p class="mb-0">Last Location On <b>( 16th Oct 2019 at 09:59AM )</b><br>
                                          <b>Delhi , Dwarka More sec-21, New Delhi 2001839  </b>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="row my-2 mx-auto align-items-center">
                                 <div class="col-sm-3">
                                    <div class="vlastupddate-img text-center mb-3">
                                       <img src="img/car-locator.png">
                                    </div>
                                 </div>
                                 <div class="col-sm-9 pl-0">
                                    <div class="vlastupddate-text  ">
                                       <p class="mb-0">Current Status:  <b>Not Reached</b>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Content Row -->
                  <div class="row mt-4">
                     <div class="col-xl-5 col-lg-6">
                        <div class="card  shadow mb-4 p-3">
                           <!-- Card Header - Dropdown -->
                           <div>
                              <p><span class="float-left vtitle"><b> Vehicles Last Update</b></span>
                                 <span class="float-right"><button type="button" class="btn btn-outline-primary allsee">Get Summery</button></span>
                              </p>
                           </div>
                           <div class="card-sec-cod1 py-3">
                              <div class="row my-2 mx-auto align-items-center">
                                 <div class="col-sm-3">
                                    <div class="v-activity-img text-center mb-3">
                                       <img src="img/vector-03.png">
                                    </div>
                                 </div>
                                 <div class="col-sm-9 pl-0">
                                    <div class="v-activity-text  ">
                                       <p class="mb-0"> <b> Reached DLF Cyber Hub Phase-3</b><br>
                                          11:05am
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="row my-2 mx-auto align-items-center">
                                 <div class="col-sm-3">
                                    <div class="v-activity-img text-center mb-3">
                                       <img src="img/vector-03.png">
                                    </div>
                                 </div>
                                 <div class="col-sm-9 pl-0">
                                    <div class="v-activity-text  ">
                                       <p class="mb-0"> <b> Stop New Delhi More Step-3</b><br>
                                          11:05am
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="row my-2 mx-auto align-items-center">
                                 <div class="col-sm-3">
                                    <div class="v-activity-img text-center mb-3">
                                       <img src="img/vector-03.png">
                                    </div>
                                 </div>
                                 <div class="col-sm-9 pl-0">
                                    <div class="v-activity-text  ">
                                       <p class="mb-0"> <b> Stop New Delhi More Step-3</b><br>
                                          11:05am
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="row my-2 mx-auto align-items-center">
                                 <div class="col-sm-3">
                                    <div class="v-activity-img text-center mb-3">
                                       <img src="img/vector-03.png">
                                    </div>
                                 </div>
                                 <div class="col-sm-9 pl-0">
                                    <div class="v-activity-text  ">
                                       <p class="mb-0"> <b> Stop New Delhi More Step-3</b><br>
                                          11:05am
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-7 col-lg-6">
                        <div class="card  shadow mb-4 p-3">
                           <!-- Card Header - Dropdown -->
                           <div>
                              <p><span class="float-left vtitle"><b> Engine Shutdown</b></span>
                                 <span class="float-right"><button type="button" class="btn btn-outline-primary allsee">History</button></span>
                              </p>
                           </div>
                           <div class="card-sec-cod1 py-2">
                              <div class="row my-2 mx-auto text-center align-items-center">
                                 <div class="col-sm-6">
                                    <div class=" v-stdn text-center ">
                                       <img class="img-responsive" src="img/engine-01.png">
                                       <p >Shutdown the engine , <br> If your vehicle disturbing </p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 pl-0">
                                    <div class="vlastupddate-text  ">
                                       <label class="switch">
                                       <input type="checkbox" checked>
                                       <span class="slider round"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="card  shadow mb-4 py-4 px-4">
                           <!-- Nav pills -->
                           <ul class="nav nav-pills nav-tabsA1" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" data-toggle="pill" href="#tabB1">Kms Travelled</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" data-toggle="pill" href="#tabB2">Expenses Incurred</a>
                              </li>
                              <li><span class="float-right position-absolute btn-rgt">
                                 <button type="button" class="btn btn-outline-primary allsee">See All</button>
                                 </span>
                              </li>
                           </ul>
                           <!-- Tab panes -->
                           <div class="tab-content">
                              <div id="tabB1" class=" tab-pane active">
                                 <br>
                                 <div class="">
                                    <div class="row ">
                                       <div class="col-4">
                                          <div class="userdash-sec123">
                                             <h5>250km</h5>
                                             <p>This Month</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec123">
                                             <h5>15km</h5>
                                             <p>August</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec123">
                                             <h5>15km</h5>
                                             <p>July</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div id="tabB2" class="container tab-pane fade">
                                 <br>
                                 <div class="">
                                    <div class="row ">
                                       <div class="col-4">
                                          <div class="userdash-sec123">
                                             <h5>250km</h5>
                                             <p>Today</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec123">
                                             <h5>15km</h5>
                                             <p>Yesterday</p>
                                          </div>
                                       </div>
                                       <div class="col-4">
                                          <div class="userdash-sec123">
                                             <h5>300km</h5>
                                             <p>Last Hour</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>






                   <!-- Content Row -->
                  <div class="row mt-4">
                     <div class="col-xl-5 col-lg-6">
                        <div class="card  shadow mb-4 p-3">
                           <!-- Card Header - Dropdown -->
                           <div>
                              <p><span class="float-left vtitle"><b> SOS Service</b></span>
                                 
                              </p>
                           </div>
                             <div class="card-sec-cod1 py-4">
                              <div class="row my-2 mx-auto text-center align-items-center">
                                 <div class="col-sm-12">
                                    <div class=" v-stdn2 text-center ">
                                       <img class="img-responsive" src="img/SOS-button.png">
                                       <p > In case of emergency, click on the button </p>
                                    </div>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-7 col-lg-6">
                        <div class="card  shadow mb-4 p-3">
                           <!-- Card Header - Dropdown -->
                          <div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class=" active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</a>
      <a class="" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
      <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
      <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content ml-4" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">No Stopped!!!!!!!!!!!!!!</div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
      <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
    </div>
  </div>
</div>


                        </div>
                    
                     </div>
                  </div>





               </div>
               <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->






            
@endsection          