@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    <form action="{{url('admin/searched-activities')}}" method="POST">
      @csrf
      <div class="row">      
        <div class="col-sm-4"></div>
        <div class="col-sm-3">            
          <input type="text" name="from_date" id="from_date" class="form-control datepicker" placeholder="From (Time)" required="required" value="{{$from}}">          
        </div>
        <div class="col-sm-3">               
          <input type="text" name="to_date" id="to_date" class="form-control datepicker" placeholder="To (Time)" required="required" value="{{$to}}">          
        </div>
        <div class="col-sm-2">                      
          <button type="submit" name="submit" id="submit" class="btn btn-primary">Search</button>          
        </div>       
      </div>
    </form>

    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <hr> 
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Vehicle</th>
       <th>User</th>
       <th>Activity</th>
       <th>Title</th>
       <th>Date</th>
     </tr>
   </thead>
   <thead>
    @php $i=1; @endphp
    @if(!empty($activities))
    @foreach($activities as $d)
    <tr>
     <td>{{$i++}}</td> 
     <td>{{$d->vehicle_name}}</td>
     <td>{{$d->name}}</td>
     <td>{{$d->description}}</td>
     <td>{{$d->title}}</td>
     <td>{{$d->created_at}}</td>
   </tr>
   @endforeach
   @endif
 </thead>
</table>	
@if($i==1)
<div style="text-align: center; padding: 50px">No Result Found</div>
@endif
</div>

</div>
</div>
<!-- Begin Page Content -->

@endsection()