@extends('admin.layouts.header')


@section('content')

  <div class="container-fluid ">
                  <!-- Page Heading -->
                  <!-- Content Row -->
                  <div class="row">
                     <!-- Earnings (Monthly) Card Example -->
                     <div class="col-xl-12 col-md-12 mb-4 pt-4">

@if(isset($success))
    <div class="alert alert-success"> {{$success}} </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(isset($brands))
    {{ Form::model($brands, ['route' => ['brands.update', $brands->id],'class'=>'form-horizontal', 'files' => true, 'method' => 'patch']) }}
@else

    {!!  Form::open(array('route' =>'brands.store','class'=>'form-horizontal','files' => true )) !!}

@endif

   <a class="btn btn-warning" href="{{url('admin/brands')}}">Back</a> 
                        <hr>

                <div class="row">
                             <div class="col-sm-12">  

                             <form>
                                 
                                 <legend>Add Brand</legend>
                                         
                                    
                <div class="form-group">
                    {!! Form::label('title','Brand Name :',['class'=>'control-label'])!!}
                    <div class="">
                        {!! Form::text('brand_name',old('brand_name'),['class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('title','Description :',['class'=>'control-label'])!!}
                    <div class="">
                        {!! Form::text('description',old('description'),['class'=>'form-control'])!!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('title','Brand Image :',['class'=>'control-label'])!!}
                    <div class="">
                        {!! Form::file('brand_img',old('brand_img'),['class'=>'form-control'])!!}
                    </div>
                </div>	
               
                <div class="form-group">
                    <div class=" col-sm-offset-5">
                        {!! Form::submit('submit',array('class' => 'btn btn-primary'))!!}
                    </div>
                </div>

                             </form>        

            {!! Form::close() !!}                  
                   </div>
                   </div>                      

        </div>
    </div>
</div>
                        

                    
@endsection


