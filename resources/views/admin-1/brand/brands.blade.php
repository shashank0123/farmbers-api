@extends('admin.layouts.header')


@section('content')
<div class="container-fluid  ">
    
                  <!-- Page Heading -->
                  <!-- Content Row -->
                  <div class="row">
                     <!-- Earnings (Monthly) Card Example -->
                     <div class="col-xl-12 col-md-12 mb-4 pt-4">
                      @if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif

<div class="row">
    <div class="col-md-6">        
     <a class="btn btn-warning" href="{{url('brands/create')}}">Add Brand</a> 
   </div>
   <div class="col-md-6">
    <input type="search" class="form-control" name="brand_search" id="brand_search" placeholder="Search brand" onkeyup="getBrandSearch()" style="width: 80%" />        
  </div>
</div>

                        
                        <hr>
                        <table style="width: 100%" class="table table-bordered">
                            <thead>
                                <tr class="alert-info">
                                    <th>#</th>
                                    <th>Brand Name</th>
                                    <th>Brand Image</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="brand-section">
                             <?php $i=1; ?>
                            @foreach ($brands as $brand)
                            <tr>
                                <td> {{ $i }}</td>
                                <td>{{ $brand->brand_name }}</td>
                                <td><img src="{{ asset('images/brand/'.$brand->brand_img) }}" alt="" style="width: 100px; height: auto;"></td>
                                <td>{{ $brand->description }}</td>
                                <td>
                                    {{ link_to_route('brands.edit','Edit',[$brand->id], ['class'=> 'btn btn-success  mb-1 ml-1']) }}

                                        {!! Form::open(['method'=>'delete','route'=>['brands.destroy', $brand->id],'class'=>'col-md-6' ]) !!}

                                    {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!}
                               </td>
                            </tr>
                            <?php $i++; ?> 
                            @endforeach
                           </tbody>
                        </table>    
                        
                     </div>
                        
                  </div>
               </div>
               <!-- Begin Page Content -->



                   
@endsection

@section('script')
<script type="text/javascript">
 
  function getBrandSearch(){
    var search = $('#brand_search').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/search-brands',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search },
     success: function (data) {
       $('#brand-section').empty();
       $('#brand-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }
</script>
@endsection

