@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
  
@if($type=='distance')
<form class="pb-5" action="{{url('admin/distance_report')}}" method="GET">
 @endif
 @if($type=='trip')
<form class="pb-5" action="{{url('admin/trip_report')}}" method="GET">
 @endif
<div class="form-row">
    <div class="form-group  col-md-12 ">
      <label for="inputVehicle">Select Vehicle</label>
      <select id="inputVehicle" name="inputVehicle" class="form-control" required="required">
        @foreach ($vehicles as $vehicle)
          <option value="{{$vehicle->vehicle_id}}">{{$vehicle->vehicle_no}}</option>
        @endforeach
        <option>...</option>
      </select>
    </div>
    
  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="from_date">From Select Date</label>
    <input type="text" class="form-control datepicker" id="inputDate" name="from_date" placeholder="dd/mm/yyyy" required="required">
    </div>
    <div class="form-group col-md-6">
     <label for="to_date"> to Select Date </label>
    <input type="text" class="form-control datepicker" id="inputDate2" name="to_date" placeholder="dd/mm/yyyy" required="required">
    </div>
  </div>


 


 <div class="form-group">
  <button type="submit" class="btn btn-primary">SUBMIT</button>
</div>
</form>

</div>
</div>
</div>

@endsection