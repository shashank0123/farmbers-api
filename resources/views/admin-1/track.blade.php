<?php  
use App\User;
use App\Vehicle;
use App\Device;
use App\AssignedVehicle;
use App\DeviceToVehicle;

//For Vehicles
$assigned_v = AssignedVehicle::select('vehicle_id')->get();
$assign_v = array();
foreach ($assigned_v as $key) { $assign_v[] = $key->vehicle_id; }
$assigned_vehicles = Vehicle::whereIn('vehicle_id',$assign_v)->get();

//For Devices

$assigned_devices = Device::distinct()->get(['device_type']);

$users = User::all();
?>

@extends('admin.layouts.header')
@section('content')



<!-- Begin Page Content -->
<div class="container-fluid  bg-gradient-primary">

  <style>
  .table { background-color: #fff !important; height: 350px; overflow-y: auto  }

  #table-response { height: 500px !important; background-color: #eee; border-radius: 8px; }
  
  #table-response label {background-color: #006699; color: #fff; width: 100% ; padding: 20px 5px ; text-align: center; border-top-left-radius: 8px; border-top-right-radius: 8px; font-weight: 500; font-size: 18px }
  .table{ width: 98%; margin: 0 1% }

  .row{  margin-bottom: 0px }

  .clickable { cursor: pointer; }
  .clickable:hover { color: #006699  }
  .form-control { width: 96%;  margin: 0% 2% 2% ;}
  #search-sec p { padding-top: 10px }


</style>
<!-- Page Heading -->
<!-- Content Row -->
<div class="row">   

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-6 col-md-6 col-lg-6 mb-4 pt-4" >
    <div id="table-response">
      <div class="map-responsive">
        <label>Vehicle(s) Details on Map </label>
        <div class="row">
            <div class="col-md-6" id="search-sec">
                 <p><b>Current User : </b>{{'asc123'}}</p>
            </div>
            <div class="col-md-6">
              <input type="search" class="form-control" name="vehicle_map_search" id="vehicle_map_search" placeholder="Vehicle No">
            </div>
        </div>       
        
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.766788144537!2d77.27259541455975!3d28.636750890641974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfde68e92d50b%3A0xa3079c526ec485c9!2sv3s%20mall!5e0!3m2!1sen!2sin!4v1575362096798!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
    </div>
  </div>

  <div class="col-xl-6 col-md-6 col-lg-6 mb-4 pt-4">
    <div id="table-response">
      <table class="table table-bordered table-responsive table-hover">
        <label>User Wise Working Vehicle(s)</label>

        <div class="row">
            <div class="col-md-6" id="search-sec">                 
            </div>
            <div class="col-md-6">
              <input type="search" class="form-control" name="user_search" id="user_search" placeholder="Search User" onkeyup="getDynamicUser()">
            </div>
        </div>

        <thead >
          <th>S.No.</th>
          <th>User ID</th>
          <th>Working Vehicle/All Vehicles</th>
        </thead>
        <tbody id="user-section">
          @if(!empty($users))
          <?php $i=1; ?>
          @foreach($users as $user)
          <?php $total_vehicles = Vehicle::where('owner_id',$user->id)->count(); ?>
          <tr>
            <td>{{$i++}}</td>
            <td class="clickable" onclick="getUser({{$user->id}})">{{$user->name}}</td>
            <td>0/{{$total_vehicles}}</td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>

</div> 


<div class="row">
  <div class="col-xl-6 col-md-6 col-lg-6 mb-4 pt-4">
    <div id="table-response">
      <table class="table table-bordered table-responsive table-hover">
        <label>Vehicle(s) Details</label>
        <div class="row">
            <div class="col-md-6" id="search-sec">                 
            </div>
            <div class="col-md-6">
              <input type="search" class="form-control" name="vehicle_search" id="vehicle_search" placeholder="Search Vehicle" onkeyup="getDynamicVehicle()">
            </div>
        </div>
        <thead>
          <th>S.No.</th>
          <th>Vehicle No</th>
          <th>Last Update</th>
          <th>Status</th>
          <th>Speed</th>
          <th>Activation Status</th>
          <th>Power Status</th>
          <th>Ac St.</th>
        </thead>

        <tbody id="vehicle-section">
          @if(!empty($assigned_vehicles))
          <?php $i=1; ?>
          @foreach($assigned_vehicles as $vehicle)
          <tr>
            <td>{{$i++}}</td>
            <td>{{$vehicle->vehicle_no}}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>

  <div class="col-xl-6 col-md-6 col-lg-6 mb-4 pt-4">
    <div id="table-response">
      <table class="table table-bordered table-responsive table-hover" >
        <label>Device Wise Working Vehicle(s)</label>

        <div class="row">
            <div class="col-md-6" id="search-sec">                 
            </div>
            <div class="col-md-6">
              <input type="search" class="form-control" name="device_search" id="device_search" placeholder="Search Device" onkeyup="getDynamicDevice()">
            </div>
        </div>

        <thead >
          <th>S.No.</th>
          <th>Device Type</th>
          <th>Working Devices(s)/All Devices</th>
        </thead>

        <tbody id="device-section">
          @if(!empty($assigned_devices))
          <?php $i=1; ?>
          @foreach($assigned_devices as $device)
          <?php
          $dev = Device::where('device_type',$device->device_type)->count();
          ?>
          <tr>
            <th>{{$i++}}</th>
            <th>{{$device->device_type}}</th>
            <th>0/{{$dev}}</th>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<!-- Begin Page Content -->

<script type="text/javascript">
 
 function getDynamicUser(){
  var search = $('#user_search').val();
  // alert(search);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         url: '/dynamic-user-search',
         type: 'POST',
         data: {_token: CSRF_TOKEN, search: search },
         success: function (data) {
           $('#user-section').empty();
           $('#user-section').html(data);
         },
         failure: function (data) {
           alert('Something Went Wrong');
         }
      });
  }

  function getDynamicVehicle(){
  var search = $('#vehicle_search').val();
  // alert(search);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         url: '/dynamic-vehicle-search',
         type: 'POST',
         data: {_token: CSRF_TOKEN, search: search },
         success: function (data) {
           $('#vehicle-section').empty();
           $('#vehicle-section').html(data);
         },
         failure: function (data) {
           alert('Something Went Wrong');
         }
      });
  }

  function getDynamicDevice(){
  var search = $('#device_search').val();
  // alert(search);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         url: '/dynamic-device-search',
         type: 'POST',
         data: {_token: CSRF_TOKEN, search: search },
         success: function (data) {
           $('#device-section').empty();
           $('#device-section').html(data);
         },
         failure: function (data) {
           alert('Something Went Wrong');
         }
      });
  }

   function getUser(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
 
      $.ajax({
         url: '/dynamic-tracking',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id },
         success: function (data) {
           // Swal(data.cartItem);
           $('#vehicle-section').empty();
           $('#vehicle-section').html(data);

           // $('#device-section').empty();
           // $('#device-section').html(data.vehicles);
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
  }
</script>
@endsection