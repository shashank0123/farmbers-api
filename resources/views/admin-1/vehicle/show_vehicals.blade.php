<?php
use App\Brand;
use App\User;
use App\DeviceToVehicle;
use App\VehicleToUser;
use App\LocationData;
?>

@extends('admin.layouts.header')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">

     @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <div class="row">
      <div class="col-md-6">        
        <a class="btn btn-warning" href="{{url('admin/add_vehicals')}}">Add Vehicles</a> 

      </div>
      <div class="col-md-6">
        <input type="search" class="form-control" name="vehicle_search" id="vehicle_search" placeholder="Search Vehicle" onkeyup="getVehicleSearch()" style="width: 80%">        
      </div>
    </div>


    
    
    <hr>
    <table class="table table-bordered table-responsive">
     <thead>
      <tr class="alert-info">
       <th>SN</th>  
       <th>User ID</th> 
       <th>Total Vehicles</th>	
       <th>Vehicle No</th>                              
       <th>Device No</th>                             
       <th>Device Type</th>                             
       <th>Port</th>                              
       <th>Sim No</th>                              
       <th>Operator</th>                              
       <th>Last Update</th>                             
       <th>Power Status</th>                              
       <th>Activation Status</th>                             
       <th>Installation Date</th>                             
       <th>Price</th>                             
       <th>Remaining Subscription</th>                        			

       <!-- <th>Image</th> -->
       <th colspan="2">Activity</th>
     </tr>
     <thead id="vehicle-section">
      @php $i=1; @endphp
      @if(!empty($vehicles))

      @foreach($vehicles as $v)

      <?php 
      $count_user = VehicleToUser::where('user_id',$v->id)->count();

       $last_updated="";$activation_status='Inactive';$color="blue";$power_status='Not Connected';
          //to get the details of the vehicle
            $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $v->vehicle_id)->first();

            if ($assigned_vehicles1){
              $current_data = LocationData::where('imei', '0'.$assigned_vehicles1->device_no)->orderBy('id', 'DESC')->first();  

              if($assigned_vehicles1->device_status=='active')
                $activation_status = 'Active';

              if(isset($current_data) && $current_data->created_at)
                $last_updated = $current_data->created_at;


              if(isset($current_data) && $current_data->speed)
                $speed = $current_data->speed;


              if(isset($current_data)){

                if ($current_data->terminal == 44 || $current_data->terminal == 02 || $current_data->terminal == 68 || $current_data->terminal == 20){
                  $color = "red";

                }
                else if ($current_data->terminal == 46 || $current_data->terminal == 70){
                  $color = "yellow";
                }

                if ($current_data->terminal == 53){
                  $color = "blue";
                }

                if ($current_data->terminal == 68 || $current_data->terminal == 196){
                  $power_status = "Disconnected";
                }
                else{
                  $power_status = 'Connected';
                }
                if ((int)$current_data->speed > 0){
                  $color = "green";
                }
              }

              if($color == 'blue'){
                $status = 'Not Connected';
              }
              else{
                $status = 'Connected';  
              }
             
            }
      ?>

      <tr>
       <td>{{$i}}</td> 
       <td>@if(isset($v->user_name)){{$v->user_name }}@endif</td>
       <td>{{$count_user}}</td>
       <td>@if(isset($v->vehicle_no)){{$v->vehicle_no}}@endif</td>
       <td>@if(isset($v->device_no)){{$v->device_no}}@endif</td>
       <td>@if(isset($v->device_type)){{$v->device_type}}@endif</td>
       <td>6969</td>
       <td>@if(isset($v->sim_no)){{$v->sim_no}}@endif</td>
       <td>@if(isset($v->sim_type)){{$v->sim_type}}@endif</td>
       <td>{{$last_updated}}</td>
       <td>{{$power_status}}</td>
       <td>@if($v->device_status){{$v->device_status}}@else{{'Inactive'}}@endif</td>
       <td>@if($v->subscription_date){{$v->subscription_date}}@endif</td>
       <td>@if($v->price){{$v->price}}@endif</td>
       <td>
        <?php
        
        $v->device_expiry_date = str_replace('/', '-', $v->device_expiry_date);
        $diff = date_diff(date_create($v->device_expiry_date),date_create(date('Y-m-d')));

        ?><?php print_r(($diff->y*365)+($diff->m*30)+$diff->d); ?> Days
      </td>

    <td><a href="{{url('admin/edit_vehicals/'.$v->vehicle_id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 

     <form action="{{url('admin/delete_vehicals/'.$v->vehicle_id)}}" method="POST">
      {{csrf_field()}}

      <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$v->vehicle_id}})"><i class="fa fa-remove"></i></button>
      <input type="submit" hidden="hidden" id="form{{$v->vehicle_id}}" class="btn btn-danger btn-xs">
    </form>
  </td>
</tr>
@php $i++ @endphp
@endforeach
@endif
</thead>
</thead>
</table>	

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">
  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }



  function getVehicleSearch(){
    var search = $('#vehicle_search').val();
  // alert(search);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-vehicles',
   type: 'POST',
   data: {_token: CSRF_TOKEN, search: search },
   success: function (data) {
     $('#vehicle-section').empty();
     $('#vehicle-section').html(data);
   },
   failure: function (data) {
     alert('Something Went Wrong');
   }
 });
}

</script>
@endsection

