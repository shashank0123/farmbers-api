@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
     @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    <a class="btn btn-warning" href="{{url('admin/show_vehicals')}}">Back</a> 
    <hr>
    <form action="{{url('admin/store_vehicals')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

      <legend>Add Vehicles</legend>

      @if ($errors->any())
      @foreach($errors->all() as $error)
      <div class="alert-danger">* {{$error}}</div>
      @endforeach
      @endif
      <div class="row">
       <div class="col-sm-6">

         <div class="form-group">
          <label for="">Vehicle Brand</label>
          {{ Form::select('vehicle_brand', $brands , null, ['class'=>'form-control autoselect', 'id' => 'brand']) }}
        </div>

        <div class="form-group">
          <label for="">Vehicle Name</label>
          <input type="text" class="form-control" id="vehicle_name" name="vehicle_name" placeholder="Vehicle Name">
        </div>

        <div class="form-group">
          <label for="">Vehical Type</label>

          {{ Form::select('vehicle_type', $vehicle_type , null, ['class'=>'form-control autoselect']) }}
        </div>
      </div>
      <div class="col-sm-6">

       <div class="form-group">
        <label for="">Vehicle Number</label>
        <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="Registeration Plate Number" required="required" onchange="checkVehicle()">
        <p id="vehicle-error" style="color: red"></p>
      </div>

      <div class="form-group">
        <label for="">Owner Name/ID</label>  &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" name="new_user" id="new_user" onclick="fieldRequired()" value="new" label="New User">&nbsp;New User 
        {{ Form::text('owner_id', null, ['class'=>'form-control autoselect', 'id' => 'ownername', 'value'=>'new' , 'onchange'=>'getRadio()']) }}
        <p id="user-error" style="color: red"></p>
      </div>    

      <div class="form-group">
        <label for="">Vehicle Image</label>
        <input type="file" class="form-control" id="vehicle_image" name="vehicle_image" placeholder="Image of car">
      </div>

    </div>
  </div>
  <div class="new-user row">
    <div class="form-group autocomplete col-sm-6">
      <label for="">Password</label> 
      <input type="password" name="password" id="password" class="form-control autoselect"> 
    </div>

    <div class="form-group autocomplete col-sm-6">
      <label for="">Mobile Number</label> 
      <input type="tel" name="mobile" id="mobile" class="form-control autoselect"  oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" pattern="[6-9]{1}[0-9]{9}"> 
     
    </div>
  </div>
  <button type="submit" id="submit" class="btn btn-primary">Save</button>
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script>

  var countries = <?php echo json_encode($users);?>;

  autocomplete(document.getElementById("ownername"), countries);
  
  $(document).ready(function() { 
    $(".autoselect").select2({
      placeholder: "Select a brand",
      allowClear: true
    }); 

  });
  function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false;}
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < arr.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        /*make the matching letters bold:*/
        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
        b.innerHTML += arr[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function(e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
            });
        a.appendChild(b);
      }
    }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
    });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

function getRadio(){
  if(document.getElementById('new_user').checked) {
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
        var owner = $('#ownername').val(); 

        $('#user-error').hide();
    $.ajax({
      type: "POST",
      url: "/admin/checkUser",
      data:{ _token: CSRF_Token, id: owner},
      success:function(msg){
       if(msg.message != 'Invalid User'){
        $('#user-error').show();
        $('#user-error').text('Username Already Exist');
      }  
      else{
        $('#user-error').hide();        
      }   
    }
  });
  }
  else{  
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var owner = $('#ownername').val();   
    
    alert(owner); 
    $.ajax({
      type: "POST",
      url: "/admin/checkUser",
      data:{ _token: CSRF_Token, id: owner},
      success:function(msg){
       if(msg.message == 'Invalid User'){
        $('#user-error').text(msg.message);
      }     
      else{
        $('#user-error').empty();        
      }
    }
  });
  }
}

function fieldRequired(){
  if (document.getElementById('new_user').checked){
    $('#password').attr('required',true);
        $('#mobile').attr('required',true);
  }
  else{
    $('#password').prop('required',false);
        $('#mobile').prop('required',false);
      }
}


function checkVehicle(){    
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var vehicle = $('#vehicle_no').val();
   
    $.ajax({
    type: "POST",
    url: "/admin/checkVehicle",
    data:{ _token: CSRF_Token, vehicle: vehicle},
    success:function(msg){
     if(msg.message == 'Vehicle Number Already Exist'){
        $('#vehicle-error').text(msg.message);
        $('#submit').prop('disabled',true);
     }     
     else{
        $('#vehicle-error').empty();      
        $('#submit').attr('disabled',false);
     }
    }
  });
  }



</script>

@endsection