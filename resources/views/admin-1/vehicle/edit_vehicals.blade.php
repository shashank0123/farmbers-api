@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
     @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    <a class="btn btn-warning" href="{{url('admin/show_vehicals')}}">Back</a> 
    <hr>
    <form action="{{url('admin/update_vehicals/'.$vehicle->vehicle_id)}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

      <legend>Edit Vehicle's Detail</legend>

      @if ($errors->any())
      @foreach($errors->all() as $error)
      <div class="alert-danger">* {{$error}}</div>
      @endforeach
      @endif
      <div class="row">
        <div class="col-sm-6">

          <div class="form-group">
            <label for="vehicle_brandd">Vehicle Brand</label>
            <select class="form-control" id="vehicle_brand" name="vehicle_brand" placeholder="Vehicle Brand" required="required">
              <option>Select Brand</option>
              @if(!empty($brands))
              @foreach($brands as $brand)
              <option value="{{$brand->id}}" @if($vehicle->vehicle_brand == $brand->id){{'selected'}}@endif>{{$brand->brand_name}}</option>
              @endforeach
              @endif
            </select>          
          </div>

          <div class="form-group">
            <label for="">Vehicle Name</label>
            <input type="text" class="form-control" id="vehicle_name" name="vehicle_name" placeholder="Vehicle Name" value="{{$vehicle->vehicle_name ?? ''}}">
          </div>

          <div class="form-group">
            <label for="vehicle_type">Vehical Type</label>
            <select class="form-control" id="vehicle_type" name="vehicle_type" placeholder="Vehicle Type" required="required">
              <option>Select Brand</option>
              @if(!empty($vehicle_type))
              @foreach($vehicle_type as $type)
              <option value="{{$type}}" @if($vehicle->vehicle_type == $type){{'selected'}}@endif>{{$type}}</option>
              @endforeach
              @endif
            </select>
          </div>
        </div>
        <div class="col-sm-6">

         <div class="form-group">
          <label for="">Vehicle Number</label>
          <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="Registeration Plate Number" value="{{$vehicle->vehicle_no}}" required="required">
        </div>

        <div class="form-group">
          <label for="">Vehicle Image</label>
          <input type="file" class="form-control" id="vehicle_image" name="vehicle_image" placeholder="Image of car">
        </div>

        <div class="form-group">
          <input type="hidden" value="{{$vehicle->owner_id}}" name="old_owner" id="old_owner">
          <label for="">Owner Name</label>&nbsp;&nbsp; <!-- <b><i>OR</i></b> &nbsp;&nbsp;<input type="radio" name="new_user" label="New User" value="new_user" >New User  -->
          <select class="form-control" id="owner_id" name="owner_id" placeholder="Vehicle Type" >
            <option>Select Owner</option>
            @if(!empty($users))
            @foreach($users as $user)
            <option value="{{$user->id}}" @if($vehicle->owner_id == $user->id){{'selected'}}@endif>{{ucfirst($user->name)}}</option>
            @endforeach
            @endif
          </select>
        </div>

      </div>
    </div>
    <!-- <div class="new-user row" id="newUser">

      <div class="form-group  col-sm-6">
        <label for="owner_name">Owner Name</label> 
        {{ Form::text('owner_name', null, ['class'=>'form-control autoselect']) }}
      </div>
      <div class="form-group  col-sm-6">
        <label for="owner_email">Email</label> 
        {{ Form::email('owner_email', null, ['class'=>'form-control autoselect']) }}
      </div>

      <div class="form-group  col-sm-6">
        <label for="password">Password</label> 
        {{ Form::password('password', null, ['class'=>'form-control autoselect']) }}
      </div>

      <div class="form-group  col-sm-6">
        <label for="mobile">Mobile Number</label> 
        {{ Form::text('mobile', null, ['class'=>'form-control autoselect']) }}
      </div>
    </div> -->
    <button type="submit" class="btn btn-primary">Save</button>
  </form>

</div>
</div>
</div>
<!-- Begin Page Content -->

@endsection()

@section('script')
<script>

function getNewUser(){
  $('#owner_id').attr('disabled','disabled');
}

</script>
@endsection