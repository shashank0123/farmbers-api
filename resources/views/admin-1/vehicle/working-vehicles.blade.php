<?php
use App\Brand;
?>

@extends('admin.layouts.header')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">

     @if($message = Session::get('message'))
     <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    <h3>{{$key}}</h3>
    <!-- <a class="btn btn-warning" href="{{url('add_vehicals')}}">{{$key}}</a>  -->
    <hr>
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Name</th>
       <th>Type</th>
       <th>Vehicle No</th>                        			
       <th>Owner</th>
       <th>Image</th>
       <th colspan="2">Activity</th>
     </tr>
     <thead>
      @php $i=1; @endphp
      @if(!empty($vehicles))
      @foreach($vehicles as $v)
      
      <tr>
       <td>{{$i++}}</td> 
       <td>@if(!empty($v->vehicle_name)){{$v->vehicle_name }}@endif</td>
       <td>@if(!empty($v->vehicle_type)){{$v->vehicle_type }}@endif</td>
       <td>@if(!empty($v->vehicle_no)){{$v->vehicle_no }}@endif</td>
       <td>@if(!empty($v->name)){{$v->name }}@endif</td>
       <td>
        @if(!empty($v->vehicle_image))
        <img src="{{url('/upload/'.$v->vehicle_image)}} " style="width: 50px;height: 50px;">
        @endif
      </td>
      
      <td>
        @if(!empty($v->vehicle_id))<a href="{{url('admin/edit_vehicals/'.$v->vehicle_id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 
        
       <form action="{{url('admin/delete_vehicals/'.$v->vehicle_id)}}" method="POST">
        {{csrf_field()}}
        <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$v->vehicle_id}})"><i class="fa fa-remove"></i></button>
        <input type="submit" hidden="hidden" id="form{{$v->vehicle_id}}" class="btn btn-danger btn-xs">
      </form>
      @endif
    </td>
  </tr>
  @endforeach
  @endif
</thead>
</thead>
</table>	

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection


@section('script')
<script type="text/javascript">
function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }
</script>
@endsection