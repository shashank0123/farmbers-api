@extends('admin.layouts.header')
@section('content')
    

<!-- Begin Page Content -->
<div class="container-fluid">
   <!-- Page Heading -->
   <!-- Content Row -->
   <div class="row">
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-12 col-md-12 mb-4 pt-4">
         @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
         <a class="btn btn-warning" href="{{url('admin/assigned_list')}}">Back</a> 
         <hr>
         <form action="{{ url('admin/update_assigning/'.$assigned->id) }}" method="POST" role="form">
            {{ csrf_field() }}
            <legend>Edit Driver Assigned</legend>
            @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert-danger">* {{$error}}</div>
            @endforeach
            @endif
            <div class="row">
               <div class="col-sm-6">
                 <div class="form-group">
                   <label for="driver">Vehicle</label>
                   <select type="text" class="form-control" name="vehicle_id"  id="vehicle_id"  required="required">
                     <option value="">Select Vehicle</option>
                     @if(!empty($vehicles))
                     @foreach($vehicles as $vehicle)

                     <option value="{{$vehicle->vehicle_id}}" @if($assigned->vehicle_id == $vehicle->vehicle_id){{'selected'}} @endif>{{$vehicle->vehicle_name}}</option>
                     @endforeach
                     @endif
                   </select>
                </div>

                
             </div>

             <div class="col-sm-6">
               <div class="form-group">
                  <label for="driver">Driver</label>
                  <select type="text" class="form-control" name="driver_id" id="driver_id"  required="required">
                     <option value="">Select Vehicle</option>
                     @if(!empty($drivers))
                     @foreach($drivers as $driver)
                     <option value="{{$driver->driver_id}}" @if($assigned->driver_id == $driver->driver_id){{'selected'}} @endif>{{$driver->driver_name}} ({{$driver->driver_state}})</option>
                     @endforeach
                     @endif
                   </select>
               </div>

            </div>
         </div>
         <button type="submit" class="btn btn-primary">Update</button>
      </form>

   </div>
</div>
</div>
<!-- Begin Page Content -->

@endsection