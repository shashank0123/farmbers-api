<?php
use App\User;
?>

@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif

    <a class="btn btn-warning" href="{{url('admin/assign_driver')}}">Assign Driver </a>
    <hr> 
    <table class="table table-bordered">
     <thead>
      <tr class="alert-info">
       <th>SN</th>	
       <th>Vehicle Name</th>
       <th>Vehicle Number</th>
       <th>Driver's Name</th>
       <th>Driver's Contact</th>
       <th>Assigned By</th>
       <th>Action</th>
     </tr>
   </thead>
   <thead>
    @php $i=1; @endphp
    @if(!empty($lists))
    @foreach($lists as $list)
    <?php $user = User::where('id',$list->assigned_by)->first(); ?>
    <tr>
     <td>{{$i++}}</td> 
     <td>@if(!empty($list->vehicle_name)){{ucfirst($list->vehicle_name)}}@endif</td>
     <td>{{$list->vehicle_no}}</td>
     <td>{{ucfirst($list->driver_name)}}</td>
     <td>{{$list->driver_phone}}</td>
     <td>@if(!empty($user)){{ucfirst($user->name)}}@endif</td>
     
     <td><a href="{{url('admin/edit_assigning/'.$list->id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 
      
       <form action="{{url('admin/delete_assinging/'.$list->id)}}" method="post">
        {{csrf_field()}}
        <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$list->id}})"><i class="fa fa-remove"></i></button>
        <input type="submit" hidden="hidden" id="form{{$list->id}}" class="btn btn-danger btn-xs">
      </form>
    </td>
  </tr>
  @endforeach
  @endif
</thead>
</table>	

</div>

</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">
function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }
</script>
@endsection
