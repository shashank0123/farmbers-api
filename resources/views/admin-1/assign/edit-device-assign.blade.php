@extends('admin.layouts.header')
@section('content')
    

<!-- Begin Page Content -->
<div class="container-fluid">
   <!-- Page Heading -->
   <!-- Content Row -->
   <div class="row">
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-12 col-md-12 mb-4 pt-4">
         @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
         <a class="btn btn-warning" href="{{url('admin/assigned_device_list')}}">Back</a> 
         <hr>
         <form action="{{ url('admin/update_device_assigning/'.$assigned->id) }}" method="POST" role="form">
            {{ csrf_field() }}
            <legend>Edit Device Assigned</legend>
            @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert-danger">* {{$error}}</div>
            @endforeach
            @endif
            <div class="row">
               <div class="col-sm-6">
                 <div class="form-group">
                   <label for="driver">Vehicle</label>
                   <select type="text" class="form-control" name="vehicle_id"  id="vehicle_id"  required="required">
                     <option value="">Select Vehicle</option>
                     @if(!empty($vehicles))
                     @foreach($vehicles as $vehicle)

                     <option value="{{$vehicle->vehicle_id}}" @if($assigned->vehicle_id == $vehicle->vehicle_id){{'selected'}} @endif>{{$vehicle->vehicle_name}}</option>
                     @endforeach
                     @endif
                   </select>
                </div>

                
             </div>

             <div class="col-sm-6">
               <div class="form-group">
                  <label for="device_id">Device</label>
                  <select type="text" class="form-control" name="device_id" id="device_id"  required="required">
                     <option value="">Select Device</option>
                     @if(!empty($devices))
                     @foreach($devices as $device)
                     <option value="{{$device->device_id}}" @if($device->device_id == $assigned->device_id){{'selected'}}@endif>{{$device->device_no}} ({{$device->sim_no}})</option> 
                     @endforeach
                     @endif
                   </select>
               </div>

            </div>
         </div>
         <button type="submit" class="btn btn-primary">Update</button>
      </form>

   </div>
</div>
</div>
<!-- Begin Page Content -->

@endsection