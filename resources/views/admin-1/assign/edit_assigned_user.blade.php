@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
   <!-- Page Heading -->
   <!-- Content Row -->
   <div class="row">
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-12 col-md-12 mb-4 pt-4">

         @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
         <a class="btn btn-warning" href="{{url('admin/assigned_users_list')}}">Back</a> 
         <hr>
         <form action="{{ url('admin/update_assigned_user')}}/{{$list->id}}" method="POST" role="form">
            {{ csrf_field() }}

            <legend>Assign Vehicle To User</legend>
            @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert-danger">* {{$error}}</div>
            @endforeach
            @endif
            <div class="row">
               <div class="col-sm-6">
                 <div class="form-group">
                   <label for="driver">Vehicle</label>
                   <select type="text" class="form-control" name="vehicle_id"  id="vehicle_id" required="required">
                     <option value="">Select Vehicle</option>
                     @if(!empty($vehicles))
                     @foreach($vehicles as $vehicle)

                     <option value="{{$vehicle->vehicle_id}}" @if($list->vehicle_id == $vehicle->vehicle_id){{'selected'}}@endif> {{$vehicle->vehicle_name}}</option>
                     @endforeach
                     @endif
                   </select>
                </div>

                
             </div>

             <div class="col-sm-6">
               <div class="form-group">
                  <label for="user_id">User</label>
                  <select type="text" class="form-control" name="user_id" id="user_id"  required="required">
                     <option value="">Select User</option>
                     @if(!empty($users))
                     @foreach($users as $user)
                     <option value="{{$user->id}}" @if($list->user_id == $user->id){{'selected'}}@endif>{{$user->name}}</option>
                     @endforeach
                     @endif
                   </select>
               </div>

            </div>
         </div>
         <button type="submit" class="btn btn-primary">Assign</button>
      </form>

   </div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()