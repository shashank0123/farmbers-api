@extends('admin.layouts.header')
@section('content')
<style>
select option { padding: 10px; cursor: pointer; }
#dynamic_unassigned_vehicles, #dynamic_vehicles { height:280px; }
.fa-long-arrow-left, .fa-long-arrow-right { font-size: 15px; padding-top: 7px }
.card h5 { cursor: pointer;  }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">

   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  
  <div class="alert alert-primary" id="success" style="display: none;">
    <p id="success_result"></p>
  </div>

  <div class="alert alert-primary" id="fail" style="display: none;">
    <p id="fail_result">Something Went Wrong</p>
  </div>
  
  <a class="btn btn-warning" href="{{url('admin/assigned_users_list')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/assigning_vehicle_to_user') }}" method="POST" role="form">
    {{ csrf_field() }}

    <legend>Assign Vehicle To User</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">
      <input type="hidden" id="u_v_id" value="">
      <input type="hidden" id="a_v_id" value="">
      
      <div class="col-sm-6">
       <!-- <div class="form-group">
         <label for="driver">Vehicle</label>
         <select type="text" class="form-control" name="vehicle_id"  id="vehicle_id" required="required">
           <option value="">Select Vehicle</option>
           @if(!empty($vehicles))
           @foreach($vehicles as $vehicle)

           <option value="{{$vehicle->vehicle_id}}">{{$vehicle->vehicle_no}} </option>
           @endforeach
           @endif
         </select>
       </div>
     -->
   </div>

   <div class="col-sm-6">
    <div class="form-group">

      <label for="user_id">User</label>
      <select type="text" class="form-control" name="user_id" id="user_id"  required="required">
       <option value="">Select User</option>
       @if(!empty($users))
       @foreach($users as $user)
       <option value="{{$user->id}}">{{$user->name}}</option>
       @endforeach
       @endif
     </select>
   </div>

 </div>
</div>

<div class="row">
 <div class="col-sm-6">
  <div class="row">
    <div class="col-sm-5">
      <h4 style="margin-top: 10px">All Unassigned Vehicles </h4>
    </div>
    <div class="col-sm-7">
      <input type="search" class="form-control" onkeyup="getUnassigned()" placeholder="Search Unassigned Vehicle" id="search_un"><br>
    </div>
  </div>

  <div class="card">
    <h5 style="text-align: center;" onclick="assignVehicle()"> Click To Assign <i class="fa fa-long-arrow-right"></i>&nbsp;<i class="fa fa-long-arrow-right"></i></h5>
    <div class="card">
     <select size="10" id="dynamic_unassigned_vehicles">
       @if(!empty($vehicles))
       @foreach($vehicles as $vehicle)

       <option onclick="shiftToAssign({{$vehicle->vehicle_id}})">{{$vehicle->vehicle_no}} </option>
       @endforeach
       @endif
     </select>
   </div>
 </div>

</div>
<div class="col-sm-6">

 <div class="row">
    <div class="col-sm-5">
      <h4 style="margin-top: 10px">Assigned Vehicles </h4>
    </div>
    <div class="col-sm-7">
      <input type="search" class="form-control" onkeyup="getAssigned()" placeholder="Search Assigned Vehicle" id="search_as"><br>
    </div>
  </div>

 <div class="card">
  <h5 style="text-align: center;" onclick="unassignVehicle()"><i class="fa fa-long-arrow-left"></i>&nbsp;<i class="fa fa-long-arrow-left"></i> Click To Unassign</h5>
  <div class="card">
   <select id="dynamic_vehicles" size="10">

   </select>
 </div>
</div>

</div>
</div>
<!-- <button type="submit" class="btn btn-primary">Assign</button> -->
</form>

</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')

<script type="text/javascript">

  $('#user_id').on('change',function(){
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var u_id = $('#user_id').val();

    $.ajax({
      type: "POST",
      url: "/admin/dynamic_assigned_vehicles",
      data:{ _token: CSRF_Token, user_id: u_id},
      success:function(msg){
       $('#dynamic_vehicles').html(msg)
     }
   });

  });

  function shiftToUnassign(v_id){
    $('#u_v_id').val(v_id);
  }

  function shiftToAssign(v_id){
    $('#a_v_id').val(v_id);
  }

  function unassignVehicle(){
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var v_id = $('#u_v_id').val();
    var u_id = $('#user_id').val();

    if(u_id == "" || v_id == ""){
      alert('First Select the User & Vehicle Both');
    }
    else{
      $.ajax({
        type: "POST",
        url: "/admin/unassign_vehicle_to_user",
        data:{ _token: CSRF_Token, user_id: u_id, vehicle_id: v_id},
        success:function(msg){
         $('#dynamic_vehicles').html(msg.assigned)
         $('#dynamic_unassigned_vehicles').html(msg.unassigned)

         if(msg.message == 'Data successfully deleted'){
          $('#success_result').text('Vehicle Successfully Unassigned');
          setTimeout(function(){
            document.getElementById("success").style.display = "block"; 
          }, 1000);
        }
        if(msg.message == 'Something went wrong'){
          setTimeout(function(){
            document.getElementById("fail").style.display = "block"; 
          }, 1000);
        }

      }
    });
    }
  }

  function assignVehicle(){
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
    var v_id = $('#a_v_id').val();
    var u_id = $('#user_id').val();


    if(u_id == "" || v_id == ""){
      alert('First Select the User & Vehicle Both');
    }
    else{
      $.ajax({
        type: "POST",
        url: "/admin/assign_vehicle_to_user",
        data:{ _token: CSRF_Token, user_id: u_id, vehicle_id: v_id},
        success:function(msg){
         $('#dynamic_vehicles').html(msg.assigned)
         $('#dynamic_unassigned_vehicles').html(msg.unassigned)
         if(msg.message == 'Data successfully inserted'){
          $('#success_result').text('Vehicle Successfully Assigned To User');
          setTimeout(function(){
            document.getElementById("success").style.display = "block"; 
          }, 1000);
        }
        if(msg.message == 'Something went wrong'){
          setTimeout(function(){
            document.getElementById("fail").style.display = "block"; 
          }, 1000);
        }
      }
    });
    }
  }


  function getUnassigned(){
   var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
   var search = $('#search_un').val();   

   $.ajax({
    type: "POST",
    url: "/admin/search_unassigned",
    data:{ _token: CSRF_Token, search: search},
    success:function(msg){
     $('#dynamic_unassigned_vehicles').html(msg.unassigned)
   }
 });
 }

  function getAssigned(){
   var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
   var search = $('#search_as').val();   
    var u_id = $('#user_id').val();

   $.ajax({
    type: "POST",
    url: "/admin/search_assigned",
    data:{ _token: CSRF_Token, search: search, user_id:u_id},
    success:function(msg){
     $('#dynamic_vehicles').html(msg.assigned)
   }
 });
 }

</script>

@endsection

