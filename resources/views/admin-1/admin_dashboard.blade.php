<?php  
use App\User;
use App\Vehicle;
use App\Device;
use App\Driver;
use App\VehicleToUser;
use App\AssignedVehicle;
use App\DeviceToVehicle;
use App\LocationData;

$stopped_vehicles = 0;
$users = User::where('parent_user','0')->where('user_type','!=','admin')->limit(10)->get();
$total_users = User::where('parent_user','0')->where('user_type','!=','admin')->count();
$drivers = Driver::get();
$total_drivers = count($drivers);
$vehicles = Vehicle::get();
$total_vehicles = count($vehicles);
$devices = DeviceToVehicle::get();
$active_devices = count($devices);
//For Vehicles
$assigned_v = AssignedVehicle::select('vehicle_id')->limit(10)->get();

$acive_web_users = User::where('web_login_status','>','0')->where('user_type','!=','admin')->count();
$acive_app_users = User::where('app_login_status','>','0')->where('user_type','!=','admin')->count();
$assign_v = array();
foreach ($assigned_v as $key) { $assign_v[] = $key->vehicle_id; }

$assigned_vehicles = Vehicle::leftJoin('users','users.id','vehicles.owner_id')->whereIn('vehicles.vehicle_id',$assign_v)->limit(10)->get();

$working = DeviceToVehicle::leftJoin('devices','devices.device_id','device_to_vehicles.device_id')->select('devices.device_no')->limit(10)->get();
$working_vehicle = 0;

if($working){
  foreach ($working as $w) {
    $working_v = LocationData::where('imei','0'.$w->device_no)->orderBy('created_at','DESC')->first();


    if($working_v){ 
      $working_vehicle++;   
      if($working_v->satellites>5 && ($working_v->terminal == 44 || $working_v->terminal == 02)){
        $stopped_vehicles++;
      }    
    }
  }
}

//die();

$not_working_vehicles =  count($devices) - $working_vehicle;


//For Devices

$assigned_devices = Device::distinct()->get(['device_type']);
//$users = User::where('user_type','!=','admin')->get();
?>

@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  bg-gradient-primary">
  @if(isset($data))
  <div class="alert alert-danger center">{{$data}}</div>
  @endif

  <br><br><br><br>
  <div class="row">
    <div class="col-sm-3 center">
      <a href="/admin/user-list/{{'user'}}">
        <div class="card1">
          <img src="/side-icon/user-icon.png" class="img-responsive">
          <h2>Total Main Users <br>
          {{ $total_users ?? '' }}</h2>
        </div>
      </a>
    </div>
    <div class="col-sm-3 center">
      <a href="/admin/show_vehicals">
        <div class="card1">
          <img src="/side-icon/car-icon.png" class="img-responsive">
          <h2>Total Vehicles <br>
          {{$total_vehicles ?? '' }}</h2>
        </div>
      </a>
    </div>
    <div class="col-sm-3 center">
      <a href="/admin/show_driver">
        <div class="card1">
          <img src="/side-icon/driver-icon.png" class="img-responsive">
          <h2>Total Drivers<br>
          {{$total_drivers ?? '' }}</h2>
        </div>
      </a>
    </div>
    <div class="col-sm-3 center">
      <a href="/admin/assigned_device_list">
        <div class="card1">
          <img src="/side-icon/device-icon.png" class="img-responsive">
          <h2>Total Devices<br>
          {{$active_devices}}</h2>
        </div>
      </a>
    </div>


    <div class="col-sm-3 center">
      <a href="/admin/not_working_vehicles">
        <div class="card1">
          <img src="/side-icon/not-working-vehicles.png" class="img-responsive">
          <h2>Not Working Vehicles<br>
          {{$not_working_vehicles}}</h2>
        </div>
      </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/stopped_vehicles">
        <div class="card1">
          <img src="/side-icon/stopped-vehicles.png" class="img-responsive">
          <h2>Stopped Vehicles<br>
          {{$stopped_vehicles}}</h2>
        </div>
      </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/logged_in_users">
        <div class="card1">
          <img src="/side-icon/logged-in-users.png" class="img-responsive">
          <h2>Logged In Users<br>
          {{$acive_web_users}}</h2>
        </div>
      </a>
    </div>

    <div class="col-sm-3 center">
      <a href="/admin/active_app_users">
        <div class="card1">
          <img src="/side-icon/active-users.png" class="img-responsive">
          <h2>Active App Users<br>
          {{$acive_app_users}}</h2>
        </div>
      </a>
    </div>
  </div>
</a>
</div>
</div>

<style>
.table { background-color: #fff !important; height: 350px; overflow-y: auto  }

#table-response { height: 500px !important; background-color: #eee; border-radius: 8px; }

#table-response label {background-color: #006699; color: #fff; width: 100% ; padding: 20px 5px ; text-align: center; border-top-left-radius: 8px; border-top-right-radius: 8px; font-weight: 500; font-size: 18px }
.table{ width: 98%; margin: 0 1% }

.row{  margin-bottom: 0px }

.clickable { cursor: pointer; }
.clickable:hover { color: #006699  }
.form-control { width: 96%;  margin: 0% 2% 2% ;}
#search-sec p { padding-top: 10px }

/*#map-canvas { position: inherit!important; height: 200px !important; }*/
#map-canvas { position: absolute!important;
  overflow: hidden;
  width: 96% !important;
  bottom: 10px;
  height: 55vh ;
}


</style>
<!-- Page Heading -->
<!-- Content Row -->
<div class="row">   
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-8 col-md-12 col-lg-7 col-xs-12 mb-4 pt-4" >
    <div id="table-response">
      <div class="map-responsive">
        <label>Vehicle(s) Details on Map </label>
        <div class="row">
          <div class="col-md-7" id="search-sec">
           <p><b>Current User : </b><span id="current_user">{{Auth::user()->email}}</span></p>
         </div>
         <div class="col-md-5">
          <input type="search" class="form-control" name="vehicle_map_search" value="" id="vehicle_map_search" placeholder="Vehicle No">
        </div>
      </div>       
      <div id="map-canvas" style="position: inherit !important;"></div>
    </div>
  </div>
</div>

<div class="col-xl-4 col-md-12 col-lg-5  col-xs-12 mb-4 pt-4">
  <div id="table-response">
    <table class="table table-bordered table-responsive table-hover">
      <label>User Wise Working Vehicle(s)</label>

      <input type="hidden" name="user_id" id="user_id" value="">
      <div class="row">
        <div class="col-md-6" id="search-sec">                 
        </div>
        <div class="col-md-6">
          <input type="search" class="form-control" name="user_search" id="user_search" placeholder="Search User" onkeyup="getDynamicUser()">
        </div>
      </div>

      <thead>
        <th>S.No.</th>
        <th>User ID</th>
        <th>Working Vehicle/All Vehicles</th>
      </thead>
      <tbody id="user-section">
        @if(!empty($users))
        <?php $i=1; ?>
        @foreach($users as $user)
        <?php 

        $total_vehicles = VehicleToUser::where('user_id',$user->id)->count();
        $working_vehicles = 0;
        if($total_vehicles>0){
          $get_vehicles =  Vehicle::where('owner_id',$user->id)->get();
          foreach($get_vehicles as $getv){
            $color = 'blue';
            //$check_vehicles = array();
            $check_vehicles = DeviceToVehicle::leftJoin('devices','devices.device_id','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id',$getv->vehicle_id)->select('devices.device_no')->first();
            if(!empty($check_vehicles)){
              $data_check = LocationData::where('imei','0'.$check_vehicles->device_no)->orderBy('id','DESC')->first();

              if($data_check){ 

                $startTime = new \DateTime($data_check->location_date." ".$data_check->location_time);
                $endTime = new \DateTime();
                $duration = $startTime->diff($endTime); 
                $diff_in_minutes = $duration->format("%D". " days " ."%H:%I:%S");
                $integer = (int)$duration->format("%I");

                if ($data_check->terminal == 44 || $data_check->terminal == 02){
                  $color = "red";
                }
                else if ($data_check->terminal == 46){
                  $color = "yellow";
                }

                if ($data_check->terminal == 53  || $integer > 05){
                  $color = "blue";
                }
                if ((int)$data_check->speed > 0){
                  $color = "green";
                } 
              }
            }          
            if($color == 'blue'){                  
            }
            else{
              $working_vehicles++;
            }
          }
        }


        ?>
        <tr>
          <td>{{$i++}}</td>
          <td class="clickable" onclick="getUser({{$user->id}})">{{ucfirst($user->name)}}</td>
          <td>{{$working_vehicles}}/{{$total_vehicles}}</td>
        </tr>
        @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>

</div> 

<div class="row">
  <div class="col-xl-8 col-md-12 col-lg-7 col-xs-12 mb-4 pt-4">
    <div id="table-response">
      <table class="table table-bordered table-responsive table-hover">
        <label>Vehicle(s) Details</label>
        <div class="row">
          <div class="col-md-6" id="search-sec">                 
          </div>
          <div class="col-md-6">
            <input type="search" class="form-control" name="vehicle_search" id="vehicle_search" placeholder="Search Vehicle" onkeyup="getDynamicVehicle()">
          </div>
        </div>
        <thead>
          <th>S.No.</th>
          <th>Vehicle No</th>
          <th>Username</th>
          <th>Last Update</th>
          <th>Status</th>
          <th>Speed</th>
          <th>User ID</th>
          <th>Device No</th>
          <th>Sim No</th>
          <th>Activation Date</th>
          <th>Address</th>
          <th>Activation Status</th>
          <th>Power Status</th>
        </thead>

        <tbody id="vehicle-section">
          @if(!empty($assigned_vehicles))
          <?php $i=1; ?>
          @foreach($assigned_vehicles as $vehicle)

          <?php 
          //to get the details of the vehicle
          $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $vehicle->vehicle_id)->first();
          if ($assigned_vehicles1){
            $current_data = LocationData::where('imei', '0'.$assigned_vehicles1->device_no)->orderBy('id', 'DESC')->first();  

            if(isset($current_data) && $current_data->latitude && $current_data->longitude){
              function getaddress($lat,$lng)
              {
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBGb-xEtL1x94c0I_KfULk-FOokCT8ZgPE&latlng='.trim($lat).','.trim($lng).'&sensor=false';
                $json = @file_get_contents($url);
                $data=json_decode($json);
                if(isset($data))
                  $status = $data->status;
                else
                  $status = 'Okay';
                if($status=="OK") return $data->results[0]->formatted_address;
                else
                  return false;
              }
              $lat = $current_data->latitude; $lng = $current_data->longitude;
              $address= getaddress($lat,$lng);
            }

            ?>
            <tr class="new clickable" onclick="setVehicle({{$vehicle->vehicle_id}},'{{$vehicle->vehicle_no}}')">
              <td>{{$i++}}</td>
              <td  class="new clickable" onclick="setVehicle({{$vehicle->vehicle_id}},'{{$vehicle->vehicle_no}}')">{{$vehicle->vehicle_no}}</td>
              <td>{{$vehicle->name}}</td>
              <td>@if (isset($current_data) && $current_data->created_at) {{$current_data->created_at}}@endif</td>
              <td>@if(isset($current_data)  && $current_data->satellites && ($current_data->satellites>5)){{'Connected'}}@else{{'Not Connected'}}@endif</td>
              <td>@if (isset($current_data) && $current_data->speed) {{$current_data->speed}}@endif</td>
              <td>{{$vehicle->user_name}}</td>
              <td>@if(isset($assigned_vehicles1)){{$assigned_vehicles1->device_no}}@endif</td>
              <td>@if(isset($assigned_vehicles1)){{$assigned_vehicles1->sim_no}}@endif</td>
              <td>@if(isset($assigned_vehicles1)){{$assigned_vehicles1->subscription_date}}@endif</td>
              <td>
                <?php
                if($address)
                  echo $address;
                else
                  echo "Not found";
                ?>
              </td>
              <td>@if(isset($assigned_vehicles1) && $assigned_vehicles1->device_status=='active'){{'Active'}}@else{{'Inactive'}}@endif</td>
              <td></td>
            </tr>
            <?php
          }
          ?>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>

  <div class="col-xl-4 col-md-12 col-lg-5 col-xs-12 mb-4 pt-4">
    <div id="table-response">
      <table class="table table-bordered table-responsive table-hover" >
        <label>Device Wise Working Vehicle(s)</label>

        <div class="row">
          <div class="col-md-6" id="search-sec">                 
          </div>
          <div class="col-md-6">
            <input type="search" class="form-control" name="device_search" id="device_search" placeholder="Search Device" onkeyup="getDynamicDevice()">
          </div>
        </div>

        <thead >
          <th>S.No.</th>
          <th>Device Type</th>
          <th>Working Devices(s)/All Devices</th>
        </thead>

        <tbody id="device-section">
          @if(!empty($assigned_devices))
          <?php $i=1; ?>
          @foreach($assigned_devices as $device)
          <?php
          $dev = Device::where('device_type',$device->device_type)->count();
          $working_dev =DeviceToVehicle::leftJoin('devices','devices.device_id','device_to_vehicles.device_id')->where('devices.device_type',$device->device_type)->count();
          ?>
          <tr >
            <th>{{$i++}}</th>
            <th>{{$device->device_type}}</th>
            @if($device->device_type == 'ET300')
            <th><span id="working_et">{{$working_dev}}</span>/<span id="et">{{$dev}}</span></th>
            @else
            <th><span id="working_bw">{{$working_dev}}</span>/<span id="bw">{{$dev}}</span></th>
            @endif
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
</div>


@endsection

@section('script')
<!-- Begin Page Content -->
<script>
  window.setInterval(function(){
    getUser2()
  }, 20000);
  var markers = [];
  var map;
  var icon;
  var position = [28.67176277777778, 76.96169944444445];
  var markers = [];
            function initMap() { // Google Map Initialization...
              var latlng = new google.maps.LatLng(position[0], position[1]);
              map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 16,
                center: latlng,
                mapTypeId: 'terrain'
              });

                icon = { // car icon
                  path: 'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805',
                  scale: 0.7,
                    fillColor: "#427af4", //<-- Car Color, you can change it 
                    fillOpacity: 1,
                    strokeWeight: 1,
                    anchor: new google.maps.Point(0, 5),
                    rotation: 68 //<-- Car angle
                  };

                  marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: icon
                  });
                  markers.push(marker);

                }



                var timer = setInterval( initializemap, 5000);
                initMap()
                function initializemap(){ 
                  var url = '/admin/home/livetracking_json/';
                  $.ajax({
                    type: "GET",
                    url: url,
                    success:function(result){ 
            // console.log(result);    
            if (result["last_location"]){

              var last_location = result['last_location'];
              var lat_and_lon= {"lat":parseFloat(last_location['latitude']),"lng":parseFloat(last_location['longitude']),"terminal":last_location['terminal'],"icon":last_location['type']};
              var username = last_location['imei'];
              if(last_location != null){
                console.log(last_location['type']);
                // console.log(icon);
                icon = last_location['type'];
              // console.log(marker)
              DeleteMarkers() 
                // if (!marker)  {
                  marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: icon
                  });
                  markers.push(marker);
                // }
                transition(lat_and_lon);
              }
            }
            else if (result['all_location']) {
              locations = result['all_location']
                // map.setZoom(7);
                DeleteMarkers()

                for (i = 0; i < locations.length; i++) {  
                  // marker = new google.maps.Marker({
                  //   position: new google.maps.LatLng(locations[i].['latitude'], locations[i].['longitude']),
                  //   map: map
                  // });

                  // google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  //   return function() {
                  //     infowindow.setContent(locations[i]['imei']);
                  //     infowindow.open(map, marker);
                  //   }
                  // })(marker, i));

                  var latlng = new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']);
                  if (locations[i]['type']){
                    // var icon = { // car icon
                    // path: locations[i]['type'],
                    // scale: 0.7,
                    //     fillColor: locations[i]['color'],//"#427af4", //<-- Car Color, you can change it 
                    //     fillOpacity: 1,
                    //     strokeWeight: 1,
                    //     anchor: new google.maps.Point(0, 5),
                    //     rotation: parseInt(terminal) //<-- Car angle
                    // };
                    var icon = locations[i]['type']
                  }
                  else{
                      var icon = { // car icon
                        path: 'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805',
                        scale: 0.7,
                        fillColor: "#427af4",//"#427af4", //<-- Car Color, you can change it 
                        fillOpacity: 1,
                        strokeWeight: 1,
                        anchor: new google.maps.Point(0, 5),
                        rotation: parseInt(terminal) //<-- Car angle
                      };
                    }
                // marker.setPosition(latlng);
                // marker.setTitle('username');
                // marker.setIcon(icon);
                marker = new google.maps.Marker({
                  position: latlng,
                  map: map,
                  icon: icon
                });
                markers.push(marker);
              }

            }
          }
        });
}

var numDeltas = 100;
        var delay = 10; //milliseconds
        var i = 0;
        var deltaLat;
        var deltaLng;
        var terminal;
        function transition(result){
         i = 0;
         deltaLat = (parseFloat(result.lat) - position[0])/numDeltas;
         deltaLng = (parseFloat(result.lng) - position[1])/numDeltas;
         terminal=result.terminal;
         icon=result.icon;
         moveMarker();
       }

       function DeleteMarkers() {
            //Loop through all the markers and remove
            for (var i = 0; i < markers.length; i++) {
              markers[i].setMap(null);
            }
            markers = [];
          };

          function getRotation(previouspos, newposition){
            lat1 = degreesToRadians(previouspos[0])
            long1 = degreesToRadians(previouspos[1])
            lat2 = degreesToRadians(newposition[0])
            long2 = degreesToRadians(newposition[1])

            dLon = (long2 - long1);


            y = Math.sin(dLon) * Math.cos(lat2);
            x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
            * Math.cos(lat2) * Math.cos(dLon);

            radiansBearing = Math.atan2(y, x);


            return radiansToDegrees(radiansBearing);

          }

          function degreesToRadians(degrees)
          {
            var pi = Math.PI;
            return degrees * (pi/180);
          }


          function radiansToDegrees(radians)
          {
            var pi = Math.PI;

            return  radians * 180.0 / Math.PI;
          // return degrees * (pi/180);
        }

        function moveMarker(){
          position[0] += deltaLat;
          position[1] += deltaLng;
          var uluru = { lat: position[0], lng: position[1]};
          var latlng = new google.maps.LatLng(position[0], position[1]);
          marker.setPosition(latlng);
          marker.setTitle('username');
          marker.setIcon(icon);
          markers.push(marker);
          console.log(marker);
            //var center = new google.maps.LatLng(lat, lng);
            map.panTo(latlng);
            if(i!=numDeltas){
              i++;
              setTimeout(moveMarker, delay);
            }
          }
        </script>

        {{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_q4dK7XxTGUh2NziiU749EGP4qUoNKU&callback=initMap"></script> --}}

        {{-- <script type="text/javascript"> 
          $(document).ready(function () {
            var map, location_marker, bounds, infowindow;
    // var current_pin = '/assets/images/t-current-pin.png';
    var current_pin = '/vehicle-icons/car-blue.png';

    initialize();
    loadMap();
  //Update map every 10 sec
  setInterval(function () {
    initialize();
  }, 5000);

  function loadMap()
  {
    var myOptions = {
      scrollwheel: false,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      draggable: true,
      mapTypeId: google.maps.MapTypeId.ROAD
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
    bounds = new google.maps.LatLngBounds();
    infowindow = new google.maps.InfoWindow();

    // var d_latitude = '28.000';
    // var d_longitude = '76.000';
    // curret lcation
    // var username = 'No data';
    // var current_latlng = new google.maps.LatLng(d_latitude, d_longitude);
    // bounds.extend(current_latlng);
    // location_marker = new google.maps.Marker({
    //   position: current_latlng,
    //   map: map,
    //   username: username,
    //   icon: current_pin
    // });

    // google.maps.event.addListener(location_marker, 'click', function() {
    //   var contentString = '<div class="gm-style-iw1">'+
    //   '<div id="siteNotice">'+
    //   '</div>'+'<h1 class="iw-title">Driver Name</h1>'+
    //   '<div id="bodyContent"> '+
    //   '<p><b>&nbsp;&nbsp;</b>'+username+'</p>'+
    //   '</div>'+
    //   '</div>';
    //   infowindow.setContent(contentString);
    //   infowindow.open(map, this);
    // });
    
    // map.fitBounds(bounds);
    map.setZoom(15);
  }

  function initialize()
  { 

    var url = '/admin/home/livetracking_json/';

    $.ajax({
      type: "GET",
      url: url,
      success:function(result)
      {           
        var last_location = result['last_location'];
        var d_latitude = last_location['latitude'];
        var d_longitude = last_location['longitude'];
        var username = last_location['imei'];
        if(last_location != null)
        { 
          var current_latlng = new google.maps.LatLng(d_latitude, d_longitude);
            //var myLatlng = new google.maps.LatLng(last.lat, last.lng);
            marker = new google.maps.Marker({
              position: current_latlng,
              map: map,
              username: username,
              title: username,
              icon: current_pin

            });

            google.maps.event.addListener(location_marker, 'click', function() {
              var contentString = '<div class="gm-style-iw1">'+
              '<div id="siteNotice">'+
              '</div>'+'<h1 class="iw-title">Driver Name</h1>'+
              '<div id="bodyContent"> '+
              '<p><b>&nbsp;&nbsp;</b>'+username+'</p>'+
              '</div>'+
              '</div>';
              infowindow.setContent(contentString);
              infowindow.open(map, this);
            });

            bounds.extend(current_latlng);      

            map.fitBounds(bounds);
            map.setZoom(15);

          }

        }
      });
  }
  google.maps.event.addDomListener(window, 'load', initialize);

});
</script> --}}
<script type="text/javascript">

 function getDynamicUser(){
  var search = $('#user_search').val();
  // alert(search);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-user-search',
   type: 'POST',
   data: {_token: CSRF_TOKEN, search: search },
   success: function (data) {
     $('#user-section').empty();
     $('#user-section').html(data);
   },
   failure: function (data) {
     alert('Something Went Wrong');
   }
 });
}

function getDynamicVehicle(){
  var search = $('#vehicle_search').val();
  var user_id = $('#user_id').val();
  // alert(search);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-vehicle-search',
   type: 'POST',
   data: {_token: CSRF_TOKEN, search: search, user_id: user_id },
   success: function (data) {
     $('#vehicle-section').empty();
     $('#vehicle-section').html(data.message);
     $('#current_user').text(data.user);
   },
   failure: function (data) {
     alert('Something Went Wrong');
   }
 });
}

function getDynamicDevice(){
  var search = $('#device_search').val();
  // alert(search);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-device-search',
   type: 'POST',
   data: {_token: CSRF_TOKEN, search: search },
   success: function (data) {
     $('#device-section').empty();
     $('#device-section').html(data);
   },
   failure: function (data) {
     alert('Something Went Wrong');
   }
 });
}

function getUser(id){
  DeleteMarkers();
  map.setZoom(7);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $('#user_id').val(id);

  $.ajax({
   url: '/admin/dynamic-tracking',
   type: 'POST',
   data: {_token: CSRF_TOKEN, id: id },
   success: function (data) {

     $('#vehicle-section').empty();
     $('#vehicle-section').html(data.message);
     $('#current_user').text(data.user);
     $('#et').text(data.et);
     $('#working_et').text(data.working_et);
     $('#bw').text(data.bw);
     $('#working_bw').text(data.working_bw);
           // setTimeout(function(){getUser(id);},3000);   

         },
         failure: function (data) {
           Swal(data.message);
         }
       });
}

function getUser2(){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/dynamic-tracking2',
   type: 'POST',
   data: {_token: CSRF_TOKEN},
   success: function (data) {

     $('#vehicle-section').empty();
     $('#vehicle-section').html(data.message);
     $('#current_user').text(data.user);
     $('#et').text(data.et);
     $('#working_et').text(data.working_et);
     $('#bw').text(data.bw);
     $('#working_bw').text(data.working_bw);
           // setTimeout(function(){getUser(id);},3000);   

         },
         failure: function (data) {
           Swal(data.message);
         }
       });
}


function setVehicle(id,no){
  DeleteMarkers();
  map.setZoom(16);

  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
   url: '/admin/set_vehicle',
   type: 'POST',
   data: {_token: CSRF_TOKEN, vehicle_id: id },
   success: function (data) {
    // alert(no);
    $('#vehicle_map_search').val(no);
  },
  failure: function (data) {
   Swal(data.message);
 }
});
}
</script>
@endsection
