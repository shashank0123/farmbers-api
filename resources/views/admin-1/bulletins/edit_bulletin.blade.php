@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
 <!-- Page Heading -->
 <!-- Content Row -->
 <div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-12 col-md-12 mb-4 pt-4">
   @if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <a class="btn btn-warning" href="{{url('admin/bulletins-list')}}">Back</a> 
  <hr>
  <form action="{{ url('admin/edit_bulletin/'.$bulletin->id) }}" method="POST" role="form">
    {{ csrf_field() }}
    <legend>Edit Bulletin (Rodeside Assistance</legend>
    @if ($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert-danger">* {{$error}}</div>
    @endforeach
    @endif
    <div class="row">     

      <div class="col-sm-6">

        <div class="form-group">
          <label class="col-sm-12 control-label"><b><br><br>{{ucfirst($bulletin->user_name)}}</b></label>
          
        </div>

        <div class="form-group">
          <label class="col-sm-12 control-label">Message</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="message" required="required">{{$bulletin->message}}</textarea>
          </div>
        </div>

      </div>

    </div>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>

</div>
</div>
</div>
<!-- Begin Page Content -->

@endsection()