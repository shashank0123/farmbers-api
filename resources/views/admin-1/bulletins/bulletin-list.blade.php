@extends('admin.layouts.header')
@section('content')
<style type="text/css">
.sendsms li { display: inline-block; }
</style>

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
    @endif
    
    <div class="row">
      <div class="col-md-6">        
       <a class="btn btn-warning" href="{{url('admin/bulletin')}}">Add Bulletin</a>
     </div>
     <div class="col-md-6">
      <input type="search" class="form-control" name="bulletin_search" id="bulletin_search" placeholder="Search Bulletin" onkeyup="getBulletinSearch()" style="width: 80%">        
    </div>
  </div>
  
  <hr> 
  <table class="table table-bordered">
   <thead>
    <tr class="alert-info">
     <th>SN</th>  
     <th>User ID</th>       
     <th>Message</th>
     <th>Activity</th>
   </tr>
 </thead>
 <thead id="bulletin-section">
  @php $i=1; @endphp
  @foreach($bulletins as $bulletins)
  <tr>
   <td>{{$i++}}</td> 
   <td>@if(!empty($bulletins->user_name)){{$bulletins->user_name}}@endif</td>
   <td>{{$bulletins->message}}</td>     
   
   <td >
    <ul class="sendsms">
      <li>
        <a href="{{url('admin/edit_bulletin/'.$bulletins->id)}}" class="btn btn-success  mb-1 ml-1">Edit</a> 
      </li>
      <li>
        <form action="{{url('admin/bulletins-list/'.$bulletins->id)}}" method="post" >
          {{csrf_field()}}
          <button type="button" class="btn btn-danger btn-xs" onclick="confirmDelete({{$bulletins->id}})"><i class="fa fa-remove"></i></button>
          <input type="submit" hidden="hidden" id="form{{$bulletins->id}}" class="btn btn-danger btn-xs">
        </form>
      </li>
    </ul>      
  </td>
</tr>
@endforeach
</thead>
</table>	

</div>
<div class="row">
 
</div>
</div>
</div>
<!-- Begin Page Content -->


@endsection()

@section('script')
<script type="text/javascript">
  function confirmDelete(id){
    var result = confirm('Do you want to delete ?');       
    if(result){
      $('#form'+id).click();
    }
  }

  function getBulletinSearch(){
    var search = $('#bulletin_search').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/admin/search-bulletin',
     type: 'POST',
     data: {_token: CSRF_TOKEN, search: search},
     success: function (data) {
       $('#bulletin-section').empty();
       $('#bulletin-section').html(data);
     },
     failure: function (data) {
       alert('Something Went Wrong');
     }
   });
  }
</script>
@endsection
