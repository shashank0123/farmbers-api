<?php
use App\User;
?>

@extends('admin.layouts.header')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
  <!-- Page Heading -->
  <!-- Content Row -->
  <div class="row">
   <!-- Earnings (Monthly) Card Example -->
   <div class="col-xl-12 col-md-12 mb-4 pt-4">
    @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
  </div>
  @endif

  <a class="btn btn-warning" href="{{url('admin/bulletins-list')}}">All Bulletins</a>
    <hr> 
<h1>Bulletins</h1>
<br>
  <form action="bulletin" method="POST">                               
    @csrf

    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <label class="col-sm-12 control-label">Username &nbsp; &nbsp; &nbsp; &nbsp;<input type="checkbox" name="all_users" id="all_users" value="all" onclick="selectDisable()">Select All Users</label>

                <div class="col-sm-12">

                    <div id="output"></div>
                    <select data-placeholder="Select Username (UserID) ..." name="users[]" id="users" multiple class="chosen-select form-control" required="required">
                        @if(!empty($users))
                        @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}} ( {{$user->user_name}} ) </option>
                        @endforeach
                        @endif
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 control-label">Message</label>
                <div class="col-sm-12">
                    <textarea class="form-control" name="message" required="required"></textarea>
                </div>
            </div> 
        </div>
        <div class="col-sm-6" style="border-left: 1px solid #ccc">

        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-5">
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>             
        </div>
    </div>
</form>         

</div>

</div>
</div>
<!-- Begin Page Content -->

@endsection()

@section('script')
<script src="/js/chosen1.jquery.js"></script>
<script>

  $(document).ready(function(){
    document.getElementById('output').innerHTML = location.search;
    $(".chosen-select").chosen()   
});

function selectDisable(){
  if (document.getElementById('all_users').checked){
    $('#users').prop('required',false);
  }
  else{
    $('#users').attr('disabled',true);
      }
}

</script>
@endsection()