<!DOCTYPE html>
<html lang="en" class="no-js">
    

<head>
        <meta charset="UTF-8">
        <title>FastTrack</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="FastTrack" />
        <meta name="keywords" content="FastTrack" />
        <meta content="Designer" name="Navin" />
        
        <!-- favicon -->
        <link rel="shortcut icon" href="/img/fast track-final-logo-design.png">

        <!-- css -->
        <link href="/homes/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/homes/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="/homes/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css" />
        <link href="/homes/css/owl.carousel.css" rel="stylesheet" type="text/css" /> 
        <link href="/homes/css/owl.theme.css" rel="stylesheet" type="text/css" />        
        <link href="/homes/css/owl.transitions.css" rel="stylesheet" type="text/css" />
        <link href="/homes/css/magnific-popup.css" rel="stylesheet" type="text/css" />
        <link href="/homes/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/homes/css/about.css" rel="stylesheet" type="text/css" />
        <!-- <link href="/homes/css/sb-admin-2.min.css" rel="stylesheet"> -->
    </head>

    <body @if($message = Session::get('message'))
      onload="showPopUp()" @endif>
        <!--Navbar Start-->
        <nav class="navbar navbar-expand-lg navbar-custom fixed-top sticky">
         <div class="container">
            <!-- LOGO -->
            <a class="navbar-brand logo  main-color " href="{{url('/')}}">
               {{-- Fast
               <sapn class="main-color2">Track</sapn> --}}
               <img width="auto" height="100%" src="{{asset('img/fast track-final-logo-design.png')}}">
            </a>
            <button class="navbar-toggler align-items-sm-right text-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="mdi mdi-menu text-white"></i>
            </button>
            <div class="collapse navbar-collapse text-right" id="navbarCollapse">
               <ul class="navbar-nav ml-auto navbar-center mx-auto align-items-sm-center" id="mySidenav">
                
                         <li class="nav-item ">
                            <a href="#" class="nav-link">Pricing</a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="#" class="nav-link">Demo</a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="{{ route('login') }}" class="nav-link">Sign IN</a>
                        </li>
                        {{--  <li class="nav-item bg-home ">
                            <a href="{{url('register')}}" class="nav-link">SignUp</a>
                        </li> --}}
               </ul>
             <!--   <p class="navbar-nav  navbar-center align-items-sm-right" id="mySidenav"> <img src="  homes/img/menu.png" height="40px" width="40px"></p> -->
            </div>
         </div>
      </nav>
        <!-- Navbar End -->







@yield('content')








  <!--Start Footer-->
        <section class="section bg-home2 footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-12">
                        <div class="footer-logo">
                <a class="navbar-brand logo  main-color text-uppercase" href="{{url('index')}}">Fast<sapn class="main-color2">Track</sapn></a>
                           
                        </div>
                        <div class="footer-icons mt-3 mb-3 pt-3">
                            <a target="_blank"  href="https://www.facebook.com/"><i class="mdi mdi-facebook ml-0"></i></a>
                            <a target="_blank"  href="https://www.twitter.com"><i class="mdi mdi-twitter"></i></a> 
                            <a  target="_blank" href="https://www.gmail.com"><i class="mdi mdi-gmail"></i></a> 
                           {{--  <a href="#"><i class="mdi mdi-google-plus"></i></a>  --}}
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-12 footer-links">
                        <h5>Company</h5>
                        <ul class="list-unstyled">
                            <li><a href="{{url('index')}}">Home</a></li>
                            <li><a href="{{url('about')}}">About Us</a></li>
                            <li><a href="{{url('blog')}}">Blog</a></li>
                            <li><a href="{{url('device')}}">Device</a></li>
                            <li><a href="{{url('contact-us')}}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-12 footer-links">
                        <h5>Resources</h5>
                        <ul class="list-unstyled">
                            <li><a href="#"><img width="150px" height="auto" src="img/playstore.png"></a></li>                            
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-12 footer-links">
                        <h5>Support</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Terms &amp; Condition</a></li>
                            <li><a href="{{url('privacy')}}">Privacy Policy</a></li>
                            <li><a href="{{url('contact-us')}}">Contact Us</a></li>
                            <li><a href="#">Documentation</a></li>
                        </ul>
                    </div> 
                </div>
            </div>
            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#logoutModal" style="display: none;">Click Me</button>
        </section>

        <hr class="m-0">

        <section class="section bg-light py-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 my-1 text-center">
                        <p class="copy-rights text-muted mb-0">2019 © FastTrack. Design by BackStage Supporters</p>
                    </div>
                </div>
            </div>
        </section>
        <!--End Footer-->

        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Not Able To Login</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Your account has been blocked. Contact to Admin.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Ok</button>
          </div>
        </div>
      </div>
    </div>


        <!-- javascript -->
        <script src="/homes/js/jquery.min.js"></script>
        <script src="/homes/js/bootstrap.bundle.min.js"></script>
        <script src="/homes/js/scrollspy.min.js"></script>
        <script src="/homes/js/jquery.easing.min.js"></script>
        <script src="/homes/js/owl.carousel.min.js"></script>
        <script src="/homes/js/isotope.pkgd.min.js"></script>
        <script src="/homes/js/jquery.magnific-popup.min.js"></script>

        <!-- Main Js -->
        <script src="/homes/js/app.js"></script>
        <script src="/homes/js/magnificPopup.init.js"></script>
        <script src="/homes/js/owl.carousel.init.js"></script>

        <script type="text/javascript">
            function showPopUp(){
                $('.dropdown-item').click();
            }
        </script>
    </body>


</html>