<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::group(['middleware' => ['admin'], 'prefix' => 'admin'], function () {

	//For Users
Route::get('users','admin\UserController@index');
Route::get('add_user','admin\UserController@create');
Route::post('add_user','admin\UserController@store');
Route::get('edit_user/{id}','admin\UserController@edit');
Route::post('edit_user/{id}','admin\UserController@update');
Route::get('delete_user/{id}','admin\UserController@destroy');


	//For Crops
Route::get('crops','admin\CropController@index');
Route::get('add_crop','admin\CropController@create');
Route::post('add_crop','admin\CropController@store');
Route::get('edit_crop/{id}','admin\CropController@edit');
Route::post('edit_crop/{id}','admin\CropController@update');
Route::get('delete_crop/{id}','admin\CropController@destroy');


	//For Locations
Route::get('locations','admin\LocationController@index');
Route::get('add_location','admin\LocationController@create');
Route::post('add_location','admin\LocationController@store');
Route::get('edit_location/{id}','admin\LocationController@edit');
Route::post('edit_location/{id}','admin\LocationController@update');
Route::get('delete_location/{id}','admin\LocationController@destroy');

	//For Lands
Route::get('lands','admin\LandController@index');
Route::get('add_land','admin\LandController@create');
Route::post('add_land','admin\LandController@store');
Route::get('edit_land/{id}','admin\LandController@edit');
Route::post('edit_land/{id}','admin\LandController@update');
Route::get('delete_land/{id}','admin\LandController@destroy');

	//For Steps
Route::get('steps/{id}','admin\StepController@index');
Route::get('add_step/{id}','admin\StepController@create');
Route::post('add_step','admin\StepController@store');
Route::get('edit_step/{id}','admin\StepController@edit');
Route::post('edit_step/{id}','admin\StepController@update');
Route::get('delete_step/{id}','admin\StepController@destroy');

	//For Steps
Route::get('changes','admin\ChangeProcedureController@index');
Route::get('add_change','admin\ChangeProcedureController@create');
Route::post('add_change','admin\ChangeProcedureController@store');
Route::get('edit_change/{id}','admin\ChangeProcedureController@edit');
Route::post('edit_change/{id}','admin\ChangeProcedureController@update');
Route::get('delete_change/{id}','admin\ChangeProcedureController@destroy');

Route::post('get_setps','admin\ChangeProcedureController@getSteps');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


