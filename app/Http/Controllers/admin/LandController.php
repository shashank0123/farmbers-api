<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Land;
use App\User;
use App\Location;

class LandController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'land';
        $lands = Land::leftJoin('users','users.id','lands.user_id')->leftJoin('locations','lands.land_location_id','locations.id')->get();
        
        return view('admin.lands.list',compact('lands','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'add_land';
        $users = User::where('user_type','user')->where('account_status','1')->get();
        $locations = Location::all();
        return view('admin.lands.add',compact('page','users','locations'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $newData = new land;

     //    if($request->hasFile('crop_image')) 
     //    {
     //     $file=$request->image_path;
     //     $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
     //     $newData->image_path = time().$file->getClientOriginalName();
     // }

 $newData->land_name = $data['land_name'] ?? '' ;
     $newData->land_location_id = $data['land_location_id'] ?? '' ;
     $newData->user_id = $data['user_id'] ?? '' ;
     $newData->area = $data['area'] ?? '' ;
     $newData->address = $data['address'] ?? '' ;
     $newData->land_latitude = $data['latitude'] ?? '';
     $newData->land_longitude = $data['longitude'] ?? '';
     
     $newData->save();

     return redirect()->back()->with('message','Data successfully added');
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('user_type','user')->where('account_status','1')->get();
        $locations = Location::all();
        $page = 'edit_land';
        $land = Land::find($id);
        return view('admin.lands.edit',compact('land','page','users','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $newData = Land::find($id);
     //   if($request->hasFile('crop_image')) 
     //    {
     //     $file=$request->image_path;
     //     $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
     //     $newData->image_path = time().$file->getClientOriginalName();
     // }

   $newData->land_name = $data['land_name'] ?? '' ;
     $newData->land_location_id = $data['land_location_id'] ?? '' ;
     $newData->user_id = $data['user_id'] ?? '' ;
     $newData->area = $data['area'] ?? '' ;
     $newData->address = $data['address'] ?? '' ;
     $newData->land_latitude = $data['latitude'] ?? '';
     $newData->land_longitude = $data['longitude'] ?? '';
     $newData->update();


     return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $land = Land::where('id',$id)->delete();
     return redirect()->back()->with('message','Data successfully deleted');
    }
}
