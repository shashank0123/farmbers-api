<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;

class LocationController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'location';
        $locations = Location::all();
        
        return view('admin.locations.list',compact('locations','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'add_location';
        return view('admin.locations.add',compact('page'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $newData = new location;

     //    if($request->hasFile('crop_image')) 
     //    {
     //     $file=$request->image_path;
     //     $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
     //     $newData->image_path = time().$file->getClientOriginalName();
     // }

 $newData->location_name = $data['location_name'];
     $newData->parent_location = $data['parent_location'];
     $newData->pincode = $data['pincode'];
     $newData->latitude = $data['latitude'];
     $newData->longitude = $data['longitude'];
     
     $newData->save();

     return redirect()->back()->with('message','Data successfully added');
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'edit_location';
        $location = Location::find($id);
        return view('admin.locations.edit',compact('location','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $newData = Location::find($id);
     //   if($request->hasFile('crop_image')) 
     //    {
     //     $file=$request->image_path;
     //     $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
     //     $newData->image_path = time().$file->getClientOriginalName();
     // }

     $newData->location_name = $data['location_name'];
     $newData->parent_location = $data['parent_location'];
     $newData->pincode = $data['pincode'];
     $newData->latitude = $data['latitude'];
     $newData->longitude = $data['longitude'];

     $newData->update();


     return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::where('id',$id)->delete();
     return redirect()->back()->with('message','Data successfully deleted');
    }
}
