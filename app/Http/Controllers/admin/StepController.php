<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crop;
use App\CropStep;

class StepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
    	$page = 'crop';
    	$crop = Crop::find($id);
    	$steps = CropStep::where('crop_id',$id)->orderBy('step_number','ASC')->get();

    	return view('admin.steps.list',compact('steps','page','crop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
    	$crop = Crop::find($id);
    	$page = 'add_crop';
    	return view('admin.steps.add',compact('page','crop'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$data = $request->all();

    	$newData = new CropStep;

    	if($request->hasFile('step_image')) 
    	{
    		$file=$request->step_image;
    		$file->move(public_path(). '/steps_images/', time().$file->getClientOriginalName());   
    		$newData->step_image = time().$file->getClientOriginalName();
    	}

    	$newData->crop_id = $data['crop_id'];
    	$newData->step_number = $data['step_number'];
    	$newData->no_of_days = $data['no_of_days'];
    	$newData->description = $data['description'];

    	$newData->save();

    	return redirect()->back()->with('message','Data successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$page = 'edit_crop';
    	$step = CropStep::find($id);
    	return view('admin.steps.edit',compact('step','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	$data = $request->all();
    	$newData = CropStep::find($data['step_id']);
    	if($request->hasFile('step_image')) 
    	{
    		$file=$request->step_image;
    		$file->move(public_path(). '/steps_images/', time().$file->getClientOriginalName());   
    		$newData->step_image = time().$file->getClientOriginalName();
    	}

    	$newData->step_number = $data['step_number'];
    	$newData->no_of_days = $data['no_of_days'];
    	$newData->description = $data['description'];

    	$newData->update();

    	return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$step = CropStep::where('id',$id)->delete();
    	return redirect()->back()->with('message','Data successfully deleted');
    }
}
