<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Profile;
use Hash;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'user';
        $users = User::all();
        
        return view('admin.users.list',compact('users','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'add_user';
        return view('admin.users.add',compact('page'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $newData = new User;

        $newData->name = $data['name'];
        $newData->email = $data['email'];
        $newData->mobile = $data['mobile'] ?? '';
        $newData->password = Hash::make($data['password']);
        $newData->account_status = $data['account_status'] ?? '';
        $newData->user_type = 'user';

        $newData->save();


        $user = User::where('email',$data['email'])->first();

        $newProfile = new Profile;

        if($request->hasFile('profile_pic')) 
        {
           $file=$request->profile_pic;
           $file->move(public_path(). '/profiles/', time().$file->getClientOriginalName());   
           $newProfile->profile_pic = time().$file->getClientOriginalName();
       }

       $newProfile->user_id = $user->id;
       $newProfile->profession = $data['profession'] ?? '';
       $newProfile->nickname = $data['nickname'] ?? '';

       $newProfile->save();


       return redirect()->back()->with('message','Data successfully added');
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'edit_user';
        $user = User::LeftJoin('profiles','profiles.user_id','users.id')->select('users.*','profiles.profession','profiles.nickname','profiles.profile_pic')->where('users.id',$id)->first();

        // print_r($user);
        // die;
        return view('admin.users.edit',compact('user','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $data = $request->all();

        $newData = User::find($id);
        

        $newData->name = $data['name'];
        $newData->email = $data['email'];

        if(isset($data['password'])){
           $newData->password = Hash::make($data['password']);        
       }

       $newData->mobile = $data['mobile'] ?? '';
       $newData->account_status = $data['account_status'];
       $newData->update();

       $newProfile = Profile::where('user_id',$id)->first();

       if(isset($newProfile))
       {
          if($request->hasFile('profile_pic')) 
          {
           $file=$request->profile_pic;
           $file->move(public_path(). '/profiles/', time().$file->getClientOriginalName());  
           $newProfile->profile_pic = time().$file->getClientOriginalName();
       }

       $newProfile->profession = $data['profession'] ?? '';
       $newProfile->nickname = $data['nickname'] ?? '';

       $newProfile->update();
   }
   else{
    $newProfile = new Profile;
    if($request->hasFile('profile_pic')) 
    {
       $file=$request->profile_pic;
       $file->move(public_path(). '/profiles/', time().$file->getClientOriginalName());  
       $newProfile->profile_pic = time().$file->getClientOriginalName();
   }

   $newProfile->user_id = $id;
   $newProfile->profession = $data['profession'] ?? '';
   $newProfile->nickname = $data['nickname'] ?? '';
   $newProfile->save();
}

return redirect()->back()->with('message','Data successfully updated');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id',$id)->delete();
        return redirect()->back()->with('message','Data successfully deleted');
    }
}
