<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crop;
use App\CropStep;
use App\Change;

class ChangeProcedureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'change';
        $changes = Change::leftJoin('crops','crops.id','changes.crop_id')->leftJoin('crop_steps','crop_steps.id','changes.step_id')->whereNotNull('crops.id')->whereNotNull('crop_steps.id')->select('changes.*','crop_steps.description','crops.crop_name')->get();
        
        return view('admin.changes.list',compact('changes','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'add_change';
        $crops = Crop::all();
        return view('admin.changes.add',compact('page','crops'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $newData = new Change;

     //    if($request->hasFile('crop_image')) 
     //    {
     //     $file=$request->image_path;
     //     $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
     //     $newData->image_path = time().$file->getClientOriginalName();
     // }

 $newData->crop_id = $data['crop_id'];
     $newData->step_id = $data['step_id'];
     $newData->no_of_days = $data['no_of_days'];
     $newData->reason_of_change = $data['reason_of_change'];
     $newData->change_type = $data['change_type'];
     $newData->value_after_change = $data['value_after_change'];
     $newData->changed_by = \Auth::user()->id;
     
     $newData->save();

     return redirect()->back()->with('message','Data successfully added');
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'edit_location';
        $change = Change::find($id);
        
        $crops = Crop::all();
        $steps = CropStep::where('crop_id',$change->crop_id)->orderBy('step_number','ASC')->get();
        return view('admin.changes.edit',compact('change','page','crops','steps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $newData = Change::find($id);
     //   if($request->hasFile('crop_image')) 
     //    {
     //     $file=$request->image_path;
     //     $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
     //     $newData->image_path = time().$file->getClientOriginalName();
     // }

     $newData->crop_id = $data['crop_id'];
     $newData->step_id = $data['step_id'];
     $newData->no_of_days = $data['no_of_days'];
     $newData->reason_of_change = $data['reason_of_change'];
     $newData->change_type = $data['change_type'];
     $newData->value_after_change = $data['value_after_change'];
     $newData->changed_by = \Auth::user()->id;
     $newData->update();


     return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $change = Change::where('id',$id)->delete();
     return redirect()->back()->with('message','Data successfully deleted');
    }


    public function getSteps(Request $request){
    	$data = $request->all();

    	$steps = CropStep::where('crop_id',$data['crop_id'])->get();

    	if(isset($steps)){
    		foreach($steps as $step){
    			echo "<option value='".$step->id."'>".$step->step_number.". ".substr($step->description,0,50)."</option>";
    		}
    	}
    }

}
