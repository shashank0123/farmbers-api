<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vehicle;
use App\Complaint;
use App\Driver;
use App\Brand;
use App\User;
use App\Device;
use App\Profile;
use App\VehicleToUser;
use App\DeviceToVehicle;
use App\CarActivity;
use App\AssignedVehicle;
use App\LocationData;
use App\Rsa;
use App\Bulletin;
use Validator;
use StdClass;
use Hash;

class AdminController extends Controller
{
    //
	public function admin_dashboard(Request $request){

		$vehicles = Vehicle::limit(2)->get();
		$drivers = Driver::limit(2)->get();
		$vehicle_drivers = AssignedVehicle::leftjoin('vehicles','vehicles.vehicle_id','assigned_vehicles.vehicle_id')->leftjoin('drivers','drivers.driver_id','assigned_vehicles.driver_id')->limit(2)->get();
		$data = $this->livetracking_page($request);

		return view("admin.admin_dashboard",compact('vehicles','drivers','vehicle_drivers','data'));
	}

	public function set_vehicle(Request $request)  
	{
		$vehicle_id = $request->vehicle_id;
		$assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $vehicle_id)->first();
		if ($assigned_vehicles1){
			session()->put('owner_id', "");
			session()->put('vehicle_id', $assigned_vehicles1->device_no);
		}
		return 1;
	}

    function track_live($device_imei)
    {
        $vehicle_type = 'car';
        $device_no = ltrim($device_imei, $device_imei[0]); 
        // v.registration_number, v.vehicle_type, v.variant, vm.name as make, vmo.name as model, vmo.image, CONCAT(u.fname," ",u.lname) as username
        //  dd($device_imei);

        $locations = LocationData::where('imei', $device_imei)
        ->orderBy('created_at', 'DESC')
        ->first();
        if (!$locations){
            $locations = new StdClass;
        }

        $device = Device::where('device_no', $device_no)->first();
        if ($device){
            $assigned_vehicles1 = DeviceToVehicle::leftJoin('vehicles','vehicles.vehicle_id','=','device_to_vehicles.vehicle_id')->where('device_to_vehicles.device_id', $device->device_id)->first();

            if ($assigned_vehicles1){

                if($assigned_vehicles1->vehicle_type=='Motor Car')
                    $vehicle_type='car';
                else if($assigned_vehicles1->vehicle_type=='Bike')
                    $vehicle_type='bike';
                else if($assigned_vehicles1->vehicle_type=='Taxi')
                    $vehicle_type='taxi';
                else if($assigned_vehicles1->vehicle_type=='Crane')
                    $vehicle_type='crane';
                else if($assigned_vehicles1->vehicle_type=='JCB')
                    $vehicle_type='jcb';
                else if($assigned_vehicles1->vehicle_type=='Tractor' || $assigned_vehicles1->vehicle_type=='Personal Tractor')
                    $vehicle_type='tractor';
                else if($assigned_vehicles1->vehicle_type=='Ambulance')
                    $vehicle_type='ambulance';
                else if($assigned_vehicles1->vehicle_type=='Chhota Hathi')
                    $vehicle_type='chota-hathi';
                else if($assigned_vehicles1->vehicle_type=='Scooter')
                    $vehicle_type='scooty';
                else if($assigned_vehicles1->vehicle_type=='Truck')
                    $vehicle_type='truck';
                else if($assigned_vehicles1->vehicle_type=='Bus')
                    $vehicle_type='bus';
                else
                    $vehicle_type = 'car';

                $locations->type = "/assets/images/vehicles/".$vehicle_type."-blue.png";
            }
            else
                $locations->type = "/assets/images/vehicles/".$vehicle_type."-blue.png";
        }
        else
            $locations->type = "/assets/images/vehicles/".$vehicle_type."-blue.png";

        if ($locations){
            $locations->color = "blue";

            if (!isset($locations->terminal)){
                $locations->terminal = 53;
            }
            if (!isset($locations->speed)){
                $locations->speed = 0;
            }
            if (isset($locations->location_date)){
                $startTime = new \DateTime($locations->location_date." ".$locations->location_time);           
                $endTime = new \DateTime();
                $duration = $startTime->diff($endTime); //$duration is a DateInterval object
                $diff_in_minutes = $duration->format("%D". " days " ."%H:%I:%S");
                $integer = (int)$duration->format("%I"); 
            }
            else
                $integer = 1000;
            if ($locations->terminal == 44 || $locations->terminal == 02 || $locations->terminal == 68|| $locations->terminal == 20){

                $locations->color = "red";
                $locations->type = "/assets/images/vehicles/".$vehicle_type."-red.png";

            }else if ($locations->speed>0){
                $locations->color = "green";
                $locations->type = "/assets/images/vehicles/".$vehicle_type."-green.png";

            }
            else if ($locations->terminal == 46 || $locations->terminal == 70){
                $locations->color = "yellow";
                $locations->type = "/assets/images/vehicles/".$vehicle_type."-yellow.png";
            }

            if ((int)$locations->speed > 0){
                $locations->color = "green";
                $locations->type = "/assets/images/vehicles/".$vehicle_type."-green.png";


            }
            if ($locations->terminal == 53 || $integer > 05){


                $locations->color = "blue";
                $locations->type = "/assets/images/vehicles/".$vehicle_type."-blue.png";
            }
        }

        if($locations)
        {            
        	return $locations;
        }
        else
        {
        	return array();
        }
    }

    public function livetracking_page(Request $request){        

    	if (isset($request->device_imei)){        
    		$device_imei = $request->device_imei;
    	}
    	else {
    		$device_imei = session()->get('vehicle_id');
    	}

    	if(empty($device_imei))
    	{
    		$message = "Sorry! Track Trip Details Not Found!";
    		return $message;
    	}

    	/*get order details with live tracking*/
    	$data['last_location'] = $this->track_live($device_imei);
        //echo "<pre>";print_r($data);die;
    	if(empty($data['last_location']))
    	{
            //echo "Sorry! This ride was completed";die();
    		return view("admin.trip_completed",compact('data'));
    	}

    	$timezone = 'Asia/Calcutta';
        // if(!empty($this->session->userdata('tracking_timezone')))
        //     $timezone = $this->session->userdata('tracking_timezone');
    	$trip_datetime = '';
    	if(!empty($data['last_location']))
    		$trip_datetime = $data['last_location']['location_date'].' '.$data['last_location']['location_time'];
    	$data['last_location']['trip_datetime'] = $this->date_convert($trip_datetime, $timezone, 'Y-m-d h:i:s');


    	return view("admin.livetracking",compact('data'));
    }

    function date_convert($date, $timezone, $dateformat) {
    	if($date == '0000-00-00 00:00:00')
    		return $date;
    	$date = new \DateTime($date);
    	$date->setTimezone(new \DateTimeZone($timezone));

    	return $date->format($dateformat);
    }
    
    /*
    * Get Live tracking of the ride json
    */
    function livetracking_json($device_imei = '')
    {
        $vehicle_type = 'car';
        $location = "";
        if (!isset($request->device_imei)){        
          $device_imei = '0'.session()->get('vehicle_id');
      }
      if (session()->get('owner_id') != null){
          $owner_id = session()->get('owner_id');
          $vehicles = VehicleToUser::leftJoin('vehicles', 'vehicles.vehicle_id', '=', 'vehicle_to_users.vehicle_id')->where('vehicle_to_users.user_id', $owner_id)->orderBy('vehicle_to_users.vehicle_id','ASC')->get();
          $locations = array();
          foreach ($vehicles as $key => $value) {

           $vehicle_id = $value->vehicle_id;
           $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $vehicle_id)->first();
                // dd($assigned_vehicles1);
           if($assigned_vehicles1)
               $location = $this->track_live("0".$assigned_vehicles1->device_no);

           if ($location)	{


               if($value->vehicle_type=='Motor Car')
                $vehicle_type='car';
            else if($value->vehicle_type=='Bike')
                $vehicle_type='bike';
            else if($value->vehicle_type=='Taxi')
                $vehicle_type='taxi';
            else if($value->vehicle_type=='Crane')
                $vehicle_type='crane';
            else if($value->vehicle_type=='JCB')
                $vehicle_type='jcb';
            else if($value->vehicle_type=='Tractor' || $value->vehicle_type=='Personal Tractor')
                $vehicle_type='tractor';
            else if($value->vehicle_type=='Ambulance')
                $vehicle_type='ambulance';
            else if($value->vehicle_type=='Chhota Hathi')
                $vehicle_type='chota-hathi';
            else if($value->vehicle_type=='Scooter')
                $vehicle_type='scooty';
            else if($value->vehicle_type=='Truck')
                $vehicle_type='truck';
            else if($value->vehicle_type=='Bus')
                $vehicle_type='bus';
            else
                $vehicle_type = 'car';


            $location->image_type = "/assets/images/vehicles/".$vehicle_type."-blue.png";

            if ($value->image_type == "Motor Car")
             $location->type = "m -682.872,-273.554 c 0,0.461 -0.379,0.835 -0.847,0.835 h -17.251 c -0.468,0 -0.847,-0.374 -0.847,-0.835 v -5.747 c 0,-0.461 0.379,-0.836 0.847,-0.836 h 17.251 c 0.468,0 0.847,0.375 0.847,0.836 z";
         else
             $location->image_type = "M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805";
     }
     array_push($locations, $location);
 }
 $data['all_location'] = $locations;
 return response()->json($data);      

}
if(empty($device_imei))
{
  echo "Sorry! Track Trip Details Not Found!";die;
}

/*get trip details with live tracking*/
$data['last_location'] = $this->track_live($device_imei);


if(empty($data['last_location']))
{
  echo "Sorry! This track trip was completed";die;
}
return response()->json($data);
}


    /*
     * set timezone for browser
    */
    function set_timezone()
    {
        // if($this->session->userdata('tracking_timezone') == ''){
        //     // $this->session->set_userdata('tracking_timezone',$this->input->get('timezone'));  
        //     echo "success";  
        // }
        // else{
    	echo "already set";
        // }
    }
    public function user_dashboard(){
    	return view("admin.user_dashboard");
    }

    public function getProfile(){
    	$user = \Auth::user();
    	$profile = Profile::where('user_id',$user->id)->first();
    	return view("admin.settings.profile",compact('user','profile'));
    }  
    public function getComplaint(){
    	return view("admin.complaint");
    }
    public function getHistory(){
    	return view("admin.history");
    }
    public function getDistance(){
    	$type="distance";
    	$vehicles = Vehicle::all();
    	return view("admin.distance", compact('vehicles','type'));
    } 

    public function getTrip(){
    	$type="trip";
    	$vehicles = Vehicle::all();
    	return view("admin.distance", compact('vehicles','type'));
    } 

    public function getSettings(){
    	return view("admin.settings.settings");
    } 

    public function getNotification(){
    	return view("admin.notifications.notification");
    }

    public function getActivity(){
    	$activities = CarActivity::leftJoin('vehicles', 'vehicles.vehicle_id', '=', 'car_activities.vehicle_id')->leftJoin('users', 'users.id', '=', 'car_activities.user_id')->get();        
    	return view("admin.activity.activities",compact('activities'));
    }

    public function searchActivity(Request $request){

    	$data = $request->all();
    	$from = $request->from_date;
    	$to = $request->to_date;

    	$activities = CarActivity::leftJoin('vehicles', 'vehicles.vehicle_id', '=', 'car_activities.vehicle_id')->leftJoin('users', 'users.id', '=', 'car_activities.user_id')->where('date','>=',$from)->where('date','<=',$to)->get();

    	return view("admin.activity.searched-activities",compact('activities','from','to'));
    }

    public function postComplaint(Request $request){
    	$data = $request->all();

    	$complaint = new Complaint;
    	$complaint->user_id = '1';
    	$complaint->name = $data['name'];
    	$complaint->vehicle_no = $data['vehicle_no'];
    	$complaint->mobile = $data['mobile'];
    	$complaint->complaint = $data['complaint'];

    	$complaint->save();

    	return redirect()->back()->with('message','Compaint registered successfully.');
    }

    public function showComplaints(){
    	$complaints = Complaint::orderBy('created_at','DESC')->get();
    	return view('admin.all-complaints',compact('complaints'));
    }

    public function deleteComplaint($id){
        Complaint::where('id',$id)->delete();
        return redirect()->back()->with('message','Complaint deleted successfully.');
    }

    public function searchComplaint(Request $request){
        $data = $request->all();

        $complaints = Complaint::where('name','LIKE','%'.$data['search'].'%')->get();

        if($complaints){
            $i=1;
            foreach ($complaints as $complaint) {
                $action = " "; $reply = " ";
                if($complaint->status == 'Open'){
                    $action = "<a href='/admin/complaint-open/".$complaint->id."' class='btn btn-primary'>Close</a>";
                }
                else{
                   $action = "<a href='/admin/complaint-close/".$complaint->id."' class='btn btn-success'>Open</a>";
               }
               if($complaint->reply){
                $reply = 'Replied';
            }
            else{
                $reply = 'Reply';
            }
            echo "<tr>
            <td>".$i++."</td> 
            <td>".$complaint->name."</td>
            <td>".$complaint->mobile."</td>
            <td>".$complaint->vehicle_no."</td>
            <td>".$complaint->complaint."</td>
            <td>".$complaint->status."</td>
            <td>".$action."
            <a href='/admin/reply/".$complaint->id."' class='btn btn-secondary btn-xs'>".$reply."</a>       

            <form action='/admin/complaint/".$complaint->id."' method='post' >".csrf_field()."
            <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$complaint->id.")'><i class='fa fa-remove'></i></button>
            <input type='submit' hidden='hidden' id='form".$complaint->id."' class='btn btn-danger btn-xs'>
            </form>

            </td>
            </tr>";
        }
    }
}


    // Dynamic user Search
public function getDynamicUser(Request $request){
   $search = $request->search;

   $users = User::where('name','LIKE','%'.$search.'%')->get();

   if(!empty($users)){
      $i = 1;
      foreach($users as $user){
         $total_vehicles = Vehicle::where('owner_id',$user->id)->count();
         echo "<tr> <td>".$i++."</td><td class='clickable' onclick='getUser(".$user->id.")'>".$user->name."</td><td>0/".$total_vehicles."</td> </tr>";
     }
 }
}

    // Dynamic Vehicle Search

public function getDynamicVehicle(Request $request){
   $data = new StdClass;
   $message = "";
   $search = $request->search;
   $user_id = $request->user_id;

   $user_details = User::where('id', $user_id)->first();    
   $user = strtoupper($user_details->user_name);

   $i=1;
   $vehicles = VehicleToUser::leftjoin('vehicles','vehicles.vehicle_id','vehicle_to_users.vehicle_id')->where('vehicle_to_users.user_id',$user_id)->where('vehicle_no','LIKE','%'.$search.'%')->get();

   if($vehicles){
      foreach($vehicles as $vehicle){
         $last_updated=""; $status="Not Connected"; $speed=""; $device_no=""; $sim_no="";$subscription_date=""; $address="Not Found"; $activation_status='Inactive';$color="blue";$power_status='Not Connected';
         if(isset($vehicle->vehicle_no)){
            $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $vehicle->vehicle_id)->first();
            if ($assigned_vehicles1){
               $current_data = LocationData::where('imei', '0'.$assigned_vehicles1->device_no)->orderBy('created_at', 'DESC')->first();  

               if($assigned_vehicles1->device_status=='active')
                  $activation_status = 'Active';

              if(isset($current_data) && $current_data->created_at)
                  $last_updated = $current_data->created_at;


              if(isset($current_data) && $current_data->speed)
                  $speed = $current_data->speed;


              if(isset($current_data)){

                  if ($current_data->terminal == 44 || $current_data->terminal == 02 || $current_data->terminal == 68 || $current_data->terminal == 20){
                     $color = "red";

                 }
                 else if ($current_data->terminal == 46 || $current_data->terminal == 70){
                     $color = "yellow";
                 }

                 if ($current_data->terminal == 53){
                     $color = "blue";
                 }

                 if ($current_data->terminal == 68 || $current_data->terminal == 196){
                     $power_status = "Disconnected";
                 }
                 else{
                     $power_status = 'Connected';
                 }
                 if ((int)$current_data->speed > 0){
                     $color = "green";
                 }
             }

             if($color == 'blue'){
              $status = 'Not Connected';
          }
          else{
              $status = 'Connected';
          }

          if(isset($assigned_vehicles1))
              $device_no = $assigned_vehicles1->device_no;

          if(isset($assigned_vehicles1))
              $sim_no = $assigned_vehicles1->sim_no;

          if(isset($assigned_vehicles1))
              $subscription_date = $assigned_vehicles1->subscription_date;

          if(isset($current_data) && $current_data->latitude && $current_data->longitude){

              $lat = $current_data->latitude; $lng = $current_data->longitude;

              $address= $this->getaddress($lat,$lng);
              if(empty($address))
                 $address="Not Found";               
         }
     }

     $message = $message."<tr class='new clickable' onclick='setVehicle($vehicle->vehicle_id),($vehicle->vehicle_no)'> 
     <td>".$i++."</td>
     <td class='new clickable' onclick='setVehicle($vehicle->vehicle_id)' style='color: ".$color."; font-weight: bold;'>".$vehicle->vehicle_no."</td>
     <td>".$user_details->name."</td>
     <td>".$last_updated."</td>
     <td>".$status."</td>
     <td>".$speed."</td>
     <td>".$user_details->user_name."</td>
     <td>".$device_no."</td>
     <td>".$sim_no."</td>
     <td>".$subscription_date."</td>
     <td>".$address."</td>
     <td>".$activation_status."</td>
     <td>".$power_status."</td>
     </tr>";
 }
}

}
$data->message = $message;
$data->power_status = $power_status;
$data->user = $user;

return response()->json($data);
}


    // Dynamic Device Search
public function getDynamicDevice(Request $request){
 $search = $request->search;

 $devices = Device::where('device_type','LIKE','%'.$search.'%')->distinct()->get(['device_type']);

 if(!empty($devices)){
  $i = 1;
  foreach($devices as $device){
   $dev = Device::where('device_type',$device->device_type)->count();
   echo "<tr> <td>".$i++."</td><td>".$device->device_type."</td><td>0/".$dev."</td> </tr>";
}
}
}

    // Dynamic Devices Search
public function getDynamicDevices(Request $request){
 $search = $request->search;

 $devices = Device::where('device_no','LIKE','%'.$search.'%')->orWhere('sim_no','LIKE','%'.$search.'%')->get();

 if(!empty($devices)){
  $i = 1;
  foreach($devices as $device){
    if ($device->device_status == 'barred'){
       echo "<tr> <td>".$i++."</td><td>".$device->device_no."</td><td>".$device->sim_no."</td><td>".$device->sim_type."</td><td>".$device->device_type."</td><td>".$device->subscription_date."</td><td>".explode(' ',$device->created_at)[0]."</td><td>


       <a href='/admin/unbar_device/".$device->device_id."' class='btn btn-success  mb-1 ml-1'>Unbar</a> 

       <a href='/admin/edit_device/".$device->device_id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

       <form action='/admin/delete_device/".$device->device_id."' method='post'>".csrf_field()."

       <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$device->device_id.")'><i class='fa fa-remove'></i></button>
       <input type='submit' hidden='hidden' id='form".$device->device_id."' class='btn btn-danger btn-xs'>
       </form>
       </td> </tr>";
   }
   else{
       echo "<tr> <td>".$i++."</td><td>".$device->device_no."</td><td>".$device->sim_no."</td><td>".$device->sim_type."</td><td>".$device->device_type."</td><td>".$device->subscription_date."</td><td>".explode(' ',$device->created_at)[0]."</td><td>


       <a href='/admin/bar_device/".$device->device_id."' class='btn btn-success  mb-1 ml-1'>Bar</a>

       <a href='/admin/edit_device/".$device->device_id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

       <form action='/admin/delete_device/".$device->device_id."' method='post'>".csrf_field()."

       <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$device->device_id.")'><i class='fa fa-remove'></i></button>
       <input type='submit' hidden='hidden' id='form".$device->device_id."' class='btn btn-danger btn-xs'>
       </form>
       </td> </tr>";
   }

}
}
}


    // Dynamic Vehicle Search
public function getSearchedVehicles(Request $request){
 $search = $request->search;
 $user_name = "";
 $vehicle_no = "";
 $device_no = "";
 $sim_no = "";
 $sim_type = "";
 $sub_date = "";
 $device_status = "";
 $price = "";
 $days = "0";
 $device_type = "";

 $vehicles = Vehicle::leftJoin('device_to_vehicles','device_to_vehicles.vehicle_id','vehicles.vehicle_id')
 ->leftjoin('devices','devices.device_id','device_to_vehicles.device_id')
 ->leftjoin('vehicle_to_users','vehicle_to_users.vehicle_id','vehicles.vehicle_id')
 ->leftjoin('users','users.id','vehicle_to_users.user_id')
 ->where('vehicles.vehicle_no','LIKE','%'.$search.'%')
 ->orWhere('vehicles.vehicle_name','LIKE','%'.$search.'%')
 ->orWhere('vehicles.vehicle_type','LIKE','%'.$search.'%')
 ->orWhere('users.user_name','LIKE','%'.$search.'%')
 ->orderBy('vehicles.created_at','DESC')
 ->get();

 if(!empty($vehicles)){
  $i = 1;
  foreach($vehicles as $v){
    $count_user = VehicleToUser::where('user_id',$v->id)->count();

    $last_updated="";$activation_status='Inactive';$color="blue";$power_status='Not Connected';
          //to get the details of the vehicle
    $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $v->vehicle_id)->first();

    if ($assigned_vehicles1){
      $current_data = LocationData::where('imei', '0'.$assigned_vehicles1->device_no)->orderBy('created_at', 'DESC')->first();  

      if($assigned_vehicles1->device_status=='active')
        $activation_status = 'Active';

    if(isset($current_data) && $current_data->created_at)
        $last_updated = $current_data->created_at;

    if(isset($current_data) && $current_data->speed)
        $speed = $current_data->speed;

    if(isset($current_data)){

        if ($current_data->terminal == 44 || $current_data->terminal == 02 || $current_data->terminal == 68 || $current_data->terminal == 20){
          $color = "red";
      }
      else if ($current_data->terminal == 46 || $current_data->terminal == 70){
          $color = "yellow";
      }

      if ($current_data->terminal == 53){
          $color = "blue";
      }

      if ($current_data->terminal == 68 || $current_data->terminal == 196){
          $power_status = "Disconnected";
      }
      else{
          $power_status = 'Connected';
      }
      if ((int)$current_data->speed > 0){
          $color = "green";
      }
  }

  if($color == 'blue'){
    $status = 'Not Connected';
}
else{
    $status = 'Connected';  
}             
}
if(isset($v->user_name)) $user_name = $v->user_name;
if(isset($v->vehicle_no)) $vehicle_no = $v->vehicle_no;
if(isset($v->device_no)) $device_no = $v->device_no;
if(isset($v->device_type)) $device_type = $v->device_type;
if(isset($v->sim_no)) $sim_no = $v->sim_no;
if(isset($v->sim_type)) $sim_type = $v->sim_type;
if(isset($v->device_status)) $device_status = $v->device_status;
else $device_status = 'Inactive';
if($v->subscription_date) $sub_date = $v->subscription_date;
if($v->price) $price = $v->price;
if($v->device_expiry_date){
 $v->device_expiry_date = str_replace('/', '-', $v->device_expiry_date);
 $diff = date_diff(date_create($v->device_expiry_date),date_create(date("d-m-Y")));
 $days = (($diff->y*365)+($diff->m*30)+$diff->d);
}

echo "<tr>
<td>".$i++."</td> 
<td>".$user_name."</td>
<td>".$count_user."</td>
<td>".$vehicle_no."</td>
<td>".$device_no."</td>
<td>".$device_type."</td>
<td>6969</td>
<td>".$sim_no."</td>
<td>".$sim_type."</td>
<td>".$last_updated."</td>
<td>".$power_status."</td>
<td>".$device_status."</td>
<td>".$sub_date."</td>
<td>".$price."</td>
<td>".$days." Days</td>

<td><a href='/admin/edit_vehicals/".$v->vehicle_id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

<form action='/admin/delete_vehicals/".$v->vehicle_id."' method='POST'>
".csrf_field()."

<button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$v->vehicle_id.")'><i class='fa fa-remove'></i></button>
<input type='submit' hidden='hidden' id='form".$v->vehicle_id."' class='btn btn-danger btn-xs'>
</form>
</td>
</tr>";

}
}
}

    // Dynamic Vehicle Search
public function getSearchedUsers(Request $request){
 $search = $request->search;
 $type = $request->type;
 $users = "";
 if($type=='user')
 {
     $users = User::where('parent_user',0)
     ->where('user_name','LIKE','%'.$search.'%')
     ->get();
 }
 else
 {
    $users = User::where('parent_user','>',0)
    ->where('user_name','LIKE','%'.$search.'%')
    ->get();
}

if(!empty($users)){
  $i = 1;
  foreach($users as $user){
    $model = "";
    $email = "";
    $mobile = "";
    if($user->mobile_model)
        $model = $user->mobile_model;
    if($user->email)
        $email = $user->email;
    if($user->mobile)
        $mobile = $user->mobile;

    if($type == 'user'){
        if ($user->account_status == 'barred'){
           echo "<tr> <td>".$i++."</td>
           <td>".$user->user_name."</td>
           <td>".$email."</td>
           <td>".$mobile."</td>
           <td>".$model."</td>
           <td><a href='/admin/sub_users_list/".$user->id."' >View Sub Users</a></td>
           <td><a href='/admin/user_vehicles_list/".$user->id."' >View Vehicles List</a></td>
           <td>".$user->account_status."</td>

           <td>".explode(' ',$user->created_at)[0]."</td><td>

           <a href='/admin/unbar_user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Unbar</a> 

           <a href='/admin/edit_user/user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

           <form action='/admin/delete_user/user/".$user->id."' method='post'>".csrf_field()."

           <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$user->id.")'><i class='fa fa-remove'></i></button>
           <input type='submit' hidden='hidden' id='form".$user->id."' class='btn btn-danger btn-xs'>
           </form>
           </td> </tr>";
       }
       else{
           echo "<tr> <td>".$i++."</td>
           <td>".$user->user_name."</td>
           <td>".$email."</td>
           <td>".$mobile."</td>
           <td>".$model."</td>
           <td><a href='/admin/sub_users_list/".$user->id."' >View Sub Users</a></td>
           <td><a href='/admin/user_vehicles_list/".$user->id."' >View Vehilces List</a></td>
           <td>".$user->account_status."</td>
           <td>".explode(' ',$user->created_at)[0]."</td><td>

           <a href='/admin/bar_user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Bar</a> 

           <a href='/admin/edit_user/user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

           <form action='/admin/delete_user/user/".$user->id."' method='post'>".csrf_field()."

           <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$user->id.")'><i class='fa fa-remove'></i></button>
           <input type='submit' hidden='hidden' id='form".$user->id."' class='btn btn-danger btn-xs'>
           </form>
           </td> </tr>";
       }
   }
   else{
    $parent = User::where('id',$user->parent_user)->first();
    if($parent)
        $parent = $parent->user_name;
    else
        $parent="";
    if ($user->account_status == 'barred'){
       echo "<tr> <td>".$i++."</td>
       <td>".$user->user_name."</td>
       <td>".$email."</td>
       <td>".$mobile."</td>
       <td>".$model."</td>
       <td>".$parent."</td>
       <td>".$user->account_status."</td>
       <td><a href='/admin/user_vehicles_list/".$user->id."' >View Vehicles List</a></td>
       <td>".$user->account_status."</td>
       <td>".explode(' ',$user->created_at)[0]."</td><td>

       <a href='/admin/unbar_user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Unbar</a> 

       <a href='/admin/edit_user/sub-user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

       <form action='/admin/delete_user/sub-user/".$user->id."' method='post'>".csrf_field()."

       <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$user->id.")'><i class='fa fa-remove'></i></button>
       <input type='submit' hidden='hidden' id='form".$user->id."' class='btn btn-danger btn-xs'>
       </form>
       </td> </tr>";
   }
   else{
       echo "<tr> <td>".$i++."</td>
       <td>".$user->user_name."</td>
       <td>".$email."</td>
       <td>".$mobile."</td>
       <td>".$model."</td>
       <td>".$parent."</td>

       <td><a href='/admin/user_vehicles_list/".$user->id."' >View Vehilces List</a></td>
       <td>".$user->account_status."</td>
       <td>".explode(' ',$user->created_at)[0]."</td><td>

       <a href='/admin/bar_user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Bar</a> 

       <a href='/admin/edit_user/sub-user/".$user->id."' class='btn btn-success  mb-1 ml-1'>Edit</a> 

       <form action='/admin/delete_user/sub-user/".$user->id."' method='post'>".csrf_field()."

       <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$user->id.")'><i class='fa fa-remove'></i></button>
       <input type='submit' hidden='hidden' id='form".$user->id."' class='btn btn-danger btn-xs'>
       </form>
       </td> </tr>";
   }
}


}
}
}


//Global Search

public function globalSearch(Request $request){

 $keyword = $request->keyword;
 $type = $request->search_type;
 $vehicles = "";

 $rsas = [];
 $users = [];
 $drivers = [];
 $vehicles = [];
 $complaints = [];

 if($type == 'user'){
  $users = User::where('name','LIKE','%'.$keyword.'%')
  ->orWhere('user_name','LIKE','%'.$keyword.'%')
  ->get();
}
elseif($type == 'driver'){
  $drivers = Driver::where('driver_name','LIKE','%'.$keyword.'%')
  ->orWhere('driver_country','LIKE','%'.$keyword.'%')
  ->orWhere('driver_state','LIKE','%'.$keyword.'%')
  ->get();
}
elseif($type == 'vehicle'){
  $brand = Brand::where('brand_name','LIKE','%'.$keyword.'%')->first();

  if($brand){
   $vehicles = Vehicle::where('vehicle_brand',$brand->id)
   ->get();
}
else{
   $vehicles = Vehicle::where('vehicle_name','LIKE','%'.$keyword.'%')
   ->orWhere('vehicle_no','LIKE','%'.$keyword.'%')
   ->get();
}
}
elseif($type == 'rsa'){
  $rsas = Rsa::where('company','LIKE','%'.$keyword.'%')
  ->get();
}
elseif($type == 'complaint'){
  $complaints = Complaint::where('name','LIKE','%'.$keyword.'%')       
  ->get();
}
elseif($type == 'all'){
  $users = User::where('name','LIKE','%'.$keyword.'%')
  ->orWhere('user_name','LIKE','%'.$keyword.'%')
  ->get();

  $brand = Brand::where('brand_name','LIKE','%'.$keyword.'%')->first();

  if($brand){
   $vehicles = Vehicle::where('vehicle_brand',$brand->id)
   ->get();
}
else{
   $vehicles = Vehicle::where('vehicle_name','LIKE','%'.$keyword.'%')
   ->orWhere('vehicle_no','LIKE','%'.$keyword.'%')
   ->get();
}

$drivers = Driver::where('driver_name','LIKE','%'.$keyword.'%')
->orWhere('driver_country','LIKE','%'.$keyword.'%')
->orWhere('driver_state','LIKE','%'.$keyword.'%')
->get();

$complaints = Complaint::where('name','LIKE','%'.$keyword.'%')       
->get();           

$rsas = Rsa::where('company','LIKE','%'.$keyword.'%')
->get();
}

return view("admin.search",compact('users','vehicles','drivers','complaints','rsas','keyword'));
}



public function getDetails(Request $request){
 $data = new StdClass;
 $message = "";
 $bw=0; $et=0; $working_bw = 0; $working_et = 0;
 $user_id = $request->id;    
 session()->put('owner_id', $user_id);
 session()->put('current_owner_id', $user_id);
 $user_details = User::where('id', $user_id)->first();    
 $user = strtoupper($user_details->user_name);
 $vehicles = VehicleToUser::leftJoin('vehicles','vehicles.vehicle_id','vehicle_to_users.vehicle_id')->where('vehicle_to_users.user_id',$user_id)->select('vehicles.*')->get();  

 $i=1;

 if($vehicles){
  foreach($vehicles as $vehicle){
   $last_updated=""; $status="Not Connected"; $speed=""; $device_no=""; $sim_no="";$subscription_date=""; $address="Not Found"; $activation_status='Inactive';$color="blue"; $power_status = 'Not Connected';

   $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $vehicle->vehicle_id)->first();
   if ($assigned_vehicles1){
    if($assigned_vehicles1->device_type == 'ET300'){
     $et++;
 }
 else{
     $bw++;
 }
 $current_data = LocationData::where('imei', '0'.$assigned_vehicles1->device_no)->orderBy('created_at', 'DESC')->first();  

 if($assigned_vehicles1->device_status=='active')
     $activation_status = 'Active';

 if(isset($current_data) && $current_data->created_at)
     $last_updated = $current_data->created_at;

 if(isset($current_data)  && $current_data->satellites && ($current_data->satellites>5))
     $status = 'Connected';

 if(isset($current_data) && $current_data->speed)
     $speed = $current_data->speed;

 if(isset($current_data)){
     if ($current_data->terminal == 44 || $current_data->terminal == 02 || $current_data->terminal == 68 || $current_data->terminal == 20){
      $color = "red";
  }
  else if ($current_data->terminal == 46 || $current_data->terminal == 70){
      $color = "yellow";
  }

  if ($current_data->speed > 0){
      $color = "green";
  }

  if($current_data->terminal == 44 || $current_data->terminal==46){
    $power_status = 'Normal';
}
elseif($current_data->terminal == 68 || $current_data->terminal==196 || $current_data->terminal == 02 || $current_data->terminal==70){
    $power_status = 'Alarm';                            
}
else{
    $power_status = 'Not Connected';
}


$startTime = new \DateTime($current_data->location_date." ".$current_data->location_time);           
$endTime = new \DateTime();
						$duration = $startTime->diff($endTime); //$duration is a DateInterval object
						$diff_in_minutes = $duration->format("%D". " days " ."%H:%I:%S");
						$integer = (int)$duration->format("%I"); 
						if ($integer > 5){
							$color = 'blue';
						}


                     if ($color == ''){
                      $color = "blue";
                  }
              }

              if($color == 'blue'){
                 $status = 'Not Connected';
             }
             else{
                 $status = 'Connected';
                 if($assigned_vehicles1->device_type == 'ET300'){
                  $working_et++;
              }
              else{
                  $working_bw++;
              }
          }
          if(isset($assigned_vehicles1))
             $device_no = $assigned_vehicles1->device_no;

         if(isset($assigned_vehicles1))
             $sim_no = $assigned_vehicles1->sim_no;

         if(isset($assigned_vehicles1))
             $subscription_date = $assigned_vehicles1->subscription_date;

         if(isset($current_data) && $current_data->latitude && $current_data->longitude){

             $lat = $current_data->latitude; $lng = $current_data->longitude;

             $address= $this->getaddress($lat,$lng);
             if(empty($address))
              $address="Not Found";               
      }
  }

  $message = $message."<tr class='new clickable' onclick='setVehicle($vehicle->vehicle_id),($vehicle->vehicle_no)'> 
  <td>".$i++."</td>
  <td class='new clickable' onclick='setVehicle($vehicle->vehicle_id)' style='color: ".$color."; font-weight: bold;'>".$vehicle->vehicle_no."</td>
  <td>".$user_details->name."</td>
  <td>".$last_updated."</td>
  <td>".$status."</td>
  <td>".$speed."</td>
  <td>".$user_details->user_name."</td>
  <td>".$device_no."</td>
  <td>".$sim_no."</td>
  <td>".$subscription_date."</td>
  <td>".$address."</td>
  <td>".$activation_status."</td>
  <td>".$power_status."</td>
  </tr>";
}
}

$data->message = $message;
$data->user = $user;
$data->power_status = $power_status;
$data->et = $et;
$data->working_et = $working_et;
$data->bw = $bw;
$data->working_bw = $working_bw;

return response()->json($data);
}

public function getDetails2(Request $request){
 $data = new StdClass;
 $message = "";
 $bw=0; $et=0; $working_bw = 0; $working_et = 0;
 $user_id = session()->get('current_owner_id');
 session()->put('current_owner_id', $user_id);
 $user_details = User::where('id', $user_id)->first();    
 $user = strtoupper($user_details->user_name);
 $vehicles = VehicleToUser::leftJoin('vehicles','vehicles.vehicle_id','vehicle_to_users.vehicle_id')->where('vehicle_to_users.user_id',$user_id)->select('vehicles.*')->get();  

 $i=1;

 if($vehicles){
  foreach($vehicles as $vehicle){
   $last_updated=""; $status="Not Connected"; $speed=""; $device_no=""; $sim_no="";$subscription_date=""; $address="Not Found"; $activation_status='Inactive';$color="blue"; $power_status = 'Not Connected';

   $assigned_vehicles1 = DeviceToVehicle::leftJoin('devices','devices.device_id','=','device_to_vehicles.device_id')->where('device_to_vehicles.vehicle_id', $vehicle->vehicle_id)->first();
   if ($assigned_vehicles1){
    if($assigned_vehicles1->device_type == 'ET300'){
     $et++;
 }
 else{
     $bw++;
 }
 $current_data = LocationData::where('imei', '0'.$assigned_vehicles1->device_no)->orderBy('created_at', 'DESC')->first();  

 if($assigned_vehicles1->device_status=='active')
     $activation_status = 'Active';

 if(isset($current_data) && $current_data->created_at)
     $last_updated = $current_data->created_at;

 if(isset($current_data)  && $current_data->satellites && ($current_data->satellites>5))
     $status = 'Connected';

 if(isset($current_data) && $current_data->speed)
     $speed = $current_data->speed;

 if(isset($current_data)){
     if ($current_data->terminal == 44 || $current_data->terminal == 02 || $current_data->terminal == 68 || $current_data->terminal == 20){
      $color = "red";
  }
  else if ($current_data->terminal == 46 || $current_data->terminal == 70){
      $color = "yellow";
  }

  if ($current_data->speed > 0){
      $color = "green";
  }

  if($current_data->terminal == 44 || $current_data->terminal==46){
    $power_status = 'Normal';
}
elseif($current_data->terminal == 68 || $current_data->terminal==196 || $current_data->terminal == 02 || $current_data->terminal==70){
    $power_status = 'Alarm';                            
}
else{
    $power_status = 'Not Connected';
}


$startTime = new \DateTime($current_data->location_date." ".$current_data->location_time);           
$endTime = new \DateTime();
                        $duration = $startTime->diff($endTime); //$duration is a DateInterval object
                        $diff_in_minutes = $duration->format("%D". " days " ."%H:%I:%S");
                        $integer = (int)$duration->format("%I"); 
                        if ($integer > 5){
                            $color = 'blue';
                        }


                        if ($color == ''){
                          $color = "blue";
                      }
                  }

                  if($color == 'blue'){
                     $status = 'Not Connected';
                 }
                 else{
                     $status = 'Connected';
                     if($assigned_vehicles1->device_type == 'ET300'){
                      $working_et++;
                  }
                  else{
                      $working_bw++;
                  }
              }
              if(isset($assigned_vehicles1))
                 $device_no = $assigned_vehicles1->device_no;

             if(isset($assigned_vehicles1))
                 $sim_no = $assigned_vehicles1->sim_no;

             if(isset($assigned_vehicles1))
                 $subscription_date = $assigned_vehicles1->subscription_date;

             if(isset($current_data) && $current_data->latitude && $current_data->longitude){

                 $lat = $current_data->latitude; $lng = $current_data->longitude;

                 $address= $this->getaddress($lat,$lng);
                 if(empty($address))
                  $address="Not Found";               
          }
      }

      $message = $message."<tr class='new clickable' onclick='setVehicle($vehicle->vehicle_id),($vehicle->vehicle_no)'> 
      <td>".$i++."</td>
      <td class='new clickable' onclick='setVehicle($vehicle->vehicle_id)' style='color: ".$color."; font-weight: bold;'>".$vehicle->vehicle_no."</td>
      <td>".$user_details->name."</td>
      <td>".$last_updated."</td>
      <td>".$status."</td>
      <td>".$speed."</td>
      <td>".$user_details->user_name."</td>
      <td>".$device_no."</td>
      <td>".$sim_no."</td>
      <td>".$subscription_date."</td>
      <td>".$address."</td>
      <td>".$activation_status."</td>
      <td>".$power_status."</td>
      </tr>";
  }
}

$data->message = $message;
$data->user = $user;
$data->power_status = $power_status;
$data->et = $et;
$data->working_et = $working_et;
$data->bw = $bw;
$data->working_bw = $working_bw;

return response()->json($data);
}



function getaddress($lat,$lng)
{
	$url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDS_q4dK7XxTGUh2NziiU749EGP4qUoNKU&latlng='.trim($lat).','.trim($lng).'&sensor=false';
	$json = @file_get_contents($url);
	$data=json_decode($json);
	if(isset($data))
		$status = $data->status;
	else
		$status = 'Okay';
	if($status=="OK") return $data->results[0]->formatted_address;
	else
		return false;
}



public function editComplaint(Request $request,$id){
	$complaint = Complaint::find($id);
	return view('admin.edit-complaint',compact('complaint'));
}

public function replyComplaint(Request $request,$id){
	$complaint = Complaint::find($id);
	$complaint->reply = $request->reply;
	$complaint->update();

	return redirect()->back()->with('message','Replied To Complaint Successfully');
}

public function openComplaint($id){
	$complaint = Complaint::where('id',$id)
	->update(
		array(
			'status' => 'Close'
		));

	return redirect()->back()->with('message','Complaint closed successfully');
}

public function closeComplaint($id){
	$complaint = Complaint::where('id',$id)
	->update(
		array(
			'status' => 'Open'
		));

	return redirect()->back()->with('message','Complaint status changed successfully');
}


public function registration(){
	$brandlist = Brand::all();
	$brands = array();
	$vehicle_type = array(
		'Motor Car' => "Motor Car", 
		'Bike' => "Bike", 
		'Taxi' => "Taxi", 
		'Crane' => "Crane", 
		'JCB' => "JCB", 
		'Tractor' => "Tractor", 
		'Ambulance' => "Ambulance", 
		'E-Rickshaw' => "E-Rickshaw", 
		'Personal Tracker' => "Personal Tracker", 
		'Chhota Hathi' => "Chhota Hathi", 
		'Scooter' => "Scooter",
		'Truck' => "Truck",
		'Bus' => "Bus",
		'Other' => "Other",
	);
	$userlist = User::where('user_type', 'user')->get();
	$users = array();
	foreach ($userlist as $key => $value) {
		array_push($users, $value->name);
	}


	foreach ($brandlist as $key => $value) {
		$brands[$value->id] = $value->brand_name;
	}        

	return view('admin.registration', compact('brands', 'vehicle_type', 'users'));
}

public function postRegistration(Request $request){

	$new = $request->new_user;

	$vehicle_id = "";
	$device_id = "";
	$owner_id = "";
	$obj = new Vehicle;
	if($request->hasFile('vehicle_image')) 
	{
		$file=$request->vehicle_image;
		$file->move(public_path(). '/upload/', time().$file->getClientOriginalName());   
		$obj->vehicle_image = time().$file->getClientOriginalName();
	}

	$data=Validator::make($request->all(),[
		"vehicle_no"=>"required|min:2|max:250",
		"owner_id"=>"required|min:2|max:250"
	])->validate();     

	if($new != NULL){
		$user = new User;

		$user->name = $request->owner_id;
		$user->email = $request->owner_id;
		$user->mobile = $request->mobile;
		$user->user_name = $request->owner_id;
		$user->password = Hash::make($request->password);
		$user->parent_user = '0';
		$user->user_type = 'user';
		$user->save();
		$owner_id = $user->id;
	}
	else{
		$check = User::where('user_name',$request->owner_id)->first();
		$owner_id = $check->id;
	}

	$obj->vehicle_name= $request->vehicle_name;
	$obj->vehicle_brand= $request->vehicle_brand;
	$obj->vehicle_type= $request->vehicle_type;
	$obj->vehicle_no= $request->vehicle_no;
	$obj->owner_id= $owner_id;
	$obj_v=$obj->save();

	$getV = Vehicle::where('vehicle_no',$request->vehicle_no)->first();

	$vehicle_id = $getV->vehicle_id;

	$data=Validator::make($request->all(),[
		"device_no"=>"required|min:2|max:250|unique:devices",
		"sim_no"=>"required|min:2|max:100",
	])->validate();     

	$obj=new Device;
	$obj->device_no= $request->input('device_no');
	$obj->sim_no= $request->input('sim_no');
	$obj->sim_type= $request->input('sim_type');
	$obj->device_type= $request->input('device_type');

    $obj->subscription_date= $request->input('subscription_date');
    $obj->device_expiry_date= $request->input('expiry_date');
    $obj->price= $request->price;
    $obj->user_id= $request->user()->id;
    $obj->added_by= $request->user()->id;
    $obj_d=$obj->save();

    $getD = Device::where('device_no',$request->device_no)->first();

    $device_id = $getD->device_id;

    $obj_dv = new DeviceToVehicle;

    $obj_dv->device_id = $device_id;
    $obj_dv->vehicle_id = $vehicle_id;
    $obj_dv->assigned_by = \Auth::user()->id;
    $obj_dv->save();

    $obj_uv = new VehicleToUser;

    $obj_uv->vehicle_id = $vehicle_id; 
    $obj_uv->user_id = $owner_id;
    $obj_uv->save(); 

    if($obj_d && $obj_v && $obj_dv)
      return redirect()->back()->with('message','Registration successfully.');
}

public function getBulletins(Request $request)
{
	$bulletins = Bulletin::leftJoin('users','users.id','bulletins.user_id')->orderBy('bulletins.created_at','DESC')->select('bulletins.*','users.user_name')->get();
	return view('admin.bulletins.bulletin-list',compact('bulletins'));
}

public function showsendsms(Request $request)
{
	$users = User::where('user_type','!=','admin')->get();
	return view('admin.bulletins.sendsms',compact('users'));
}

public function sendsms(Request $request)
{
	$users = $request->all_users;
	$message = $request->message;

	if($users == 'all'){
		$get_all = User::where('user_type','!=','admin')->get();

		if(isset($get_all)){
			foreach($get_all as $user){
				$check = Bulletin::where('user_id',$user->id)->first();
				if($check){
					$check->message = $message;
					$check->update();
				}
				else{
					$bulletin = new Bulletin;
					$bulletin->user_id = $user->id;
					$bulletin->message = $message;
					$bulletin->save();
				}
			}
		}
	}
	else{
		$nos = $request->users;
		foreach($nos as $n){
			$no = $n;
			$check = Bulletin::where('user_id',$no)->first();
			if($check){
				$check->message = $message;
				$check->update();
			}
			else{
				$bulletin = new Bulletin;
				$bulletin->user_id = $no;
				$bulletin->message = $message;
				$bulletin->save();
			}
		}   
	}
	return redirect()->back()->with('message','Message Sent Successfully');
}

public function editBulletin($id){
	$bulletin = Bulletin::leftJoin('users','users.id','bulletins.user_id')->where('bulletins.id',$id)->select('bulletins.*','users.user_name')->first();
	return view('admin.bulletins.edit_bulletin',compact('bulletin'));
} 


public function updateBulletin(Request $request , $id){
	$bulletin = Bulletin::find($id);
	$bulletin->message = $request->message;
	$bulletin->update();
	return redirect()->back()->with('message','Bulletin updated Successfully');
} 

public function deleteBulletin($id){
	$rsas = Bulletin::find($id);
	$rsas->delete();
	return redirect()->back()->with('message','Record deleted Successfully');
}  

public function searchBulletin(Request $request){
    $data = $request->all();

    $ids = [];

    $users = User::where('user_name','LIKE','%'.$data['search'].'%')->get();

    if($users){
        foreach ($users as $user) {
            $ids[] = $user->id;
        }
    }

    $bulletins = Bulletin::whereIn('user_id',$ids)->get();

    if($bulletins){
        $i=1;
        foreach ($bulletins as $bullet) {
            $user = User::where('id',$bullet->user_id)->first();
            echo "<tr>
            <td>".$i++."</td> 
            <td>".$user->user_name."</td>
            <td>".$bullet->message."</td>       
            <td>
            <ul class='sendsms'>
            <li>
            <a href='/admin/edit_bulletin/".$bullet->id."' class='btn btn-success mb-1 ml-1'>Edit</a> 
            </li>
            <li>
            <form action='/admin/bulletins-list/".$bullet->id."' method='post'>".
            csrf_field()."
            <button type='button' class='btn btn-danger btn-xs' onclick='confirmDelete(".$bullet->id.")'><i class='fa fa-remove'></i></button>
            <input type='submit' hidden='hidden' id='form".$bullet->id."' class='btn btn-danger btn-xs'>
            </form>
            </li>
            </ul>      
            </td>
            </tr>";
        }
    }

}


public function getSearchedBarredUsers(Request $request){
    $data = $request->all();
    $users = User::where('account_status','barred')->where('user_name','LIKE','%'.$data['search'].'%')->get();

    if($users){
        $i=1;
        foreach ($users as $user) {
            $type = "";
            if($user->parent_user>0) 
                $type = 'sub-user'; 
            else 
                $type = 'user';
            echo "<tr>
            <td>".$i++."</td> 
            <td>".$user->name."</td>
            <td>".$user->email."</td>
            <td>".$user->mobile."@endif</td>
            <td>Barred</td>
            <td><a href='/admin/unbar_user/".$user->id."' class='btn btn-success mb-1 ml-1'>Unbar</a></td>

            <td>".explode(' ',$user->created_at)[0]."</td>
            <td colspan='2'><a href='/admin/edit_user/".$type."/".$user->id."' class='btn btn-success mb-1 ml-1'>Edit</a> 

            <form action='/admin/delete_user/".$type."/".$user->id."' method=post>
            ".csrf_field()."
            <button type='submit' class='btn btn-danger btn-xs'>Delete</button>
            </form>
            </td>
            </tr>";
        }
    }
}

}
