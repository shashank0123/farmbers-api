<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crop;

class CropController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'crop';
        $crops = Crop::all();
        
        return view('admin.crops.list',compact('crops','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'add_crop';
        return view('admin.crops.add',compact('page'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $newData = new crop;

        if($request->hasFile('crop_image')) 
        {
         $file=$request->crop_image;
         $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
         $newData->crop_image = time().$file->getClientOriginalName();
     }

     $newData->crop_name = $data['crop_name'];
     $newData->crop_type = $data['crop_type'];
     $newData->crop_cycle = $data['crop_cycle'];
     
     $newData->save();

     return redirect()->back()->with('message','Data successfully added');
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'edit_crop';
        $crop = Crop::find($id);
        return view('admin.crops.edit',compact('crop','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $newData = Crop::find($id);
       if($request->hasFile('crop_image')) 
        {
         $file=$request->crop_image;
         $file->move(public_path(). '/crops_images/', time().$file->getClientOriginalName());   
         $newData->crop_image = time().$file->getClientOriginalName();
     }

     $newData->crop_name = $data['crop_name'];
     $newData->crop_type = $data['crop_type'];
     $newData->crop_cycle = $data['crop_cycle'];

     $newData->update();


     return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crop = Crop::where('id',$id)->delete();
     return redirect()->back()->with('message','Data successfully deleted');
    }
}
