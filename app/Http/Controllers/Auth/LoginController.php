<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    

    public function logout() {
        // $user = User::find(Auth::user()->id);
        // $user->web_login_status = '0';
        // $user->update();
        Auth::logout();
        return redirect('/login');
    }
    
  //   public function login(Request $request)
  //   {
  //       $next = session()->get('url.intended');

  //       if ( Auth::attempt([
  //           'user_name' => $request->user_name,
  //           'password' => $request->password
  //           ],$request->has('remember'))){
  //           if(session()->has('url.intended'))
  //           {
  //               echo $next = session()->get('url.intended');
  //           }
  //               return redirect('/home');
  //               // var_dump(auth()->user());
  //               // die();
  //               return redirect($next);

  //       }
	 // else
  //           return redirect('login')->withErrors([
  //                               'credentials' => 'Please, check your credentials'
  //                           ]);

        


  //    }
}
