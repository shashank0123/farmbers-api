<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crop_steps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('crop_id');
            $table->string('step_number');
            $table->string('no_of_days')->nullable();
            $table->string('description');
            $table->string('step_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crop_steps');
    }
}
